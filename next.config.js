/** @type {import('next').NextConfig} */
const nextConfig = {
    reactStrictMode: true,
}

module.exports = nextConfig

module.exports = {
    trailingSlash: true,
};

module.exports = {
    i18n: {
        locales: ['ar', 'de', 'fr', 'en'],
        defaultLocale: 'fr'
    }
}