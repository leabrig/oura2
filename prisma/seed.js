const {PrismaClient} = require('@prisma/client');
const prisma = new PrismaClient();

async function main() {

    // TODO: initial seeding, to adapt to multilingual
    await prisma.user.upsert({
        where: {email: "admin@oura2.ch"},
        update: {},
        create: {
            email: "admin@oura2.ch",
            hash: "06d37307f46e38af3daa32f03889a78bce22577e24724078b85417995f6c9b2fd01c7126e57f44041b643be69104f8f9e9b3a7d9691ddcdd69ba1a3f481886d2",
            salt: "006f0d29b04902c50eac4017599cb40d",
            admin: true,
        },
    })

    await prisma.domain.upsert({
        where: {label: 'Cognitif'},
        update: {},
        create: {
            label: 'Cognitif',
            dimensions: {
                create: [
                    {
                        label: 'Utilité perçue',
                        questions: {
                            create: [
                                {
                                    label: "L'activité réalisée était utile.",
                                },
                                {
                                    label: "Cette activité n'a servi à rien.",
                                    posneg: false,
                                },
                            ]
                        }
                    },
                    {
                        label: 'Sentiment de compétence',
                        questions: {
                            create: [
                                {
                                    label: "J'ai trouvé cette activité difficile à faire.",
                                    posneg: false,
                                },
                                {
                                    label: "Cette activité était facile à faire.",
                                },
                            ]
                        }
                    },
                    {
                        label: 'Autodétermination',
                        questions: {
                            create: [
                                {
                                    label: "Dans cette activité, je devais suivre toutes les instructions de l'enseignant·e.",
                                    posneg: false,
                                },
                                {
                                    label: "Dans cette activité, je pouvais choisir ce que je voulais faire.",
                                },
                            ]
                        }
                    }
                ]
            }
        }
    })

    await prisma.domain.upsert({
        where: {label: 'Affectif'},
        update: {},
        create: {
            label: 'Affectif',
            dimensions: {
                create: [
                    {
                        label: 'Attrait',
                        questions: {
                            create: [
                                {
                                    label: "J'ai eu du plaisir à faire cette activité.",
                                },
                                {
                                    label: "Cette activité était ennuyeuse.",
                                    posneg: false,
                                },
                            ]
                        }
                    },
                    {
                        label: 'Stress-Anxiété',
                        questions: {
                            create: [
                                {
                                    label: "J'étais stressé·e en faisant cette activité.",
                                },
                                {
                                    label: "Je me sentais détendu·e pour réaliser cette activité.",
                                    posneg: false,
                                },
                            ]
                        }
                    },
                    {
                        label: 'Sentiment de fierté',
                        questions: {
                            create: [
                                {
                                    label: "Je suis fier·ère du travail effectué durant cette activité.",
                                },
                                {
                                    label: "J'ai honte du travail que j'ai fait durant cette activité.",
                                    posneg: false,
                                },
                            ]
                        }
                    }
                ]
            }
        }
    })

    await prisma.domain.upsert({
        where: {label: 'Comportement'},
        update: {},
        create: {
            label: 'Comportement',
            dimensions: {
                create: [
                    {
                        label: 'Engagement',
                        questions: {
                            create: [
                                {
                                    label: "J'ai fait des efforts pour réaliser cette activité.",
                                },
                                {
                                    label: "Mon engagement était faible durant cette activité.",
                                    posneg: false,
                                },
                            ]
                        }
                    },
                    {
                        label: 'Concentration',
                        questions: {
                            create: [
                                {
                                    label: "Je suis resté·e très concentré·e durant cette activité.",
                                },
                                {
                                    label: "J'ai été souvent distrait·e durant cette activité.",
                                    posneg: false,
                                },
                            ]
                        }
                    },
                    {
                        label: 'Organisation',
                        questions: {
                            create: [
                                {
                                    label: "J'étais bien organisé·e durant cette activité.",
                                },
                                {
                                    label: "J'étais désorganisé·e pour réaliser cette activité.",
                                    posneg: false,
                                },
                            ]
                        }
                    }
                ]
            }
        }
    })

    await prisma.domain.upsert({
        where: {label: 'Conditions'},
        update: {},
        create: {
            label: 'Conditions',
            dimensions: {
                create: [
                    {
                        label: 'Acceptance',
                        questions: {
                            create: [
                                {
                                    label: "J'ai pu réaliser cette activité dans de bonnes conditions.",
                                },
                                {
                                    label: "Les conditions pour réaliser l'activité étaient mauvaises.",
                                    posneg: false,
                                },
                            ]
                        }
                    },
                    {
                        label: 'Utilisabilité',
                        questions: {
                            create: [
                                {
                                    label: "J'ai pu utiliser facilement les ressources à disposition.",
                                },
                                {
                                    label: "C'était compliqué d'utiliser les ressources proposées.",
                                    posneg: false,
                                },
                            ]
                        }
                    },
                    {
                        label: 'Soutien',
                        questions: {
                            create: [
                                {
                                    label: "J'ai eu toute l'aide nécessaire pour réaliser cette activité.",
                                },
                                {
                                    label: "Je m'attendais à être davantage soutenu·e dans cette activité.",
                                    posneg: false,
                                },
                            ]
                        }
                    }
                ]
            }
        }
    })

    await prisma.domain.upsert({
        where: {label: 'Apprentissage'},
        update: {},
        create: {
            label: 'Apprentissage',
            dimensions: {
                create: [
                    {
                        label: 'Objectivation',
                        questions: {
                            create: [
                                {
                                    label: "J'ai atteint les objectifs de l'activité.",
                                },
                                {
                                    label: "J'ai raté l'atteinte des objectifs de l'activité.",
                                    posneg: false,
                                },
                            ]
                        }
                    },
                    {
                        label: 'Performance',
                        questions: {
                            create: [
                                {
                                    label: "J'ai réalisé un bon travail.",
                                },
                                {
                                    label: "Le travail que j'ai effectué est de qualité médiocre.",
                                    posneg: false,
                                },
                            ]
                        }
                    },
                    {
                        label: 'Intérêt personnel',
                        questions: {
                            create: [
                                {
                                    label: "Cette activité m'a beaucoup apporté sur le plan personnel.",
                                },
                                {
                                    label: "Je ne retire rien d'intéressant de cette activité.",
                                    posneg: false,
                                },
                            ]
                        }
                    }
                ]
            }
        }
    })

    await prisma.domain.upsert({
        where: {label: 'Collectif'},
        update: {},
        create: {
            label: 'Collectif',
            dimensions: {
                create: [
                    {
                        label: 'Fonctionnement',
                        questions: {
                            create: [
                                {
                                    label: "Le groupe de travail a bien fonctionné.",
                                },
                                {
                                    label: "C'était difficile de travailler dans ce groupe.",
                                    posneg: false,
                                },
                            ]
                        }
                    },
                    {
                        label: 'Apports',
                        questions: {
                            create: [
                                {
                                    label: "Le travail du groupe a été fructueux.",
                                },
                                {
                                    label: "Ce travail en groupe ne m'a rien apporté.",
                                    posneg: false,
                                },
                            ]
                        }
                    },

                ]
            }
        }
    })

    await prisma.domain.upsert({
        where: {label: 'Métacognition'},
        update: {},
        create: {
            label: 'Métacognition',
            dimensions: {
                create: [
                    {
                        label: 'Autorégulation',
                        questions: {
                            create: [
                                {
                                    label: "C'était compliqué de gérer l'activité.",
                                    posneg: false,
                                },
                                {
                                    label: "Je suis parvenu·e à bien gérer mon travail durant l'activité.",
                                },
                            ]
                        }
                    },
                    {
                        label: 'Prise de conscience',
                        questions: {
                            create: [
                                {
                                    label: "Difficile pour moi d'avoir un souvenir précis de ce qu'il s'est passé durant l'activité.",
                                    posneg: false,
                                },
                                {
                                    label: "J'ai en mémoire les moments clés de cette activité.",
                                },
                            ]
                        }
                    },
                    {
                        label: 'Transfert',
                        questions: {
                            create: [
                                {
                                    label: "Ce que j'ai fait dans cette activité pourra  être utilisé dans d'autres circonstances.",
                                },
                                {
                                    label: "Ce sera difficile de transférer dans d'autres contextes ce que j'ai fait dans cette activité.",
                                    posneg: false,
                                },
                            ]
                        }
                    },
                    {
                        label: 'Anticipation',
                        questions: {
                            create: [
                                {
                                    label: "J'ai eu des difficultés à savoir ce qu'il fallait faire dans cette activité.",
                                    posneg: false,
                                },
                                {
                                    label: "Les attentes du ou de la professeur·e étaient claires pour moi.",
                                },
                            ]
                        }
                    },
                ]
            }
        }
    })

    await prisma.domain.upsert({
        where: {label: 'Formation des enseignant·e·s'},
        update: {},
        create: {
            label: 'Formation des enseignant·e·s',
            dimensions: {
                create: [
                    {
                        label: 'Donnée de consignes',
                        questions: {
                            create: [
                                {
                                    label: "Les consignes de l'activité étaient claires.",
                                },
                                {
                                    label: "C'était difficile de comprendre les consignes de l'activité.",
                                    posneg: false,
                                },
                            ]
                        }
                    },
                    {
                        label: 'Questionnement',
                        questions: {
                            create: [
                                {
                                    label: "J'ai bien compris les questions qu'on m'a posées durant l'activité.",
                                },
                                {
                                    label: "Les questions posées durant l'activité étaient confuses.",
                                    posneg: false,
                                },
                            ]
                        }
                    },
                    {
                        label: "Objectifs d'apprentissage",
                        questions: {
                            create: [
                                {
                                    label: "Les objectifs de l'activité étaient clairement explicités.",
                                },
                                {
                                    label: "C'était difficile de savoir en quoi consistaient les objectifs de l'activité.",
                                    posneg: false,
                                },
                            ]
                        }
                    },
                    {
                        label: 'Support de cours',
                        questions: {
                            create: [
                                {
                                    label: "Les supports utilisés durant l'activité étaient de qualité.",
                                },
                                {
                                    label: "Les supports proposés étaient brouillons.",
                                    posneg: false,
                                },
                            ]
                        }
                    },
                    {
                        label: 'Usage des technologies',
                        questions: {
                            create: [
                                {
                                    label: "Le·la professeur·e a bien maîtrisé les technologies utilisées.",
                                },
                                {
                                    label: "Le·la professeur·e a eu des difficultés avec les outils techniques.",
                                    posneg: false,
                                },
                            ]
                        }
                    },
                    {
                        label: 'Structure de la leçon',
                        questions: {
                            create: [
                                {
                                    label: "La leçon allait dans tous les sens.",
                                    posneg: false,
                                },
                                {
                                    label: "Les différentes étapes à réaliser durant l'activité étaient cohérentes.",
                                },
                            ]
                        }
                    },
                    {
                        label: 'Gestion du temps',
                        questions: {
                            create: [
                                {
                                    label: "Le temps consacré à l'activité a bien été géré (ni trop de temps, ni trop peu).",
                                },
                                {
                                    label: "Le temps à disposition n'était pas adéquat (trop de temps ou pas assez).",
                                    posneg: false,
                                },
                            ]
                        }
                    },
                    {
                        label: 'Présence',
                        questions: {
                            create: [
                                {
                                    label: "J'ai apprécié la relation pédagogique avec le·la professeur·e durant l'activité.",
                                },
                                {
                                    label: "Le·la professeur·e a manqué de présence (charisme) durant l'activité.",
                                    posneg: false,
                                },
                            ]
                        }
                    },
                    {
                        label: 'Climat de la classe',
                        questions: {
                            create: [
                                {
                                    label: "L'activité s'est déroulée dans un bon climat.",
                                },
                                {
                                    label: "Le climat de classe était mauvais durant l'activité.",
                                    posneg: false,
                                },
                            ]
                        }
                    },
                    {
                        label: 'Médiation des savoirs',
                        questions: {
                            create: [
                                {
                                    label: "L'enseignant·e a utilisé de bonnes stratégies pour nous expliquer ce qu'on devait apprendre.",
                                },
                                {
                                    label: "J'ai eu du mal à comprendre ce qu'on m'expliquait.",
                                    posneg: false,
                                },
                            ]
                        }
                    },
                    {
                        label: 'Maîtrise des contenus',
                        questions: {
                            create: [
                                {
                                    label: "Le·la professeur·e a des difficultés avec les contenus qu'il·elle enseigne.",
                                    posneg: false,
                                },
                                {
                                    label: "Le·la professeur·e est à l'aise avec la matière enseignée.",
                                },
                            ]
                        }
                    },
                    {
                        label: 'Feed-back',
                        questions: {
                            create: [
                                {
                                    label: "Les feed-back donnés durant l'activité sont pertinents pour progresser.",
                                },
                                {
                                    label: "Les feed-back reçus sont inutiles.",
                                    posneg: false,
                                },
                            ]
                        }
                    },
                ]
            }
        }
    })

    await prisma.domain.upsert({
        where: {label: 'Conférence'},
        update: {},
        create: {
            label: 'Conférence',
            dimensions: {
                create: [
                    {
                        label: 'Présence',
                        questions: {
                            create: [
                                {
                                    label: "Par son charisme, l'orateur·trice a su intéresser son auditoire.",
                                },
                                {
                                    label: "L'orateur·trice a eu des difficultés à capter son auditoire.",
                                    posneg: false,
                                },
                            ]
                        }
                    },
                    {
                        label: 'Structure de la conférence',
                        questions: {
                            create: [
                                {
                                    label: "La conférence était bien structurée.",
                                },
                                {
                                    label: "C'était difficile de suivre le fil conducteur de la conférence.",
                                    posneg: false,
                                },
                            ]
                        }
                    },
                    {
                        label: 'Clarté des propos',
                        questions: {
                            create: [
                                {
                                    label: "Les propos de l'orateur·trice étaient clairs et précis.",
                                },
                                {
                                    label: "L'orateur·trice était confus·e dans son propos.",
                                    posneg: false,
                                },
                            ]
                        }
                    },
                    {
                        label: 'Élocution',
                        questions: {
                            create: [
                                {
                                    label: "L'orateur·trice parlait distinctement.",
                                },
                                {
                                    label: "L'orateur·trice avait des problèmes d'élocution.",
                                    posneg: false,
                                },
                            ]
                        }
                    },
                    {
                        label: 'Maîtrise de la langue',
                        questions: {
                            create: [
                                {
                                    label: "L'orateur·trice maîtrisait bien la langue de la conférence.",
                                },
                                {
                                    label: "L'orateur·trice faisait beaucoup d'erreurs liées à la maîtrise de la langue.",
                                    posneg: false,
                                },
                            ]
                        }
                    },
                    {
                        label: 'Réponses aux questions',
                        questions: {
                            create: [
                                {
                                    label: "L'orateur·trice a bien répondu aux questions.",
                                },
                                {
                                    label: "L'orateur·trice a éludé les questions posées.",
                                    posneg: false,
                                },
                            ]
                        }
                    },
                    {
                        label: 'Gestion du temps',
                        questions: {
                            create: [
                                {
                                    label: "L'orateur·trice a bien géré le temps qu'il·elle avait à disposition.",
                                },
                                {
                                    label: "L'orateur·trice a eu des problèmes à gérer le temps à disposition.",
                                    posneg: false,
                                },
                            ]
                        }
                    },
                    {
                        label: 'Maîtrise du contenu',
                        questions: {
                            create: [
                                {
                                    label: "L'orateur·trice est incertain·e sur le contenu de sa conférence.",
                                    posneg: false,
                                },
                                {
                                    label: "L'orateur·trice maîtrise bien son sujet.",
                                },
                            ]
                        }
                    },
                    {
                        label: 'Qualité des supports',
                        questions: {
                            create: [
                                {
                                    label: "Les supports de la conférence étaient de qualité.",
                                },
                                {
                                    label: "Les supports de la conférence étaient brouillons.",
                                    posneg: false,
                                },
                            ]
                        }
                    },
                    {
                        label: 'Technologie',
                        questions: {
                            create: [
                                {
                                    label: "L'orateur·trice a eu des difficultés avec les outils techniques à sa disposition.",
                                    posneg: false,
                                },
                                {
                                    label: "L'orateur·trice a utilisé judicieusement les outils numériques.",
                                },
                            ]
                        }
                    },
                ]
            }
        }
    })
}

main()
    .then(async () => {
        await prisma.$disconnect()
    })
    .catch(async (e) => {
        console.error(e)
        await prisma.$disconnect()
        process.exit(1)
    })