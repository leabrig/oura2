/*
  Warnings:

  - A unique constraint covering the columns `[label]` on the table `Domain` will be added. If there are existing duplicate values, this will fail.

*/
-- DropForeignKey
ALTER TABLE "Dimension" DROP CONSTRAINT "Dimension_domainId_fkey";

-- DropForeignKey
ALTER TABLE "Question" DROP CONSTRAINT "Question_dimensionId_fkey";

-- AlterTable
ALTER TABLE "users" ADD COLUMN     "recoveryHash" TEXT,
ADD COLUMN     "recoverySalt" TEXT,
ADD COLUMN     "recoveryTimeStamp" TIMESTAMP(3);

-- CreateIndex
CREATE UNIQUE INDEX "Domain_label_key" ON "Domain"("label");

-- AddForeignKey
ALTER TABLE "Dimension" ADD CONSTRAINT "Dimension_domainId_fkey" FOREIGN KEY ("domainId") REFERENCES "Domain"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Question" ADD CONSTRAINT "Question_dimensionId_fkey" FOREIGN KEY ("dimensionId") REFERENCES "Dimension"("id") ON DELETE CASCADE ON UPDATE CASCADE;
