-- CreateEnum
CREATE TYPE "Language" AS ENUM ('FRENCH', 'GERMAN', 'ENGLISH', 'ARABIC');

-- AlterTable
ALTER TABLE "Dimension" ADD COLUMN     "labelAr" TEXT,
ADD COLUMN     "labelDe" TEXT,
ADD COLUMN     "labelFr" TEXT;

-- AlterTable
ALTER TABLE "Domain" ADD COLUMN     "labelAr" TEXT,
ADD COLUMN     "labelDe" TEXT,
ADD COLUMN     "labelFr" TEXT;

-- AlterTable
ALTER TABLE "Question" ADD COLUMN     "labelAr" TEXT,
ADD COLUMN     "labelDe" TEXT,
ADD COLUMN     "labelFr" TEXT;

-- AlterTable
ALTER TABLE "users" ADD COLUMN     "language" "Language" NOT NULL DEFAULT 'FRENCH';
