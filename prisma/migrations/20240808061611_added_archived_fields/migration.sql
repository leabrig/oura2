-- AlterTable
ALTER TABLE "DiscriminantVariable" ADD COLUMN     "isArchived" BOOLEAN NOT NULL DEFAULT false;

-- AlterTable
ALTER TABLE "Survey" ADD COLUMN     "isArchived" BOOLEAN NOT NULL DEFAULT false;
