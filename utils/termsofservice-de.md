---
title: 'Allgemeine Geschäftsbedingungen'
---

Indem Sie die Allgemeinen Geschäftsbedingungen akzeptieren, verpflichten Sie sich, alle hier genannten Punkte einzuhalten.

### Institutionelle Grundlage
Die Plattform OURA² (für OUtil de Régulation des Activités d'enseignement/apprentissage) wurde von Lea Briguet im Rahmen der wissenschaftlichen Aktivitäten des CERF (Centre d'Enseignement et de Recherche pour la Formation à l'enseignement au secondaire) der Universität Freiburg (Schweiz) unter der Leitung von Pierre-François Coen entwickelt ([www.oura2.ch](https://www.oura2.ch)).

Die Entwicklung und Nutzung der von der Anwendung OURA² erzeugten Daten sowie jegliche Änderung ihres Inhalts unterliegen der alleinigen Kontrolle der Entwicklerin (Lea Briguet) und des wissenschaftlichen Leiters des Projekts (Pierre-François Coen).

### Respekt vor den Autorinnen und Autoren

Jeder Verweis auf die OURA2-Plattform und/oder ihre Verwendung in Publikationen für die breite Öffentlichkeit, in Fachzeitschriften oder in wissenschaftlichen Publikationen muss erfolgen:

&ensp; Coen, P.-F. & Briguet, L. (2023). OURA2 [Internetplattform]. Centre d'enseignement et de recherche pour la formation au secondaire. Universität Freiburg. https://www.oura2.ch/

### Dienstleistungen

Die von OURA² angebotenen Dienste ermöglichen es, Daten über die Wahrnehmungen zu sammeln, die von Personen (Schülerinnen und Schülern, Studierenden, Zuhörerinnen und Zuhörern ...) nach einer Lernerfahrung geäußert werden. Um dem Ersteller eines Lehr-/Lernarrangements (Lehrer/in, Professor/in, Dozent/in ...) Informationen zu liefern und es zu verbessern, werden diese Daten in verschiedenen Bereichen erhoben und mithilfe von anpassbaren Umfragen bei den Lernenden oder Teilnehmern/innen gesammelt. Nach der Analyse werden diese Daten in verschiedenen Formen (Grafiken, Datenbanken) an den/die Initiator/in der Umfrage zurückgegeben, der/die sie für die Selbstevaluation verwenden kann. Die Nutzung von OURA² ist kostenlos.

### Zielpublikum

Die Anwendung OURA² richtet sich an alle Personen, die Lehr-/Lernarrangements oder Aktivitäten entwickeln (Lehrer/innen, Professor/innen, Referent/innen ...) und diese auf der Grundlage der Wahrnehmungen der Lernenden und/oder Teilnehmer/innen (Schüler/innen, Student/innen, Zuhörer/innen ...) verbessern möchten. Es gibt keine Altersbegrenzung, auch wenn die Fragen für Befragte im Alter von mindestens 10-11 Jahren entwickelt wurden.

### Missbrauch

Jede/r Nutzer/in verpflichtet sich, den üblichen Gebrauch der Anwendung gemäß dem gesunden Menschenverstand zu respektieren. Insbesondere verfolgt die Anwendung eine positive Logik und ein formatives Ziel, nämlich die Verbesserung der Lehr- und Lernmethoden. Sie zielt nicht auf eine normative Bewertung des Unterrichts oder der Lehrkräfte ab. Sie zielt auch nicht auf die Stigmatisierung einer Schülerin oder eines Schülers oder einer Kategorie von Schülerinnen und Schülern ab. Darüber hinaus setzt die Verwendung des Instruments die Verwendung einer angemessenen Sprache voraus. Auch sollte der Kontakt zu den Autorinnen und Autoren der Plattform nicht missbraucht werden.

### Persönliche Daten und Anonymisierung

Persönliche Daten werden nur im Zusammenhang mit der Plattform verwendet. Sie werden niemals an Dritte weitergegeben oder zu Werbezwecken verkauft.

Um eine hohe Authentizität der Antworten zu gewährleisten, sind alle Umfragen standardmäßig anonymisiert. Die persönlichen Daten der Nutzer/innen werden beim Versenden der Antworten nicht weitergegeben, sodass es dem Initiator einer Umfrage (Lehrer/in, Dozent/in, Referent/in...) nicht möglich ist, die Befragten zu identifizieren.

### Recht auf Vergessenwerden

Nutzer/innen können das Recht auf Vergessenwerden einfordern. Dazu wenden sie sich schriftlich (siehe Kontaktadresse in der Anwendung) an die Administratorin, die das Konto und alle in der Anwendung gespeicherten Daten, die sie betreffen, endgültig löschen wird.
Sie können Ihr Konto auch selbstständig über die Seite „Mein Konto“ löschen.

### Haftung

OURA² ist ein kostenloser Dienst und garantiert nicht den jederzeitigen Zugang zur Plattform, den Fortbestand seines Dienstes sowie die Erhaltung der Daten, trotz der Bemühungen der Autorinnen und Autoren der Plattform. Im Falle eines böswilligen Angriffs werden die Nutzer/innen so schnell wie möglich informiert.

### Verzicht

Falls der/die Nutzer/in die Allgemeinen Geschäftsbedingungen nicht mehr akzeptieren möchte, kann er/sie jederzeit auf seine/ihre Zustimmung verzichten. Dazu muss er/sie die Administratorin kontaktieren (siehe Kontaktadresse in der Plattform).

Dieser Verzicht bedeutet de facto den Verlust des Zugangs zu allen von der OURA²-Anwendung angebotenen Diensten.

### Nichteinhaltung der Allgemeinen Geschäftsbedingungen

Im Falle der Nichteinhaltung der Allgemeinen Geschäftsbedingungen durch eine/n Nutzer/in behält sich OURA² das Recht vor, das Konto zu sperren oder zu löschen.

### Aktualisierung der Allgemeinen Geschäftsbedingungen

Aus Gründen des Datenschutzes und um den bestmöglichen Service zu bieten, behält sich OURA² das Recht vor, die vorliegenden Allgemeinen Nutzungsbedingungen zu ändern. Bei einer Änderung werden die NutzerInnen benachrichtigt und können diese akzeptieren oder ablehnen.

### Index

Für weitere Informationen zu den Allgemeinen Geschäftsbedingungen können Sie Kontakt aufnehmen mit: *pierre-francois.coen(at)unifr.ch* oder *lea.briguet(at)unifr.ch*.
