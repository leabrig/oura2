---
title: 'Conditions générales'
---

En acceptant les conditions générales d'utilisation, vous vous engagez à respecter tous les points mentionnés dans ce document.

### Assise institutionnelle
L'application OURA² (pour OUtil de Régulation des Activités d'enseignement/apprentissage) a été développée par Lea Briguet dans le cadre des activités scientifiques du CERF (Centre d'Enseignement et de Recherche pour la Formation à l'enseignement au secondaire) du l’Université de Fribourg (Suisse) sous la direction de Pierre-François Coen. (www.oura2.ch)

Le développement et l'utilisation des données produites par l'application OURA² ainsi que toute modification de son contenu est soumise au contrôle exclusif de la développeuse (Lea Briguet) et du responsable scientifique du projet (Pierre-François Coen).

### Respect des auteur·trice·s

Toute référence à la plateforme OURA2  et/ou à son utilisation dans des publications grand public, professionnelles ou scientifiques doit être faite :

&ensp; Coen, P.-F. & Briguet, L. (2023). OURA2 [plateforme internet]. Centre d’enseignement et de recherche pour la formation au secondaire. Université de Fribourg. https://www.oura2.ch/

### Services

Les services proposés par OURA² permettent de collecter des données relatives aux perceptions exprimées par des sujets (élèves, étudiant.e.s, auditeur.trice.s …) à la suite d'une expérience d'apprentissage. Dans le but de renseigner l'initiateur·trice d'un dispositif d'enseignement - apprentissage (enseignant·e, professeur·e, conférencier·ière …) et de lui permettre de l'améliorer, ces données ciblent différents domaines et sont collectées auprès des apprenant·e·s ou des participant·e·s au moyen de sondages personnalisables. Après analyse, ces données sont restituées sous différentes formes (graphiques, bases de données) à l'initiateur·trice du sondage qui peut s'en servir à des fins autoévaluatives. L'utilisation de OURA² est gratuite.

### Public cible

L'application OURA² s'adresse à toute personne qui élabore des dispositifs d'enseignement - apprentissage ou des activités (enseignant·e·s, professeur·e·s, conférencier.ière.s …) et qui souhaite les améliorer en s'appuyant sur les perceptions des apprenant·e·s et/ou participant·e·s (élèves, étudiant.e.s, auditeur.trice.s ...). Aucune limite d'âge n'est imposée même si les questions ont été élaborées pour des répondant·e·s de 10-11 ans minimum.

### Abus

Chaque utilisateur·trice s'engage à respecter l'usage usuel de l'application selon le bon sens commun. En particulier, l'application s'inscrit dans une logique positive et poursuit un but formatif d'amélioration des dispositifs d'enseignement - apprentissage. Elle ne vise pas l'évaluation normative des enseignements ou des enseignant·e·s. Elle ne vise pas non plus la stigmatisation d'un·e élève ou une catégorie d'élèves. En outre, l'usage de l'outil implique l’utilisation d’un langage approprié. Il convient aussi de ne pas abuser du contact auprès des auteur·trice·s de la plateforme.

### Données personnelles et anonymisation

Les données personnelles ne sont utilisées que dans le cadre de l'application. Elles ne sont jamais transmises à des tiers ou vendues à des fins publicitaires.

Afin de garantir une bonne authenticité au niveau des réponses, tous les sondages sont, par défaut, anonymisés. Les données personnelles des utilisateur·trice·s ne sont pas transmises lors de l'envoi de réponses, il n'est ainsi pas possible à l'initiateur·trice d'un sondage (enseignant·e·s, professeur·e·s, conférencier·ière…) d'identifier les répondant·e·s.

### Droit à l'oubli

Les utilisateur·trice·s peuvent réclamer le droit à l'oubli. Pour ce faire, ils contactent par écrit (voir adresse de contact dans l'application) l'administratrice qui supprimera définitivement le compte et toutes les données stockées dans l'application les concernant.

### Responsabilité

OURA² est un service gratuit et ne garantit pas l'accès en tout temps à l'application, la pérennité de son service, ainsi que la préservation des données, malgré les efforts des auteur·trice·s de la plateforme. En cas d'attaque malveillante, les utilisateur·trice·s sont informé·e·s dans les plus brefs délais.

### Renonciation

Dans le cas où l'utilisateur·trice ne souhaite plus accepter les conditions générales, il peut à tout moment renoncer à son accord. Pour ce faire, il doit contacter l'administratrice (voir l'adresse de contact dans l'application).

Cette renonciation implique de facto la perte d'accès à tous les services proposés par l'application OURA².

### Non-respect des conditions générales

En cas de non-respect des conditions générales de la part d'un·e utilisateur·trice, OURA² se réserve le droit de bloquer son compte ou de le supprimer.

### Mise à jour des conditions générales

Par souci de protection des données et pour offrir les meilleurs services possibles, OURA² se réserve le droit de modifier les présentes conditions générales d'utilisation. Lors d'une modification, les utilisateur·trice·s sont notifié·e·s et peuvent les accepter ou décliner.

### Index

Pour toutes informations complémentaires concernant les conditions générales, vous pouvez prendre contact avec : *pierre-francois.coen(arobase)unifr.ch* ou *lea.briguet(arobase)unifr.ch*.
