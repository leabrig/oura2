---
title: 'General terms and conditions'
---

By accepting the general conditions of use, you agree to comply with all the points mentioned in this document.

### Institutional basis
The OURA² application (for OUtil de Régulation des Activités d'enseignement/apprentissage) was developed by Lea Briguet as part of the scientific activities of the CERF (Centre d'Enseignement et de Recherche pour la Formation à l'enseignement au secondaire) of the University of Fribourg (Switzerland) under the direction of Pierre-François Coen ([www.oura2.ch](https://www.oura2.ch)).

The development and use of the data produced by the OURA² application, as well as any modification of its content, is subject to the exclusive control of the developer (Lea Briguet) and the project's scientific manager (Pierre-François Coen).

### Respect for authors

Any reference to the OURA2 platform and/or its use in public, professional or scientific publications must be made :

&ensp; Coen, P.-F. & Briguet, L. (2023). OURA2 [internet platform]. Centre d'enseignement et de recherche pour la formation au secondaire. University of Fribourg. https://www.oura2.ch/

### Services

OURA²'s services enables to collect data on the perceptions expressed by subjects (pupils, students, listeners, etc.) following a learning experience. With the aim of informing the initiator of a teaching-learning device (teacher, lecturer, etc.) and enabling him or her to improve it, this data targets different areas and is collected from learners or participants by means of customizable surveys. After analysis, this data is returned in various forms (graphs, databases) to the survey initiator, who can use it for self-evaluation purposes. Use of OURA² is free of charge.

### Target audience

The OURA² application is aimed at anyone who designs teaching-learning systems or activities (teachers, lecturers, etc.) and wishes to improve them based on the perceptions of learners and/or participants (pupils, students, auditors, etc.). No age limit is imposed, even though the questions have been designed for respondents aged 10-11 or over.

### Abuse

Each user undertakes to respect the usual use of the application in accordance with common sense. In particular, the application is designed to be positive and to help improve teaching-learning methods. It is not aimed at normative evaluation of teaching or teachers. Nor does it aim to stigmatize any particular student or category of students. Furthermore, the use of this tool implies the use of appropriate language. It is also important not to abuse contact with the platform's authors.

### Personal data and anonymization

Personal data are only used for the purposes of the application. It is never passed on to third parties or sold for advertising purposes.

To guarantee the authenticity of responses, all surveys are anonymized by default. Users' personal data is not transmitted when responses are sent, so it is not possible for the initiator of a survey (teacher, lecturer, etc.) to identify respondents.

### Right to be forgotten

Users can request the right to be forgotten. To do so, they must contact the administrator in writing (see contact address in the application), who will permanently delete the account and all data stored in the application concerning them.
They can also delete their account and all related data directly from the profile page.

### Liability

OURA² is a free service and, despite the best efforts of the platform's authors, does not guarantee continuous access to the application, continuity of service or data preservation. In the event of a malicious attack, users will be informed as soon as possible.

### Waiver

Should the user no longer wish to accept the general terms and conditions, they may withdraw their agreement at any time. To do so, they must contact the administrator (see contact address in the application).

This de facto implies loss of access to all OURA² services.

### Non-compliance with terms and conditions

OURA² reserves the right to block or delete a user's account in the event of non-compliance with the terms and conditions.

### Updating of terms and conditions

OURA² reserves the right to modify these terms and conditions of use in the interests of data protection and in order to provide the best possible service. Users will be notified of any changes and may accept or decline them.

### Index

For further information about these terms and conditions, please contact : *pierre-francois.coen(at)unifr.ch* or *lea.briguet(at)unifr.ch*.