---
title: "Prise en main d'OURA²"
---
### Accès à la plateforme

L’accès à la plateforme se fait en suivant ce lien : [oura2.ch](https://www.oura2.ch/)

### Création d'un compte utilisateur

Toute personne qui souhaite proposer des sondages (enseignant·e·s, enseignant·e·s en formation, conférencier·ère·s …) doit créer un compte utilisateur. Pour répondre à un sondage, il n’y a pas besoin de compte. Pour créer un compte :

1. Cliquez sur "Se connecter"
2. Cliquez sur "Créer un compte"
3. Complétez les rubriques
4. Cliquez sur créer un compte
5. Une fois que le compte est créé, cliquez sur "Se connecter"

### Se connecter

La connexion permet d’entrer sur la plateforme et de créer des sondages. Pour se connecter :

1. Cliquez sur "Se connecter"
2. Complétez les rubriques
3. Cliquez sur "Se connecter"

### Le tableau de bord

Le tableau de bord présente trois options :

![Options tableau de bord](/images/tableau-de-bord.png "Tableau de bord")

“Mes sondages” permet d’accéder à la liste de tous les sondages que vous avez créés

“Analyser mes sondages” permet d’accéder à l’interface d’analyse des sondages

“Créer un nouveau sondage” permet de créer un nouveau questionnaire

### Créer un nouveau sondage

Les sondages OURA² sont destinés à recueillir l’avis des répondant·e·s en lien avec une activité ou une expérience précédemment vécue. L’utilisateur qui veut construire un sondage doit prendre en compte cet aspect parce que les questions font toujours référence à “une activité” réalisée par les répondant·e·s.

#### Sélectionner les domaines et dimensions

1. Choisissez “Créer un sondage” dans le tableau de bord
2. Donnez un nom au sondage
3. Parmi les 9 domaines à disposition, choisissez les dimensions qui vous intéressent (cochez la case). Pour chaque dimension deux questions prédéfinies apparaissent et vous permettent de voir si c’est bien les questions que vous voulez poser.
   ![Choix des dimensions](/images/nouveau-sondage.png "Choix des dimensions")
4. Cliquez sur "Suivant" pour poursuivre

#### Créer une variable discriminante

La page suivante vous permet de créer des variables discriminantes pour regrouper les réponses selon des groupes de répondant·e·s (hommes, femmes / groupes 1, groupe 2, groupe 3 / élèves faibles, élèves moyens, élèves forts / etc.). Pour cela :

1. Cliquez sur "Ajouter"
2. Donnez un nom à votre variable
3. Donnez les modalités de la variable (au maximum six)
   ![Création variable discriminante](/images/nouvelle-variable.png "Créer une nouvelle variable discriminante")
4. Cliquez sur "Créer"

N.B. : Les variables créées restent toujours disponibles.

#### Publier le sondage

Une fois que vous avez effectué ces différentes étapes, votre sondage est prêt et vous pouvez communiquer le QR-Code ou le lien associé. Les répondant·e·s accéderont directement au sondage sans avoir besoin de s’inscrire ou de créer un compte.

### Analyser mes sondages

L’option “Analyser mes sondages” accessible via le tableau de bord vous permet de visualiser les résultats pour en discuter (ou non) avec les répondant·e·s. L’interface se divise en trois parties : à gauche, la partie qui vous permet de trouver et sélectionner vos sondages, à droite en haut, la surface de présentation des résultats et en bas, les boutons qui vous permettent de choisir les variables discriminantes et les dimensions et le bouton qui réactualise les analyses.

#### Visualiser les résultats

1. Choisissez le questionnaire qui vous intéresse (les histogrammes apparaissent)
2. Sélectionner le ou les dimensions que vous souhaitez voir
3. Sélectionner la variable discriminante qui vous intéresse (au maximum deux à la fois)
   ![Représentation graphique d'un sondage](/images/analyse-dimension.png "Représentation graphique de l'analyse d'un sondage")

#### Comparer des dimensions

Si deux ou plusieurs questionnaires sondent la même dimension, il est possible de comparer les résultats.

1. Sélectionner les questionnaires que vous souhaitez comparer
2. Sélectionnez la dimension qui vous intéresse (une à la fois)
   ![Représentation graphique de deux sondages](/images/analyse-plusieurs-sondages.png "Représentation graphique de deux sondages")

### Mes sondages

Le bouton “Mes sondages” accessible depuis le tableau de bord permet d’accéder à la liste de tous les sondages créés. Les 5 boutons disponibles permettent 5 actions.

![Voir sondage](/images/display.png "Voir sondage")
Voir les questions du sondage, le QR-Code et le lien qui permettent d’accéder directement aux questions. La moyenne de chaque réponse apparaît en face de chaque question. Le nombre de répondant·e·s apparaît en face de chaque variable discriminante.

![Accéder aux analyses](/images/analyse.png "Accéder aux analyses")
Accéder à l’interface d’analyse du sondage.

![Télécharger le CSV](/images/save.png "Télécharger le CSV")
Permettre de télécharger les réponses dans un format (.csv) utilisable par des applications statistiques.

![Copier le sondage](/images/duplicate.png "Dupliquer le sondage")
Dupliquer le sondage sélectionné.

![Supprimer le sondage](/images/trash.png "Supprimer le sondage")
Supprimer le sondage sélectionner.

N.B. : Toutes les données collectées sont sauvegardées sur un serveur sécurisé en Suisse.

### Profil

Le bouton “Profil” permet de gérer les données relatives à votre profil d’utilisateur. Vous pouvez modifier l’email de référence ou votre mot de passe.

### Domaines et dimensions proposées dans OURA²

![Tableau des dimensions](/images/tableau-dimension.png "Tableau des dimensions")

### Contact

**Pierre-François Coen**, professeur ordinaire, Université de Fribourg, *pierre-francois.coen(arobase)unifr.ch*

**Lea Briguet**, assistante - développeuse, Université de Fribourg, *lea.briguet(arobase)unifr.ch*
