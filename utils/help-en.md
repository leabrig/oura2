---
title: "Getting to know OURA²"
---

### Platform access

To access the platform, follow this link: [oura2.ch](https://www.oura2.ch/)

### Creating a user account

Anyone wishing to submit surveys (teachers, trainee teachers, lecturers, etc.) must create a user account. You don't need an account to take a survey. To create an account :

1. Click on “Login” in the navigation bar
2. Click on “Create an account”.
3. Fill in the fields
4. Click on “Create an account
5. Once the account has been created, click on “Login”.

### Login

Logging in allows you to enter the platform and create surveys. To log in :

1. Click on “Login” in the navigation bar
2. Fill in the fields
3. Click on “Login”.

### The dashboard

The dashboard has three options:

![Dashboard options](/images/dashboard-en.png "Dashboard")

“My surveys” gives access to a list of all the surveys you have created.

“ Analyse my surveys” gives access to the survey analysis interface

“Create a new survey” to create a new survey

### Create a new survey

OURA² surveys are designed to gather respondents' opinions in relation to a previous activity or experience. Users wishing to build a survey must take this aspect into account, as the questions always refer to “an activity” carried out by the respondents.

#### Select domains and dimensions

1. Select “Create a survey” in the dashboard
2. Give the survey a name
3. From the 9 fields available, choose the dimensions that interest you (tick the box). For each dimension, two predefined questions appear, allowing you to see if these are the questions you want to ask.
   ![Dimension selection](/images/new-survey.png "Dimension selection")
4. Click “Next” to continue

#### Create a discriminant variable

The next page lets you create discriminant variables to group responses according to respondent groups (men, women / group 1, group 2, group 3 / weak students, average students, strong students / etc.). To do this:

1. Click on “Add”.
2. Give your variable a name
3. Give the modalities of the variable (maximum six)
   ![Create discriminant variable](/images/new-variable.png "Create a new discriminant variable")
4. Click on “Create”.

N.B.: Created variables are available for the next surveys.

#### Publish the survey

Once you have completed these steps, your survey is ready and you can share the QR-Code or link. Respondents will be able to access the survey directly without having to register or create an account.

### Analyse my surveys

The ‘Analyse my surveys’ option accessible via the dashboard, allows you to view the results and discuss them (or not) with respondents. The interface is divided into three parts: on the left, the part that allows you to find and select your surveys, on the right at the top, the results presentation area and at the bottom, the buttons that allow you to choose the discriminating variables and dimensions and the button that updates the analyses.

#### View results

1. Choose the questionnaire you are interested in (the histograms appear)
2. Select the dimension(s) you wish to view
3. Select the discriminant variable you are interested in (maximum two at a time)
   ![Graphical representation of a survey](/images/dimension-analysis.png 'Graphical representation of the analysis of a survey')

#### Comparing dimensions

If two or more questionnaires survey the same dimension, it is possible to compare the results.

1. Select the questionnaires you wish to compare
2. Select the dimension you are interested in (one at a time)
   ![Graphical representation of two surveys](/images/analysis-multiple-surveys.png "Graphical representation of two surveys")

### My surveys

The ‘My surveys’ button, accessible from the dashboard, gives you access to a list of all the surveys you have created. The 5 buttons available allow 5 actions.

![View survey](/images/display.png "View survey")
View the survey questions, the QR-Code and the link giving direct access to the questions. The average for each answer appears next to each question. The number of respondents appears next to each discriminating variable.

![Go to analyses](/images/analyse.png "Go to analyses")
Access to the survey analysis interface.

![Download CSV](/images/save.png "Download CSV")
Allows you to download responses in a format (.csv) that can be used by statistical applications.

![Copy survey](/images/duplicate.png "Duplicate survey")
Duplicates the selected survey.

![Delete survey](/images/trash.png "Delete survey")
Delete the selected survey.

N.B.: All data collected is saved on a secure server in Switzerland.

### Profile

The ‘Profile’ button allows you to manage the data relating to your user profile. You can change the reference email, your password, the language of use and delete your account and associated data.

### OURA² domains and dimensions

![Table of dimensions](/images/dimension-table.png "Table of dimensions")

### Contact

**Pierre-François Coen**, full professor, University of Fribourg, *pierre-francois.coen(at)unifr.ch*.

**Lea Briguet**, Assistant Developer, University of Fribourg, *lea.briguet(at)unifr.ch*.
