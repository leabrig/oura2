---
title: "À propos d'OURA²"
---

## Un outil pour favoriser la régulation des enseignements

Donnant suite au développement de la première [plateforme OURA](https://pfcoen.ch/data%e2%80%a22%e2%80%a2perso/), le projet OURA² s’inscrit dans le cadre du développement professionnel des enseignant·e·s. Il part du principe qu’un enseignant·e peut questionner ses pratiques professionnelles et développer sa réflexivité en s’appuyant non seulement sur ses propres impressions ou perceptions, sur les traces des activités des élèves ou encore sur les résultats formels des évaluations, mais également en prenant en compte les avis des élèves eux-mêmes. Inscrite dans la logique du mentorat inversé (Brondyk et Searby, 2013; Chen, 2013; Zauchner-Studnicka, 2017) et du paradigme des contributions réciproques (Coen et al. sous presse), cette logique consiste à questionner les apprenant·e·s eux-mêmes, non pas sur la qualité globale des enseignements reçus, mais plus spécifiquement sur les activités d’enseignement / apprentissage proposées par leur enseignant·e au quotidien.

La plateforme [OURA²](https://www.oura2.ch/) (**OU**til de **R**égulation des **A**ctivités d’enseignement / apprentissage) a été développée pour permettre aux enseignant·e·s de collecter des données auprès de leurs élèves au moyen de micro-sondages. Elle permet d’interroger les apprenant·e·s sur leurs perceptions de l’utilité d’une activité, du soutien qu’ils ont reçu (ou non), de leur sentiment de compétence, sur les éventuels apports d’un travail en groupe, etc. La plateforme propose 9 domaines liés à l’enseignement et répartis en 38 dimensions. L’intérêt du dispositif réside dans le fait que ces dimensions touchent des aspects sur lesquels l’enseignant·e peut directement rétroagir lors d’un prochain cours.

**Figure nr. 1** : Les domaines et dimensions proposées dans OURA²

![Tableau des dimensions](/images/tableau-dimension.png "Tableau des dimensions")

Concrètement, après avoir créé un compte, l’utilisateur·trice sélectionne en fonction des activités qu’il propose aux apprenant·e·s, la(les) dimension(s) qui l’intéresse(nt). La plateforme génère automatiquement un questionnaire immédiatement accessible au moyen d’un QR-Code. Les apprenant·e·s répondent aux questions et OURA² renvoie les résultats sous forme de graphiques sur la base desquels une discussion peut être initiée entre apprenant·e·s et enseignant·e. Partant de là, ce dernier dispose d’informations précieuses, complémentaires à celles qu’il récolte déjà, et susceptibles de lui permettre d’ajuster ou non les activités qu’il propose à ses élèves eux-mêmes.

Depuis 2023, le projet OURA² est déployé dans le cadre de la formation des enseignant·e·s parce qu’il propose aussi des dimensions directement liées à une douzaine d’habiletés professionnelles (données de consignes, gestion du temps, qualité des supports, feed-back …). Les résultats des micro-sondages peuvent dès lors servir de supports non seulement dans le cadre de la classe, mais aussi pour soutenir les entretiens de débriefing effectués dans les contextes d’alternance.

### License

La plateforme OURA² © 2024 est sous licence [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International](https://creativecommons.org/licenses/by-nc-sa/4.0/). Icônes par [Font awesome](https://fontawesome.com/).

La plateforme et ses fonctionnalités sont accessibles gratuitement. Elle a été développée en [open access](https://gitlab.com/leabrig/oura2). Son utilisation fait l’objet de plusieurs communications scientifiques.

### Équipe

**Pierre-François Coen**, professeur ordinaire, Université de Fribourg, *pierre-francois.coen(arobase)unifr.ch*

**Lea Briguet**, assistante - développeuse, Université de Fribourg, *lea.briguet(arobase)unifr.ch*