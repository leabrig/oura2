---
title: 'Über OURA²'
---

## Ein Werkzeug zur Förderung der Regulierung des Unterrichts

Das Projekt OURA², das auf der Entwicklung der ersten [OURA-Plattform](https://pfcoen.ch/data%e2%80%a22%e2%80%a2perso/) aufbaut, ist Teil der beruflichen Entwicklung von Lehrpersonen. Es geht davon aus, dass Lehrpersonen ihre berufliche Praxis hinterfragen und ihre Reflexivität entwickeln können, indem sie sich nicht nur auf ihre eigenen Eindrücke und Wahrnehmungen, auf die Aufzeichnungen der Schüleraktivitäten oder auf die formalen Ergebnisse von Beurteilungen stützen, sondern auch die Meinungen der Schülerinnen und Schüler selbst berücksichtigen. In Anlehnung an die Logik des umgekehrten Mentorings (Brondyk und Searby, 2013; Chen, 2013; Zauchner-Studnicka, 2017) und des Paradigmas der gegenseitigen Beiträge (Coen et al. im Druck) besteht diese Logik darin, die Lernenden selbst zu befragen, und zwar nicht zur Gesamtqualität des erhaltenen Unterrichts, sondern speziell zu den Lehr-/Lernaktivitäten, die von ihren Lehrkräften im Alltag angeboten werden.

Die Plattform [OURA²](https://www.oura2.ch/) (**OU**til de **R**égulation des **A**ctivités d’enseignement / apprentissage) wurde entwickelt, um Lehrkräften die Möglichkeit zu geben, mithilfe von Mikroumfragen Daten von ihren Schülerinnen und Schülern zu sammeln. Die Plattform ermöglicht es, die Lernenden zu ihrer Wahrnehmung des Nutzens einer Aktivität, der erhaltenen (oder nicht erhaltenen) Unterstützung, ihres Kompetenzgefühls, des möglichen Beitrags einer Gruppenarbeit usw. zu befragen. Die Plattform bietet 9 unterrichtsrelevante Bereiche, die in 38 Dimensionen unterteilt sind. Das Interessante an der Plattform ist, dass diese Dimensionen Aspekte betreffen, auf die die Lehrkraft in einer nächsten Unterrichtsstunde direkt zurückgreifen kann.

**Abbildung Nr. 1**: Die in OURA² vorgeschlagenen Bereiche und Dimensionen

![Dimensionstabelle](/images/dimensionstabelle.png "Dimensionstabelle")

Konkret bedeutet dies, dass der/die Benutzer/in nach der Erstellung eines Kontos je nach den Aktivitäten, die er/sie den Lernenden anbietet, die Dimension(en) auswählt, die ihn/sie interessieren. Die Plattform generiert automatisch einen Fragebogen, der über einen QR-Code sofort zugänglich ist. Die Lernenden beantworten die Fragen und OURA² sendet die Ergebnisse in Form von Grafiken zurück, auf deren Grundlage eine Diskussion zwischen Lernenden und Lehrenden eingeleitet werden kann. Auf diese Weise erhalten die Lehrkräfte wertvolle Informationen, die sie zu den bereits gesammelten Informationen ergänzen und die es ihnen ermöglichen, die Aktivitäten, die sie ihren Schülern anbieten, entsprechend anzupassen.

Seit 2023 wird das Projekt OURA² im Rahmen der Lehrerausbildung eingesetzt, da es auch Dimensionen vorschlägt, die direkt mit rund zwölf beruflichen Fertigkeiten verbunden sind (Anweisungen geben, Zeitmanagement, Qualität der Materialien, Feedback ...). Die Ergebnisse der Mikro-Umfragen können daher nicht nur im Klassenzimmer, sondern auch bei der Nachbesprechung von Gesprächen im Rahmen der alternierenden Ausbildung als Unterstützung dienen.

### Lizenz

Die Plattform OURA² © 2024 steht unter der Lizenz [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International](https://creativecommons.org/licenses/by-nc-sa/4.0/). Piktogramme von [Font awesome](https://fontawesome.com/).

Die Plattform und ihre Funktionen sind kostenlos zugänglich. Sie wurde in [open access](https://gitlab.com/leabrig/oura2) entwickelt. Ihre Nutzung ist Gegenstand mehrerer wissenschaftlicher Mitteilungen.

### Team

**Pierre-François Coen**, ordentlicher Professor, Universität Freiburg, *pierre-francois.coen(at)unifr.ch*.

**Lea Briguet**, Assistentin - Entwicklerin, Universität Freiburg, *lea.briguet(at)unifr.ch*.