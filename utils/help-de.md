---
title: "Erste Schritte mit OURA²"
---
### Zugang zur Plattform

Der Zugang zur Plattform erfolgt über diesen Link: [oura2.ch](https://www.oura2.ch/)

### Erstellen eines Benutzerkontos

Jede Person, die Umfragen anbieten möchte (Lehrpersonen, Lehrpersonen in Ausbildung, Referentinnen und Referenten ...), muss ein Benutzerkonto erstellen. Um an einer Umfrage teilzunehmen, ist kein Benutzerkonto erforderlich. Um ein Konto zu erstellen:

1. Klicken Sie auf „Anmelden“ in der Navigationsleiste.
2. Klicken Sie auf „Konto erstellen“.
3. Füllen Sie die Felder aus.
4. Klicken Sie auf Konto erstellen.
5. Sobald das Konto erstellt ist, klicken Sie auf „Anmelden“.

### Einloggen

Das Einloggen ermöglicht es Ihnen, die Plattform zu betreten und Umfragen zu erstellen. Um sich einzuloggen:

1. Klicken Sie auf „Einloggen“ in der Navigationsleiste.
2. Füllen Sie die Felder aus.
3. Klicken Sie auf „Einloggen“.

### Das Dashboard

Das Dashboard zeigt drei Optionen an:

![Dashboard-Optionen](/images/dashboard-de.png "Dashboard")

„Meine Umfragen“ ermöglicht den Zugriff auf eine Liste aller Umfragen, die Sie erstellt haben.

„Meine Umfragen analysieren“ ermöglicht den Zugriff auf die Seite zur Analyse von Umfragen.

„Neue Umfrage erstellen“ ermöglicht es Ihnen, einen neuen Fragebogen zu erstellen.

### Erstellen Sie eine neue Umfrage

OURA²-Umfragen sind dazu gedacht, die Meinung der Befragten in Bezug auf eine Aktivität oder eine zuvor gemachte Erfahrung einzuholen. Der Benutzer, der eine Umfrage erstellen möchte, muss diesen Aspekt berücksichtigen, da sich die Fragen immer auf „eine Aktivität“ beziehen, die von den Befragten durchgeführt wurde.

#### Wählen Sie die Bereiche und Dimensionen aus.

1. Wählen Sie im Dashboard „Umfrage erstellen“.
2. Geben Sie der Umfrage einen Namen.
3. Wählen Sie aus den 9 zur Verfügung stehenden Bereichen die Dimensionen aus, die Sie interessieren (Kästchen ankreuzen). Für jede Dimension erscheinen zwei vordefinierte Fragen, anhand derer Sie sehen können, ob es sich um die Fragen handelt, die Sie stellen möchten.
   ![Auswahl der Dimensionen](/images/neue-umfrage.png "Auswahl der Dimensionen")
4. Klicken Sie auf „Weiter“, um fortzufahren.

#### Erstellen Sie eine Diskriminanzvariable.

Auf der nächsten Seite können Sie diskriminierende Variablen erstellen, um die Antworten nach Gruppen von Befragten zu gruppieren (Männer, Frauen / Gruppe 1, Gruppe 2, Gruppe 3 / schwache Schüler, mittlere Schüler, starke Schüler / usw.). Hierzu:

1. Klicken Sie auf „Hinzufügen“.
2. Geben Sie Ihrer Variable einen Namen.
3. Geben Sie die Modalitäten der Variable an (maximal sechs).
   ![Diskriminierende Variable erstellen](/images/neue-variable.png "Eine neue diskriminierende Variable erstellen")
4. Klicken Sie auf „Erstellen“.

N.B.: Die erstellten Variablen bleiben immer verfügbar.

#### Veröffentlichen Sie die Umfrage.

Wenn Sie diese Schritte durchgeführt haben, ist Ihre Umfrage fertig und Sie können den QR-Code oder den zugehörigen Link kommunizieren. Die Befragten werden direkt auf die Umfrage zugreifen, ohne sich anmelden oder ein Konto erstellen zu müssen.

### Meine Umfragen analysieren

Die Option „Meine Umfragen analysieren“, die Sie über das Dashboard erreichen, ermöglicht es Ihnen, die Ergebnisse einzusehen, um sie mit den Befragten zu diskutieren (oder auch nicht). Die Benutzeroberfläche ist in drei Teile gegliedert: links der Bereich, in dem Sie Ihre Umfragen finden und auswählen können, rechts oben die Präsentationsfläche für die Ergebnisse und unten die Schaltflächen, mit denen Sie die diskriminierenden Variablen und Dimensionen auswählen können, sowie die Taste, mit der die Analysen erneut aktualisiert werden.

#### Ergebnisse ansehen

1. Wählen Sie den Fragebogen, der Sie interessiert (die Histogramme werden angezeigt).
2. Wählen Sie die Dimension(en), die Sie sehen möchten.
3. Wählen Sie die Diskriminanzvariable, die Sie interessiert (maximal zwei gleichzeitig).
   ![Grafische Darstellung einer Umfrage](/images/dimensionsanalyse.png "Grafische Darstellung der Analyse einer Umfrage")

#### Vergleichen von Dimensionen

Wenn zwei oder mehr Fragebögen die gleiche Dimension abfragen, können Sie die Ergebnisse vergleichen.

1. Wählen Sie die Fragebögen, die Sie vergleichen möchten.
2. Wählen Sie die Dimension, die Sie interessiert (eine nach der anderen).
   ![Grafische Darstellung von zwei Umfragen](/images/mehrere-dimensionenanalyse.png "Grafische Darstellung von zwei Umfragen")

### Meine Umfragen

Über die Schaltfläche „Meine Umfragen“, die vom Dashboard aus zugänglich ist, können Sie auf eine Liste aller erstellten Umfragen zugreifen. Die 5 verfügbaren Schaltflächen ermöglichen 5 Aktionen.

![Umfrage ansehen](/images/display.png "Umfrage ansehen")
Sehen Sie die Fragen der Umfrage, den QR-Code und den Link, mit dem Sie direkt auf die Fragen zugreifen können. Der Durchschnitt jeder Antwort erscheint vor jeder Frage. Die Anzahl der Befragten erscheint vor jeder diskriminierenden Variable.

![Zu den Analysen](/images/analyse.png "Zu den Analysen")
Rufen Sie die Benutzeroberfläche für die Analyse der Umfrage auf.

![CSV herunterladen](/images/save.png "CSV herunterladen")
Ermöglicht das Herunterladen der Beantwortungen in einem Format (.csv), das von statistischen Anwendungen verwendet werden kann.

![Umfrage kopieren](/images/duplicate.png "Umfrage duplizieren")
Dupliziert die ausgewählte Umfrage.

![Umfrage löschen](/images/trash.png "Umfrage löschen")
Löscht die ausgewählte Umfrage.

N.B.: Alle gesammelten Daten werden auf einem sicheren Server in der Schweiz gespeichert.

### Profil

Über die Schaltfläche „Profil“ können Sie die Daten zu Ihrem Nutzerprofil verwalten. Sie können die Referenz-E-Mail, Ihr Passwort und die Benutzersprache ändern sowie Ihr Konto und die damit verbundenen Daten löschen.

### Bereiche und Dimensionen, die in OURA² angeboten werden.

![Tabelle der Dimensionen](/images/dimensionstabelle.png "Tabelle der Dimensionen")

### Kontakt

**Pierre-François Coen**, ordentlicher Professor, Universität Freiburg, *pierre-francois.coen(at)unifr.ch*.

**Lea Briguet**, Assistentin - Entwicklerin, Universität Freiburg, *lea.briguet(arobase)unifr.ch*.