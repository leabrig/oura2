---
title: "About OURA²"
---

## A tool to support teaching regulation

Following on the development of the first [OURA platform](https://pfcoen.ch/data%e2%80%a22%e2%80%a2perso/), the OURA² project is part of the professional development of teachers. It is based on the principle that teachers can question their professional practices and develop their reflexivity by drawing not only on their own impressions or perceptions, on traces of students' activities or on the formal results of assessments, but also by taking into account the opinions of the students themselves. Based on the logic of reverse mentoring (Brondyk and Searby, 2013; Chen, 2013; Zauchner-Studnicka, 2017) and the paradigm of reciprocal contributions (Coen et al. in press), this logic involves questioning the learners themselves, not about the overall quality of the teaching they receive, but more specifically about the teaching/learning activities proposed by their teacher on a daily basis.

The [OURA²](https://www.oura2.ch/) platform (**OU**til de **R**égulation des **A**ctivités d'enseignement / apprentissage) has been developed to enable teachers to collect data from their students by means of micro-surveys. It enables learners to be questioned about their perceptions of the usefulness of an activity, the support they received (or didn't receive), their sense of competence, the possible benefits of group work, and so on. The platform offers 9 teaching-related areas, divided into 38 dimensions. The beauty of the system lies in the fact that these dimensions touch on aspects that the teacher can directly address in a future lesson.

**Figure nr. 1**: OURA² domains and dimensions

![Dimensions table](/images/dimension-table.png "Dimensions table")

In concrete terms, after creating an account, the user selects the dimension(s) of interest to them, based on the activities they propose to their learners. The platform automatically generates a survey that is immediately accessible by means of a QR-Code. Learners answer the questions and OURA² returns the results in the form of graphs, on the basis of which a discussion can be initiated between learner and teacher. This provides the teacher with valuable additional information, which can be used to adjust the activities they propose to their students.

Since 2023, the OURA² project has been deployed as part of teacher training, because it also offers dimensions directly linked to a dozen professional skills (giving instructions, time management, quality of materials, feedback, etc.). The results of the micro-surveys can therefore be used not only in the classroom, but also to support debriefing interviews carried out in alternation contexts.
### License

The OURA² © 2024 platform is licensed under [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International](https://creativecommons.org/licenses/by-nc-sa/4.0/). Icons by [Font awesome](https://fontawesome.com/).

The platform and its functionalities are available free of charge. It has been developed in [open access](https://gitlab.com/leabrig/oura2). Its use is the subject of several scientific communications.

### Team

**Pierre-François Coen**, full professor, University of Fribourg, *pierre-francois.coen(at)unifr.ch*, *pierre-francois.coen(at)unifr.ch*.

**Lea Briguet**, assistant - developer, University of Fribourg, *lea.briguet(at)unifr.ch*