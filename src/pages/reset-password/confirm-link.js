import {useRouter} from "next/router";
import Head from "next/head";
import Navbar from "../../components/Navbar";
import styles from "../../styles/signup.module.css";
import {useState} from "react";
import FlashMessage from "../../components/FlashMessage";
import BlueButton from "../../components/BlueButton";
import {getLanguageFile} from "../../../lib/getLanguageFile";
import Translation from "../../components/Translation";
import ReturnButton from "../../components/ReturnButton";

export async function getServerSideProps({locale}) {
    const dictionary = getLanguageFile(locale);
    return {props: {dictionary: dictionary, locale:locale}}
}

export default function confirmLink ({dictionary, locale}) {
    const [errorMessage, setErrorMessage] = useState('');
    const [successMessage, setSuccessMessage] = useState('');
    const router = useRouter();
    const userId = router.query.id;
    const [isCodeValid, setIsCodeValid] = useState(false);
    const [isPasswordChanged, setIsPasswordChanged] =useState(false);
    const [isVerifyButtonClicked, setIsVerifyButtonClicked] = useState(false);

    // We need two constants because we do not want the return link in the beginning of procedure
    const [isCodeInvalid, setIsCodeInvalid] = useState(false);

    async function handleSubmit(e) {
        e.preventDefault();
        setErrorMessage('');
        setSuccessMessage('');
        setIsVerifyButtonClicked(true);
        document.getElementById("ValidationButton").disabled = true;

        const code = router.query.co;
        const timeStampNotSliced = router.query.ts;

        // To ensure that if the url is passed through a URL defense system, that the timestamp is still correct.
        let timeStamp = undefined;
        if (timeStampNotSliced) {
            const endAtIndex = timeStampNotSliced.indexOf('---');
            timeStamp = timeStampNotSliced.slice(0, endAtIndex);

            // Compare given timestamp to the limit of time passed for validation
            let limitDate = new Date();
            limitDate.setDate(limitDate.getDate() - 1);
            let validTimeStamp = !timeStamp <= limitDate;
            if (validTimeStamp) {
                const reqBody = {
                    userId: userId,
                    code: code,
                };
                try {
                    const res = await fetch ('/api/pswd-recovery/', {
                        method: 'PUT',
                        headers: {
                            'Content-Type': 'application/json',
                        },
                        body: JSON.stringify(reqBody),
                    })
                    if (res.status === 200) {
                        const data = await res.json();
                        if (data.codeValid) {

                            // Code is valid, we set the constant to true to allow setting new password
                            setIsCodeValid(true);
                            setIsCodeInvalid(false);
                            setSuccessMessage(dictionary.confirmLink_successMsg_validLink);
                        } else {

                            // Code is not valid, we set false to inform user to request a new link.
                            setIsCodeValid(false);
                            setIsCodeInvalid(true);
                            setErrorMessage(dictionary.confirmLink_errorMsg_invalidLink);
                        }
                    } else {
                        throw new Error (await res.text());
                    }
                } catch (error) {
                    console.error('An unexpected error occured:', error);
                    setErrorMessage('An unexpected error occurred: ' + error.message);
                }
            } else {
                setIsCodeValid(false);
                setIsCodeInvalid(true);
                setErrorMessage(dictionary.confirmLink_errorMsg_invalidLink)
            }
        } else {
            setIsCodeValid(false);
            setIsCodeInvalid(true);
            setErrorMessage(dictionary.confirmLink_errorMsg_invalidLink)
        }
    }

    async function handlePasswordChange(e) {
        e.preventDefault();

        if (errorMessage) setErrorMessage('');
        if (successMessage) setSuccessMessage('');

        const body = {
            id: userId,
            password: e.currentTarget.password.value,
        }

        if (body.password !== e.currentTarget.rpassword.value) {
            await setErrorMessage('');
            setErrorMessage(dictionary.confirmLink_errorMsg_pswdDontMatch);
            return;
        }

        try {
            const res = await fetch('/api/update-password', {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify(body),
            })
            if (res.status === 200) {

                // As the user updated his password, we delete the code from DB
                try {
                    const res = await fetch (`/api/pswd-recovery?id=${userId}`, {
                        method: 'DELETE',
                        headers: { 'Content-Type': 'application/json'},
                    });
                    if (res.status === 200) {
                        setSuccessMessage(dictionary.confirmLink_successMsg_pswdUpdated);
                        setIsCodeValid(false);
                        setIsPasswordChanged(true);
                    } else {
                        throw new Error (await res.text());
                    }
                } catch (error) {
                    console.error('An unexpected error occurred:', error)
                    setErrorMessage(error.message);
                }
            } else {
                throw new Error(await res.text())
            }
        } catch (error) {
            console.error('An unexpected error occurred:', error)
            setErrorMessage(error.message);
        }
    }

    return (
        <div className={styles.pageBackground}>
            <Head>
                <title>{dictionary.confirmLink_header_title}</title>
            </Head>
            <Navbar dictionary={dictionary} locale={locale}/>
            <div className={styles.largeContainer}>
                <div className={styles.smallContainer}>
                    <h2>
                        <Translation dictionary={dictionary} trKey="confirmLink_header2_reset"/>
                    </h2>
                    <p className={isVerifyButtonClicked ? `${styles.infoText}` : ''}>
                        <Translation dictionary={dictionary} trKey="confirmLink_info_clickButton"/>
                    </p>
                    <BlueButton isLink={false} type="button" id="ValidationButton" label={dictionary.confirmLink_button_verifyLink} title={dictionary.confirmLink_button_verifyLink} onClick={e => handleSubmit(e)}/>
                    {isCodeValid &&
                        <div>
                            <p>
                                <Translation dictionary={dictionary} trKey="confirmLink_info_linkConfirmed"/>
                            </p>
                            <form onSubmit={e => handlePasswordChange(e)}>
                                <label className={styles.formLabel} htmlFor="password">
                                    <Translation dictionary={dictionary} trKey="confirmLink_form_pswd"/>
                                </label>
                                <input type="password" id="password" title={dictionary.confirmLink_input_passwordTitle} pattern=".{8,}" name="user_password" className={styles.inputField}
                                       required/>

                                <label htmlFor="rpassword" className={styles.secondFormLabel} title={dictionary.confirmLink_input_passwordTitle}>
                                    <Translation dictionary={dictionary} trKey="confirmLink_form_repeatPswd"/>
                                </label>
                                <input type="password" id="rpassword" name="rpassword" className={styles.inputField}
                                       required/>
                                <div className={styles.confirmButtonContainer}>
                                    <BlueButton type="submit" title={dictionary.confirmLink_button_modifyPswd}
                                                label={dictionary.confirmLink_button_modifyPswd} isLink={false}/>
                                </div>
                            </form>
                        </div>}
                    {isCodeInvalid &&
                        <div>
                            <p>
                                <Translation dictionary={dictionary} trKey="confirmLink_info_linkInvalid"/>
                            </p>
                            <ReturnButton dictionary={dictionary} title={dictionary.confirmLink_button_returnTitle} href={`/reset-password/`}/>
                        </div>
                    }
                    {isPasswordChanged &&
                        <div>
                            <p>
                                <Translation dictionary={dictionary} trKey="confirmLink_info_pswdResetSuccess"/>
                            </p>
                            <BlueButton isLink={true} title={dictionary.confirmLink_button_connectTitle} label={dictionary.confirmLink_button_connectLabel} href={`/login/`}/>
                        </div>
                    }

                </div>
            </div>
            {errorMessage && <FlashMessage type='error' msg={errorMessage}/>}
            {successMessage && <FlashMessage type='success' msg={successMessage}/>}
        </div>
    )
}