import styles from '../../styles/signup.module.css';
import Head from "next/head";
import Navbar from "../../components/Navbar";
import {useState} from "react";
import FlashMessage from "../../components/FlashMessage";
import BlueButton from "../../components/BlueButton";
import {getLanguageFile} from "../../../lib/getLanguageFile";
import Translation from "../../components/Translation";

export async function getServerSideProps({locale}) {
    const dictionary = getLanguageFile(locale);
    return {props: {dictionary: dictionary, locale:locale}}
}

export default function resetPassword({dictionary, locale}){
    const [errorMsg, setErrorMsg] = useState('');
    const [infoMsg, setInfoMsg] = useState('');
    const [successMsg, setSuccessMsg] = useState('');
    const [emailSent, setEmailSent] = useState(false);

    async function handleSubmit(e) {
        setErrorMsg('');
        setSuccessMsg('');
        setInfoMsg(dictionary.resetPassword_info_requestSent);
        e.preventDefault();

        // Slice the part before the at sign to greet the user in the email.
        const email = e.currentTarget.email.value;
        const indexAt = email.indexOf('@');
        let userName = 'Nobody';
        if (indexAt !== -1) {
            userName = email.slice(0, indexAt);
        }

        const generateBody = {
            email: email,
        }

        // First create the recovery code and return it as link
        try {
            const res = await fetch('/api/pswd-recovery', {
                method: 'POST',
                headers: {'Content-Type': 'application/json'},
                body: JSON.stringify(generateBody),
            })
            if (res.status === 200){
                const data = await res.json();

                // Create the confirmation link with userid, timestamp and code
                const confirmLink = `${window.location.origin}/${locale}/reset-password/confirm-link?co=${data.code}&id=${data.userId}&ts=${data.timeStamp}---`;
                const body = {
                    sendTo: email,
                    subject: dictionary.resetPassword_mail_subject,
                    text: dictionary.resetPassword_mail_text.replace(`{userName}`, userName).replace(`{confirmLink}`, confirmLink),
                }

                try {
                    const res = await fetch('/api/mail', {
                        method: 'POST',
                        headers: {'Content-Type': 'application/json'},
                        body: JSON.stringify(body),
                    })
                    if (res.status === 200) {
                        setEmailSent(true);
                        setInfoMsg('');
                        setSuccessMsg(dictionary.resetPassword_successMsg_mailSent);
                    } else {
                        throw new Error(await res.text());
                    }
                } catch (error) {
                    console.error('An unexpected error occurred:', error);
                    setErrorMsg(error.message);
                }
            } else if (res.status === 204) {
                // User was not found, but we do not want to give that away
                setEmailSent(true);
                setSuccessMsg(dictionary.resetPassword_successMsg_mailSent);
            } else {
                throw new Error(await res.text());
            }
        } catch (error) {
            console.error('An unexpected error occurred:', error);
            setErrorMsg(error.message);
        }

    }

    return (
        <div className={styles.pageBackground}>
            <Head>
                <title>{dictionary.resetPassword_header_title}</title>
            </Head>
            <Navbar dictionary={dictionary} locale={locale}/>
            <div className={styles.largeContainer}>
                <div className={styles.smallContainer}>
                    <h2>
                        <Translation dictionary={dictionary} trKey="resetPassword_header2_title"/>
                    </h2>
                    <p>
                        <Translation dictionary={dictionary} trKey="resetPassword_info_insertEmail"/>
                    </p>
                    <form onSubmit={e => handleSubmit(e)}>
                        <label>
                            <span className={styles.formLabel}>
                                <Translation dictionary={dictionary} trKey="resetPassword_input_label"/>
                            </span>
                            <input className={styles.inputField} type="email" name="email" required />
                        </label>
                        <div className={styles.confirmButtonContainer}>
                            <BlueButton isLink={false} title={dictionary.resetPassword_submit_title} label={dictionary.resetPassword_submit_label} type="submit"/>
                        </div>
                    </form>
                    {emailSent &&
                        <p>
                            <Translation dictionary={dictionary} trKey="resetPassword_info_mailSent"/>
                        </p>
                    }
                </div >
            </div>
            {infoMsg && <FlashMessage type="info" msg={infoMsg}/>}
            {successMsg && <FlashMessage type="success" msg={successMsg}/>}
            {errorMsg && <FlashMessage type="error" msg={errorMsg}/>}
        </div>
    )
}