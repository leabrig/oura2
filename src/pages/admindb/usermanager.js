import Layout from "../../components/Layout";
import styles from "../../styles/admindb.module.css";
import Head from "next/head";
import DBNav from "../../components/DBNav";
import {useAdmin} from "../../../lib/hooks";
import Loading from "../../components/loading";
import {useRouter} from "next/router";
import {useState} from "react";
import prisma from "../../../lib/prisma";
import FlashMessage from "../../components/FlashMessage";
import {getLanguageFile} from "../../../lib/getLanguageFile";
import Translation from "../../components/Translation";

export async function getServerSideProps({locale}) {
    const userCount = await prisma.user.count();
    const surveyCount = await prisma.survey.count();
    const answerCount = await prisma.formAnswer.count();
    const users = await prisma.user.findMany({
        orderBy: [
            {
                email: 'asc',
            },
        ]
    });

    const serializedUsers = users.map(user => {
        return {
            ...user,
            createdAt: user.createdAt.toISOString(),
            updatedAt: user.updatedAt.toISOString(),
            recoveryTimeStamp: null ? user.recoveryTimeStamp.toISOString() : '',
        }
    })

    const dictionary = getLanguageFile(locale);

    return {props: {dictionary: dictionary, users: serializedUsers, userCount: userCount, surveyCount: surveyCount, answerCount: answerCount, locale:locale}}
}

export default function UserManager({dictionary, users, userCount, surveyCount, answerCount, locale}) {
    const {user, isLoading, isAdmin} = useAdmin({redirectTo: '/login'});
    const [errorMessage, setErrorMessage] = useState("");
    const router = useRouter();

    //If user not returned yet from useUser -> Loading page, else error page!
    if (isLoading) {
        return (<Loading locale={locale} dictionary={dictionary}/>);
    } else if (!isLoading && !user) {
        return (<Loading dictionary={dictionary} locale={locale}/>);
    } else if (!isAdmin) {
        return (<Loading dictionary={dictionary} locale={locale}/>);
    }

    async function handleAdminChange(userId, isAdmin) {
        const confirmText = isAdmin ? dictionary.usermanager_confirm_takeAdmin : dictionary.usermanager_confirm_giveAdmin;
        const confirmChange = confirm(confirmText);
        if (!confirmChange) return;
        setErrorMessage('');

        const body = {
            id: userId,
            admin: isAdmin,
        }

        try {
            const res = await fetch('/api/useradmin', {
                method: 'PUT',
                headers: {'Content-Type': 'application/json'},
                body: JSON.stringify(body),
            })
            if (res.status === 200) {
                await router.push("/admindb/usermanager");
            } else {
                throw new Error(await res.text());
            }
        } catch (error) {
            console.error("An unexpected error occured.", error);
            setErrorMessage(error.message);
        }
    }

    return (
        <Layout dictionary={dictionary} locale={locale}>
            <div className={styles.dbContainer}>
                <Head>
                    <title>
                        {dictionary.usermanager_header_title}
                    </title>
                </Head>
                <DBNav dictionary={dictionary}/>
                <div>
                    <div className={styles.userStatsContainer}>
                        <div className={styles.userStatsTitle}>
                            <Translation dictionary={dictionary} trKey="usermanager_statistiques_title"/>
                        </div>
                        <div>
                            <Translation dictionary={dictionary} trKey="usermanager_statistiques_users" params={{userCount: userCount}}/>
                        </div>
                        <div>
                            <Translation dictionary={dictionary} trKey="usermanager_statistiques_surveys" params={{surveyCount:surveyCount}}/>
                        </div>
                        <div>
                            <Translation dictionary={dictionary} trKey="usermanager_statistiques_answers" params={{answerCount: answerCount}}/>
                        </div>
                    </div>
                </div>
                    <div className={styles.tableContainer}>
                        <table className={styles.tableFormat}>
                            <thead>
                            <tr className={styles.tableHead}>
                                <th className={styles.thFormatFirst}>
                                    <Translation dictionary={dictionary} trKey="usermanager_header_title" />
                                </th>
                                <th className={styles.lastThFormat}>
                                    <Translation dictionary={dictionary} trKey="usermanager_tableHeader_isAdmin"/>
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            {users && users.map(usr => {
                                const isChecked = usr.admin;
                                if (usr.id === user.id) {
                                    return (
                                            <tr key={usr.id} className={styles.ownAccountElementsTableHolder}>
                                                <td className={styles.firstElementInTable}>
                                                    {usr.email}
                                                </td>
                                                <td className={styles.lastElementInTable}>
                                                    <div className={[styles.checkboxWrapper, styles.notAllowed].join(" ")}>
                                                        <input type="checkbox" className={[styles.slideCheckbox, styles.disabled].join(" ")}
                                                               defaultChecked={isChecked} disabled title={dictionary.usermanager_title_ownAccess} alt={dictionary.usermanager_title_ownAccess}/>
                                                    </div>
                                                </td>
                                            </tr>
                                        )
                                } else {
                                    return (
                                        <tr key={usr.id} className={styles.elementsTableHolder}>
                                            <td className={styles.firstElementInTable}>
                                                {usr.email}
                                            </td>
                                            <td className={styles.lastElementInTable}>
                                                <div className={styles.checkboxWrapper}>
                                                    <input type="checkbox" className={styles.slideCheckbox}
                                                           checked={isChecked} onChange={() => handleAdminChange(usr.id, isChecked)}/>
                                                </div>
                                            </td>
                                        </tr>
                                    )
                                }

                            })}
                            </tbody>
                        </table>
                </div>
            </div>
            {errorMessage && (<FlashMessage type="error" msg={errorMessage}/>)}
        </Layout>
    )
}