import {useAdmin} from "../../../../lib/hooks";
import Layout from "../../../components/Layout";
import Head from "next/head";
import prisma from "../../../../lib/prisma";
import styles from "../../../styles/admindb.module.css";
import DBNav from "../../../components/DBNav";
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faPenToSquare, faPlus} from '@fortawesome/free-solid-svg-icons';
import Loading from "../../../components/loading";
import BlueButton from "../../../components/BlueButton";
import {getLanguageFile} from "../../../../lib/getLanguageFile";
import Translation from "../../../components/Translation";
import {getLanguageLabel} from "../../../../lib/getLanguageLabel";

export async function getServerSideProps({locale}) {
    const dimensions = await prisma.dimension.findMany({
        include: {
            domain: true,
        }
    });
    const dictionary = getLanguageFile(locale);
    return {
        props: {dimensions: dimensions, dictionary: dictionary, locale:locale},
    };
}

export default function Index({dimensions, dictionary, locale}) {
    const {user, isLoading, isAdmin} = useAdmin({redirectTo: '/login'});
    const langLabel = getLanguageLabel(locale);

    //If user not returned yet from useUser -> Loading page, else error page!
    if (isLoading) {
        return (<Loading locale={locale} dictionary={dictionary}/>);
    } else if (!isLoading && !user) {
        return (<Loading locale={locale} dictionary={dictionary}/>);
    } else if (!isAdmin) {
        return (<Loading locale={locale} dictionary={dictionary}/>);
    }

    return (
        <Layout locale={locale} dictionary={dictionary}>
            <div className={styles.dbContainer}>
                <Head>
                    <title>{dictionary.admindbDimensions_header_title}</title>
                </Head>
                <DBNav dictionary={dictionary}/>
                <div className={styles.linkButtonContainer}>
                    <BlueButton isLink={true} title={dictionary.admindbDimensions_buttonNew_title} label={dictionary.admindbDimensions_buttonNew_label} href="/admindb/dimensions/new">
                        <FontAwesomeIcon icon={faPlus} className={styles.newButtonIcon}/>
                    </BlueButton>
                </div>
                <table className={styles.tableFormat}>
                    <thead>
                    <tr className={styles.tableHead}>
                        <th className={styles.thFormatFirst}>
                            <Translation dictionary={dictionary} trKey="admindbDimensions_tableHeader_dimension"/>
                        </th>
                        <th className={styles.thFormat}>
                            <Translation dictionary={dictionary} trKey="admindbDimensions_tableHeader_domain"/>
                        </th>
                        <th className={styles.lastThFormat}>
                            <Translation dictionary={dictionary} trKey="admindbDimensions_tableHeader_edit"/>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    {dimensions.map(dimension => {
                        return <tr key={dimension.id} className={styles.elementsTableHolder}>
                            <td className={styles.firstElementInTable}>{dimension[langLabel]}</td>
                            <td className={styles.elementInTable}>{dimension.domain[langLabel]}</td>
                            <td className={styles.lastElementInTable}>
                                <div className={styles.confirmButtonContainer}>
                                    <BlueButton isLink={true} href={`/admindb/dimensions/edit/${dimension.id}`} title={dictionary.admindbDimensions_buttonEdit_title}>
                                        <FontAwesomeIcon icon={faPenToSquare}/>
                                    </BlueButton>
                                </div>
                            </td>
                        </tr>
                    })}
                    </tbody>
                </table>
            </div>
        </Layout>
    )

}