import {useAdmin} from "../../../../lib/hooks";
import Layout from "../../../components/Layout";
import Head from "next/head";
import styles from "../../../styles/admindb.module.css";
import {useState} from "react";
import {useRouter} from "next/router";
import prisma from "../../../../lib/prisma";
import FlashMessage from "../../../components/FlashMessage";
import ReturnButton from "../../../components/ReturnButton";
import Loading from "../../../components/loading";
import BlueButton from "../../../components/BlueButton";
import {getLanguageFile} from "../../../../lib/getLanguageFile";
import Translation from "../../../components/Translation";
import {getLanguageLabel} from "../../../../lib/getLanguageLabel";

export async function getServerSideProps({locale}) {
    const domains = await prisma.domain.findMany();
    const dictionary = getLanguageFile(locale);
    return {
        props: {domains: domains, dictionary:dictionary, locale:locale},
    };
}

export default function Dimension({domains, dictionary, locale}) {
    const {user, isLoading, isAdmin} = useAdmin({redirectTo: '/login'});
    const [errorMsg, setErrorMsg] = useState('');
    const router = useRouter();
    const langLabel = getLanguageLabel(locale);

    //If user not returned yet from useUser -> Loading page, else error page!
    if(isLoading){
        return (<Loading locale={locale} dictionary={dictionary}/>);
    } else if (!isLoading && !user) {
        return (<Loading locale={locale} dictionary={dictionary}/>);
    } else if(!isAdmin) {
        return (<Loading locale={locale} dictionary={dictionary}/>);
    }


    async function handleSubmit(e) {
        e.preventDefault();

        if (errorMsg) setErrorMsg('');

        const body = {
            label: e.currentTarget.label.value,
            labelFr: e.currentTarget.labelFr.value,
            labelDe: e.currentTarget.labelDe.value,
            labelAr: e.currentTarget.labelAr.value,
            domainId: e.currentTarget.domainId.value,
        }

        try {
            const res = await fetch('/api/dimension', {
                method: 'POST',
                headers: {'Content-Type': 'application/json'},
                body: JSON.stringify(body),
            })
            if (res.status === 200) {
                await router.push("/admindb/dimensions");
            } else {
                throw new Error(await res.text());
            }
        } catch (error) {
            console.error('An unexpected error occurred:', error);
            setErrorMsg(error.message);
        }
    }

    return (
        <Layout dictionary={dictionary} locale={locale}>
            <div className={styles.dbContainer}>
                <Head>
                    <title>{dictionary.admindbDimensionsNew_header_title}</title>
                </Head>
                <div>
                    <h2>
                        <Translation dictionary={dictionary} trKey="admindbDimensionsNew_header2_title"/>
                    </h2>
                </div>
                <form onSubmit={handleSubmit}>
                    <label className={styles.submit}>
                        <span className={styles.label}>
                            <Translation dictionary={dictionary} trKey="admindbDimensionsNew_label_name"/>
                            -EN
                        </span>
                        <input type="text" name="label" required/>
                    </label>
                    <label className={styles.submit}>
                        <span className={styles.label}>
                            <Translation dictionary={dictionary} trKey="admindbDimensionsNew_label_name"/>
                            -FR
                        </span>
                        <input type="text" name="labelFr" required/>
                    </label>
                    <label className={styles.submit}>
                        <span className={styles.label}>
                            <Translation dictionary={dictionary} trKey="admindbDimensionsNew_label_name"/>
                            -DE
                        </span>
                        <input type="text" name="labelDe" required/>
                    </label>
                    <label className={styles.submit}>
                        <span className={styles.label}>
                            <Translation dictionary={dictionary} trKey="admindbDimensionsNew_label_name"/>
                            -AR
                        </span>
                        <input type="text" name="labelAr" required/>
                    </label>
                    <label className={styles.submit}>
                        <span className={styles.label}>
                            <Translation dictionary={dictionary} trKey="admindbDimensionsNew_label_domain"/>
                        </span>
                        <select name="domainId" required>
                            {domains.map(domain => {
                                return <option key={domain.id} value={domain.id}>{domain[langLabel]}</option>
                            })}
                        </select>
                    </label>
                    <div className={styles.bottomButtonsContainer}>
                        <ReturnButton dictionary={dictionary} title={dictionary.admindbDimensionsNew_return_title}
                                      href={`/admindb/dimensions`}/>
                        <div className={styles.confirmButtonContainer}>
                            <BlueButton type="submit" title={dictionary.admindbDimensionsNew_buttonCreate_title}
                                        label={dictionary.admindbDimensionsNew_buttonCreate_label} isLink={false}/>
                        </div>
                    </div>
                </form>
                {errorMsg && <FlashMessage msg={errorMsg} type='error'/>}
            </div>
        </Layout>
    )
}