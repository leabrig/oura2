import {useAdmin} from "../../../../../lib/hooks";
import Layout from "../../../../components/Layout";
import Head from "next/head";
import styles from "../../../../styles/admindb.module.css";
import {useRouter} from "next/router";
import {useState} from "react";
import Loading from "../../../../components/loading";
import prisma from "../../../../../lib/prisma";
import FlashMessage from "../../../../components/FlashMessage";
import ReturnButton from "../../../../components/ReturnButton";
import BlueButton from "../../../../components/BlueButton";
import DeleteButton from "../../../../components/DeleteButton";
import {getLanguageFile} from "../../../../../lib/getLanguageFile";
import Translation from "../../../../components/Translation";
import {getLanguageLabel} from "../../../../../lib/getLanguageLabel";

export async function getServerSideProps(context) {
    const dimension = await prisma.dimension.findUnique({
        where: {
            id: context.query.id,
        },
    })
    const locale = context.locale;
    const dictionary = getLanguageFile(locale);

    const domains = await prisma.domain.findMany();
    return { props: {dimension: dimension, domains: domains, dictionary: dictionary, locale: locale }};
}

export default function EditDimension({dimension, domains, dictionary, locale}) {
    const { user, isLoading, isAdmin} = useAdmin({redirectTo: '/login'});
    const [errorMsg, setErrorMsg] = useState('');
    const router = useRouter();
    const langLabel = getLanguageLabel(locale);

    //If user not returned yet from useUser -> Loading page, else error page!
    if(isLoading){
        return (<Loading locale={locale} dictionary={dictionary}/>);
    } else if (!isLoading && !user) {
        return (<Loading locale={locale} dictionary={dictionary}/>);
    } else if (!isAdmin) {
        return (<Loading locale={locale} dictionary={dictionary}/>);
    }

    async function handleSubmit(e) {
        e.preventDefault();

        if (errorMsg) setErrorMsg('');

        const body = {
            id: dimension.id,
            label: e.currentTarget.label.value,
            labelFr: e.currentTarget.labelFr.value,
            labelDe: e.currentTarget.labelDe.value,
            labelAr: e.currentTarget.labelAr.value,
            domainId: e.currentTarget.domainId.value,
        }

        try {
            const res = await fetch('/api/dimension', {
                method: 'PUT',
                headers: {'Content-Type': 'application/json'},
                body: JSON.stringify(body),
            })
            if (res.status === 200) {
                await router.push("/admindb/dimensions");
            } else {
                throw new Error(await res.text());
            }
        } catch (error) {
            console.error('An unexpected error occurred:', error);
            setErrorMsg(error.message);
        }
    }

    async function handleDelete(e) {
        e.preventDefault();

        const confirmDelete = confirm(dictionary.admindbDimensionsEdit_confirm_delete);
        if (!confirmDelete) return;

        if (errorMsg) setErrorMsg('');

        try {
            const res = await fetch(`/api/dimension?id=${dimension.id}`, {
                method: 'DELETE',
                headers: {'Content-Type': 'application/json'},
            })
            if (res.status === 200) {
                await router.push("/admindb/dimensions");
            } else {
                throw new Error(await res.text());
            }
        } catch (error) {
            console.error('An unexpected error occurred:', error);
            setErrorMsg(error.message);
        }
    }

    return (
        <Layout dictionary={dictionary} locale={locale}>
            <div>
                <Head>
                    <title>
                        {dictionary.admindbDimensionsEdit_header_title}
                    </title>
                </Head>
                <div>
                    <h2>
                        <Translation dictionary={dictionary} trKey="admindbDimensionsEdit_header1_title"/>
                        <span className={styles.titleLabel}>&nbsp;-&nbsp;{dimension[langLabel]}</span></h2>
                </div>
                <form onSubmit={e => handleSubmit(e)}>
                    <label className={styles.submit}>
                        <span className={styles.label}>
                            <Translation dictionary={dictionary} trKey="admindbDimensionsEdit_label_name"/>
                            -EN
                        </span>
                        <input type="text" name="label" defaultValue={dimension.label} required/>
                    </label>
                    <label className={styles.submit}>
                        <span className={styles.label}>
                            <Translation dictionary={dictionary} trKey="admindbDimensionsEdit_label_name"/>
                            -FR
                        </span>
                        <input type="text" name="labelFr" defaultValue={dimension.labelFr} required/>
                    </label>
                    <label className={styles.submit}>
                        <span className={styles.label}>
                            <Translation dictionary={dictionary} trKey="admindbDimensionsEdit_label_name"/>
                            -DE
                        </span>
                        <input type="text" name="labelDe" defaultValue={dimension.labelDe} required/>
                    </label>
                    <label className={styles.submit}>
                        <span className={styles.label}>
                            <Translation dictionary={dictionary} trKey="admindbDimensionsEdit_label_name"/>
                            -AR
                        </span>
                        <input type="text" name="labelAr" defaultValue={dimension.labelAr} required/>
                    </label>
                    <label className={styles.submit}>
                        <span className={styles.label}>
                            <Translation dictionary={dictionary} trKey="admindbDimensionsEdit_label_domain"/>
                        </span>
                        <select name="domainId" required defaultValue={dimension.domainId}>
                            {domains.map(domain => {
                                return <option key={domain.id} value={domain.id}>{domain[langLabel]}</option>
                            })}
                        </select>
                    </label>
                    <div className={styles.bottomButtonsContainer}>
                        <ReturnButton dictionary={dictionary} title={dictionary.admindbDimensionsEdit_return_title} href={`/admindb/dimensions`}/>
                        <div className={styles.delEditContainer}>
                            <div className={styles.deleteButtonContainer}>
                                <DeleteButton onClick={e => handleDelete(e)} title={dictionary.admindbDimensionsEdit_delete_title} label={dictionary.admindbDimensionsEdit_delete_label}/>
                            </div>
                            <div className={styles.confirmButtonContainer}>
                                <BlueButton isLink={false} type="submit" title={dictionary.admindbDimensionsEdit_edit_title} label={dictionary.admindbDimensionsEdit_edit_label}/>
                            </div>
                        </div>
                    </div>
                </form>
                {errorMsg && <FlashMessage msg={errorMsg} type='error'/>}
            </div>
        </Layout>
    )
}