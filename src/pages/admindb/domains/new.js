import {useAdmin} from "../../../../lib/hooks";
import Layout from "../../../components/Layout";
import Head from "next/head";
import styles from "../../../styles/admindb.module.css";
import {useState} from "react";
import {useRouter} from "next/router";
import FlashMessage from "../../../components/FlashMessage";
import ReturnButton from "../../../components/ReturnButton";
import Loading from "../../../components/loading";
import BlueButton from "../../../components/BlueButton";
import {getLanguageFile} from "../../../../lib/getLanguageFile";
import Translation from "../../../components/Translation";
import {getLanguageLabel} from "../../../../lib/getLanguageLabel";

export async function getServerSideProps({locale}) {
    const dictionary = getLanguageFile(locale);
    return {props: {dictionary: dictionary, locale:locale}}
}

export default function Dimension({dictionary, locale}) {
    const {user, isLoading, isAdmin} = useAdmin({redirectTo: '/login'});
    const [errorMsg, setErrorMsg] = useState('');
    const router = useRouter();

    //If user not returned yet from useUser -> Loading page, else error page!
    if(isLoading){
        return (<Loading locale={locale} dictionary={dictionary}/>);
    } else if (!isLoading && !user) {
        return (<Loading locale={locale} dictionary={dictionary}/>);
    } else if(!isAdmin) {
        return (<Loading locale={locale} dictionary={dictionary}/>);
    }

    async function handleSubmit(e) {
        e.preventDefault();

        if (errorMsg) setErrorMsg('');

        const body = {
            label: e.currentTarget.label.value,
            labelFr: e.currentTarget.labelFr.value,
            labelDe: e.currentTarget.labelDe.value,
            labelAr: e.currentTarget.labelAr.value,

        }

        try {
            const res = await fetch('/api/domain', {
                method: 'POST',
                headers: {'Content-Type': 'application/json'},
                body: JSON.stringify(body),
            })
            if (res.status === 200) {
                await router.push("/admindb/domains");
            } else {
                throw new Error(await res.text());
            }
        } catch (error) {
            console.error('An unexpected error occurred:', error);
            setErrorMsg(error.message);
        }
    }

    return (
        <Layout locale={locale} dictionary={dictionary}>
            <div className={styles.dbContainer}>
                <Head>
                    <title>{dictionary.admindbDomainsNew_header_title}</title>
                </Head>
                <div>
                    <h2>
                        <Translation dictionary={dictionary} trKey="admindbDomainsNew_header2_title"/>
                    </h2>
                </div>
                <form onSubmit={handleSubmit}>
                    <label className={styles.submit}>
                        <span className={styles.label}>
                            <Translation dictionary={dictionary} trKey="admindbDomainsNew_label_name"/>
                            -EN
                        </span>
                        <input type="text" name="label" required/>
                    </label>
                    <label className={styles.submit}>
                        <span className={styles.label}>
                            <Translation dictionary={dictionary} trKey="admindbDomainsNew_label_name"/>
                            -FR
                        </span>
                        <input type="text" name="labelFr" required/>
                    </label>
                    <label className={styles.submit}>
                        <span className={styles.label}>
                            <Translation dictionary={dictionary} trKey="admindbDomainsNew_label_name"/>
                            -DE
                        </span>
                        <input type="text" name="labelDe" required/>
                    </label>
                    <label className={styles.submit}>
                        <span className={styles.label}>
                            <Translation dictionary={dictionary} trKey="admindbDomainsNew_label_name"/>
                            -AR
                        </span>
                        <input type="text" name="labelAr" required/>
                    </label>
                    <div className={styles.bottomButtonsContainer}>
                        <ReturnButton dictionary={dictionary} title={dictionary.admindbDomainsNew_return_title}
                                      href={`/admindb/domains`}/>
                        <div className={styles.confirmButtonContainer}>
                            <BlueButton isLink={false} title={dictionary.admindbDomainsNew_buttonCreate_title}
                                        label={dictionary.admindbDomainsNew_buttonCreate_label} type="submit"/>
                        </div>
                    </div>
                </form>
                {errorMsg && <FlashMessage msg={errorMsg} type='error'/>}
            </div>
        </Layout>
    )
}