import {useAdmin} from "../../../../lib/hooks";
import Layout from "../../../components/Layout";
import Head from "next/head";
import prisma from "../../../../lib/prisma";
import styles from "../../../styles/admindb.module.css";
import DBNav from "../../../components/DBNav";
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faPenToSquare, faPlus} from '@fortawesome/free-solid-svg-icons';
import Loading from "../../../components/loading";
import BlueButton from "../../../components/BlueButton";
import {getLanguageFile} from "../../../../lib/getLanguageFile";
import Translation from "../../../components/Translation";
import {getLanguageLabel} from "../../../../lib/getLanguageLabel";

export async function getServerSideProps({locale}) {
    const domains = await prisma.domain.findMany();
    const dictionary = getLanguageFile(locale);
    return {
        props: {domains: domains, dictionary: dictionary, locale:locale},
    };
}

export default function Index({domains, dictionary, locale}) {
    const {user, isLoading, isAdmin} = useAdmin({redirectTo: '/login'});
    const langLabel = getLanguageLabel(locale);

    //If user not returned yet from useUser -> Loading page, else error page!
    if (isLoading) {
        return (<Loading locale={locale} dictionary={dictionary}/>);
    } else if (!isLoading && !user) {
        return (<Loading locale={locale} dictionary={dictionary}/>);
    } else if (!isAdmin) {
        return (<Loading locale={locale} dictionary={dictionary}/>);
    }

    return (
        <Layout dictionary={dictionary} locale={locale} >
            <div className={styles.dbContainer}>
                <Head>
                    <title>{dictionary.admindbDomains_header_title}</title>
                </Head>
                <DBNav dictionary={dictionary}/>
                <div className={styles.linkButtonContainer}>
                    <BlueButton isLink={true} title={dictionary.admindbDomains_buttonNew_title} href="/admindb/domains/new" label={dictionary.admindbDomains_buttonNew_label}>
                        <FontAwesomeIcon icon={faPlus} className={styles.newButtonIcon}/>
                    </BlueButton>
                </div>
                <table className={styles.tableFormat}>
                    <thead>
                    <tr className={styles.tableHead}>
                        <th className={styles.thFormatFirst}>
                            <Translation dictionary={dictionary} trKey="admindbDomains_tableHeader_domain"/>
                        </th>
                        <th className={styles.lastThFormat}>
                            <Translation dictionary={dictionary} trKey="admindbDomains_tableHeader_edit"/>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    {domains.map(domain => {
                         return <tr key={domain.id} className={styles.elementsTableHolder}>
                            <td className={styles.firstElementInTable}>{domain[langLabel]}</td>
                            <td className={styles.lastElementInTable}>
                                <div className={styles.confirmButtonContainer}>
                                    <BlueButton isLink={true}
                                                href={`/admindb/domains/edit/${domain.id}`} title={dictionary.admindbDomains_buttonEdit_title}>
                                        <FontAwesomeIcon icon={faPenToSquare}/>
                                    </BlueButton>
                                </div>
                            </td>
                        </tr>
                    })}
                    </tbody>
                </table>
            </div>
        </Layout>
    )

}