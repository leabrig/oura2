import Layout from "../../components/Layout";
import styles from "../../styles/admindb.module.css";
import Head from "next/head";
import DBNav from "../../components/DBNav";
import {useAdmin} from "../../../lib/hooks";
import Loading from "../../components/loading";
import {getLanguageFile} from "../../../lib/getLanguageFile";
import Translation from "../../components/Translation";

export async function getServerSideProps({locale}) {
    const dictionary = getLanguageFile(locale);
    return {props: {dictionary: dictionary, locale:locale}}
}

export default function AdminDB({dictionary, locale}) {
    const {user, isLoading, isAdmin} = useAdmin({redirectTo: '/login'});

    // If user not returned yet from useUser -> Loading page, else error page!
    if(isLoading){
        return (<Loading dictionary={dictionary} locale={locale}/>);
    } else if (!isLoading && !user) {
        return (<Loading dictionary={dictionary} locale={locale}/>);
    } else if(!isAdmin) {
        return (<Loading dictionary={dictionary} locale={locale}/>);
    }

    return (
        <Layout dictionary={dictionary} locale={locale}>
            <div className={styles.dbContainer}>
                <Head>
                    <title>
                        OURA² - Admin
                    </title>
                </Head>
                <DBNav dictionary={dictionary}/>
                <h3>
                    <Translation dictionary={dictionary} trKey="admindb_header3_title"/>
                </h3>
                <p>
                    <Translation dictionary={dictionary} trKey="admindb_paragraph_description"/>
                </p>
                <p>
                    <Translation dictionary={dictionary} trKey="admindb_paragraph_warning"/>
                </p>
                <p>
                    <Translation dictionary={dictionary} trKey="admindb_paragraph_next"/>
                </p>
            </div>
        </Layout>
    )
}
