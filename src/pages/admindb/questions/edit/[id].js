import {useAdmin} from "../../../../../lib/hooks";
import Layout from "../../../../components/Layout";
import Head from "next/head";
import styles from "../../../../styles/admindb.module.css";
import {useRouter} from "next/router";
import {useState} from "react";
import Loading from "../../../../components/loading";
import prisma from "../../../../../lib/prisma";
import FlashMessage from "../../../../components/FlashMessage";
import ReturnButton from "../../../../components/ReturnButton";
import BlueButton from "../../../../components/BlueButton";
import DeleteButton from "../../../../components/DeleteButton";
import {getLanguageFile} from "../../../../../lib/getLanguageFile";
import Translation from "../../../../components/Translation";
import {getLanguageLabel} from "../../../../../lib/getLanguageLabel";

export async function getServerSideProps(context) {
    const question = await prisma.question.findUnique({
        where: {
            id: context.query.id,
        },
    })
    const locale = context.locale;
    const dictionary = getLanguageFile(locale)

    const dimensions = await prisma.dimension.findMany();
    return { props: {question: question, dimensions: dimensions, dictionary: dictionary, locale:locale} };
}

export default function EditQuestion({question, dimensions, dictionary, locale}) {
    const {user, isLoading, isAdmin} = useAdmin({redirectTo: '/login'});
    const [errorMsg, setErrorMsg] = useState('');
    const router = useRouter();
    const langLabel = getLanguageLabel(locale);

    //If user not returned yet from useUser -> Loading page, else error page!
    if(isLoading){
        return (<Loading locale={locale} dictionary={dictionary}/>);
    } else if (!isLoading && !user) {
        return (<Loading locale={locale} dictionary={dictionary}/>);
    } else if(!isAdmin) {
        return (<Loading locale={locale} dictionary={dictionary}/>);
    }

    async function handleSubmit(e) {
        e.preventDefault();

        if (errorMsg) setErrorMsg('');

        let posneg = true;
        if (e.currentTarget.posneg.value === "neg") {
            posneg = false;
        }

        const body = {
            id: question.id,
            label: e.currentTarget.label.value,
            labelFr: e.currentTarget.labelFr.value,
            labelDe: e.currentTarget.labelDe.value,
            labelAr: e.currentTarget.labelAr.value,
            dimensionId: e.currentTarget.dimensionId.value,
            posneg: posneg,
        }

        try {
            const res = await fetch('/api/question', {
                method: 'PUT',
                headers: {'Content-Type': 'application/json'},
                body: JSON.stringify(body),
            })
            if (res.status === 200) {
                await router.push("/admindb/questions");
            } else {
                throw new Error(await res.text());
            }
        } catch (error) {
            console.error('An unexpected error occurred:', error);
            setErrorMsg(error.message);
        }
    }

    async function handleDelete(e) {
        e.preventDefault();

        const confirmDelete = confirm(dictionary.admindbQuestionsEdit_confirm_delete);
        if (!confirmDelete) return;

        if (errorMsg) setErrorMsg('');

        try {
            const res = await fetch(`/api/question?id=${question.id}`, {
                method: 'DELETE',
                headers: {'Content-Type': 'application/json'},
            })
            if (res.status === 200) {
                await router.push("/admindb/questions");
            } else {
                throw new Error(await res.text());
            }
        } catch (error) {
            console.error('An unexpected error occurred:', error);
            setErrorMsg(error.message);
        }
    }

    return (
        <Layout dictionary={dictionary} locale={locale}>
            <div>
                <Head>
                <title>{dictionary.admindbQuestionsEdit_header_title}</title>
                </Head>
                <div>
                    <h2>
                        <Translation dictionary={dictionary} trKey="admindbQuestionsEdit_header1_title"/>
                        <span className={styles.titleLabel}>&nbsp;-&nbsp;{question[langLabel]}</span></h2>
                </div>
                <form onSubmit={handleSubmit}>
                    <label className={styles.submit}>
                        <span className={styles.label}>
                            <Translation dictionary={dictionary} trKey="admindbQuestionsEdit_label_name"/>
                            -EN
                        </span>
                        <input type="text" name="label" defaultValue={question.label} required></input>
                    </label>
                    <label className={styles.submit}>
                        <span className={styles.label}>
                            <Translation dictionary={dictionary} trKey="admindbQuestionsEdit_label_name"/>
                            -FR
                        </span>
                        <input type="text" name="labelFr" defaultValue={question.labelFr} required></input>
                    </label>
                    <label className={styles.submit}>
                        <span className={styles.label}>
                            <Translation dictionary={dictionary} trKey="admindbQuestionsEdit_label_name"/>
                            -DE
                        </span>
                        <input type="text" name="labelDe" defaultValue={question.labelDe} required></input>
                    </label>
                    <label className={styles.submit}>
                        <span className={styles.label}>
                            <Translation dictionary={dictionary} trKey="admindbQuestionsEdit_label_name"/>
                            -AR
                        </span>
                        <input type="text" name="labelAr" defaultValue={question.labelAr} required></input>
                    </label>
                    <label className={styles.submit}>
                        <span className={styles.label}>
                            <Translation dictionary={dictionary} trKey="admindbQuestionsEdit_label_dimension"/>
                        </span>
                        <select name="dimensionId" required defaultValue={question.dimensionId}>
                            {dimensions.map(dimension => {
                                return <option key={dimension.id} value={dimension.id}>{dimension[langLabel]}</option>
                            })}
                        </select>
                    </label>
                    <label className={styles.submit}>
                        <span className={styles.label}>
                            <Translation dictionary={dictionary} trKey="admindbQuestionsEdit_label_scale"/>
                        </span>
                        <select name="posneg" required defaultValue={question.posneg ? "pos" : "neg"}>
                            <option key="pos" value="pos">
                                <Translation dictionary={dictionary} trKey="admindbQuestionsEdit_option_standard"/>
                            </option>
                            <option key="neg" value="neg">
                                <Translation dictionary={dictionary} trKey="admindbQuestionsEdit_option_inversed"/>
                            </option>
                        </select>
                    </label>
                    <div className={styles.bottomButtonsContainer}>
                        <ReturnButton dictionary={dictionary} title={dictionary.admindbQuestionsEdit_return_title}
                                      href={`/admindb/questions`}/>
                        <div className={styles.delEditContainer}>
                            <div className={styles.deleteButtonContainer}>
                                <DeleteButton onClick={e => handleDelete(e)}
                                              title={dictionary.admindbQuestionsEdit_delete_title}
                                              label={dictionary.admindbQuestionsEdit_delete_label}/>
                            </div>
                            <div className={styles.confirmButtonContainer}>
                                <BlueButton type="submit" label={dictionary.admindbQuestionsEdit_edit_label}
                                            title={dictionary.admindbQuestionsEdit_edit_title}/>
                            </div>
                        </div>
                    </div>
                </form>
                {errorMsg && <FlashMessage msg={errorMsg} type='error'/>}
            </div>
        </Layout>
    )
}