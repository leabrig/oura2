import {useAdmin} from "../../../../lib/hooks";
import Layout from "../../../components/Layout";
import Head from "next/head";
import prisma from "../../../../lib/prisma";
import styles from "../../../styles/admindb.module.css";
import DBNav from "../../../components/DBNav";
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faPenToSquare, faPlus} from '@fortawesome/free-solid-svg-icons';
import Loading from "../../../components/loading";
import BlueButton from "../../../components/BlueButton";
import {getLanguageFile} from "../../../../lib/getLanguageFile";
import Translation from "../../../components/Translation";
import {getLanguageLabel} from "../../../../lib/getLanguageLabel";

export async function getServerSideProps({locale}) {
    const questions = await prisma.question.findMany({
        include: {
            dimension: {
                include: {
                    domain: true,
                },
            },
        },
    });
    const dictionary = getLanguageFile(locale);
    return {
        props: {questions: questions, dictionary: dictionary, locale:locale},
    };
}

export default function Index({questions, dictionary, locale}) {
    const {user, isLoading, isAdmin} = useAdmin({redirectTo: '/login'});
    const langLabel = getLanguageLabel(locale);

    //If user not returned yet from useUser -> Loading page, else error page!
    if (isLoading) {
        return (<Loading locale={locale} dictionary={dictionary}/>);
    } else if (!isLoading && !user) {
        return (<Loading locale={locale} dictionary={dictionary}/>);
    } else if (!isAdmin) {
        return (<Loading locale={locale} dictionary={dictionary}/>);
    }

    return (
        <Layout dictionary={dictionary} locale={locale}>
            <div className={styles.dbContainer}>
                <Head>
                    <title>{dictionary.admindbQuestions_header_title}</title>
                </Head>
                <DBNav dictionary={dictionary}/>
                <div className={styles.linkButtonContainer}>
                    <BlueButton isLink={true} title={dictionary.admindbQuestions_buttonNew_title} href="/admindb/questions/new" label={dictionary.admindbQuestions_buttonNew_label}>
                        <FontAwesomeIcon icon={faPlus} className={styles.newButtonIcon}/>
                    </BlueButton>
                </div>
                <table className={styles.tableFormat}>
                    <thead>
                    <tr className={styles.tableHead}>
                        <th className={styles.thFormatFirst}>
                            <Translation dictionary={dictionary} trKey="admindbQuestions_tableHeader_question"/>
                        </th>
                        <th className={styles.thFormat}>
                            <Translation dictionary={dictionary} trKey="admindbQuestions_tableHeader_dimension"/>
                        </th>
                        <th className={styles.thFormat}>
                            <Translation dictionary={dictionary} trKey="admindbQuestions_tableHeader_domain"/>
                        </th>
                        <th className={styles.lastThFormat}>
                            <Translation dictionary={dictionary} trKey="admindbQuestions_tableHeader_edit"/>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    {questions.map(question => {
                        return <tr key={question.id} className={styles.elementsTableHolder}>
                            <td className={styles.firstElementInTable}>
                                {question[langLabel]}
                            </td>
                            <td className={styles.elementInTable}>
                                {question.dimension[langLabel]}
                            </td>
                            <td className={styles.elementInTable}>
                                {question.dimension.domain[langLabel]}
                            </td>
                            <td className={styles.lastElementInTable}>
                                <div className={styles.confirmButtonContainer}>
                                    <BlueButton isLink={true} href={`/admindb/questions/edit/${question.id}`} title={dictionary.admindbQuestions_buttonEdit_title}>
                                        <FontAwesomeIcon icon={faPenToSquare}/>
                                    </BlueButton>
                                </div>
                            </td>
                        </tr>
                    })}
                    </tbody>
                </table>
            </div>
        </Layout>
    )
}