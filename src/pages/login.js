import {useEffect, useState} from 'react'
import {useRouter} from 'next/router'
import {useUser} from '../../lib/hooks'
import Form from '../components/Form'
import styles from '../styles/signup.module.css';
import Head from "next/head";
import Navbar from "../components/Navbar";
import Loading from "../components/loading";
import Link from "next/link";
import {getLanguageFile} from "../../lib/getLanguageFile";
import Translation from "../components/Translation";

export async function getServerSideProps({locale}) {
    const dictionary = getLanguageFile(locale);
    return {props: {dictionary: dictionary, locale:locale}}
}

export default function Login({dictionary, locale}) {
    const {user, isLoading} = useUser({redirectTo: '/dashboard', redirectIfFound: true});
    const [errorMsg, setErrorMsg] = useState('');
    const router = useRouter();
    const [isConnected, setIsConnected] = useState(false);

    useEffect(() => {
        if (!(user || isLoading)) {
            router.push('/login');
        } else if (user && !isLoading) {
            router.push('/dashboard');
        }
    }, [user, isLoading]);

    //if loading: load.
    if (isLoading) {
        return (<Loading dictionary={dictionary}/>);
    }

    async function handleSubmit(e) {
        e.preventDefault();

        if (errorMsg) setErrorMsg('');

        const body = {
            email: e.currentTarget.email.value,
            password: e.currentTarget.password.value,
        }

        try {
            const res = await fetch('/api/login', {
                method: 'POST',
                headers: {'Content-Type': 'application/json'},
                body: JSON.stringify(body),
            })
            if (res.ok) {
                // TODO: BUG IS STILL PRESENT. But it always arrives here, so router push has a problem ?
                try {
                    res.json().then(data => {
                        let localeLang;
                            if (data.language === 'FRENCH') {
                                localeLang = 'fr';
                            } else if (data.language === 'GERMAN'){
                                localeLang = 'de';
                            } else if (data.language === 'ENGLISH') {
                                localeLang = 'en';
                            } else if (data.language === 'ARABIC'){
                                localeLang = 'ar';
                            }
                        setTimeout(() => {
                            setIsConnected(true);
                        }, 20*60);
                        router.push('/dashboard', '/dashboard', {locale: localeLang});
                        }
                    )
                } catch (error) {
                    console.error('Router push error: ', error);
                    setErrorMsg('Failed to redirect. Please try again.')
                }
            } else throw new Error(await res.text())
        } catch (error) {
            console.error('An unexpected error occurred:', error);
            setErrorMsg(dictionary.login_error_invalidLogin);
        }
    }

    return (
        <div className={styles.pageBackground}>
            <Head>
                <title>{dictionary.login_header_title}</title>
            </Head>
            <Navbar dictionary={dictionary} locale={locale}/>
            <div className={styles.largeContainer}>
                <div className={styles.smallContainer}>
                    <h2>
                        <Translation dictionary={dictionary} trKey="login_header2_login"/>
                    </h2>
                    <Form dictionary={dictionary} isLogin errorMessage={errorMsg} onSubmit={e => handleSubmit(e)}/>
                    {isConnected && <p className={styles.notConnectedError}>
                        <Translation dictionary={dictionary} trKey="login_warning_notRedirected"/>
                        <span>
                            <Link href={`/dashboard`}>
                                <Translation dictionary={dictionary} trKey="login_warning_link"/>
                            </Link>
                        </span>
                        .
                    </p>}
                </div>
            </div>
        </div>
    )
}
