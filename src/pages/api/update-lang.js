import {updateLanguage} from "../../../lib/user";

export default async function updateLang(req, res) {
    if(req.method === 'PUT') {
        try {
            await updateLanguage(req.body);
            res.status(200).send({ done: true });
        } catch (error) {
            console.error(error);
            res.status(500).end(error.message);
        }
    }
}