import {checkCode, createAnswer} from "../../../lib/answer";

export default async function dimension(req, res) {
    if (req.method === 'PUT') {
        try {
            let checked = await checkCode(req.body.code);
            if(checked !== null) {
                res.status(200).send({done: true});
            } else {
                res.status(500).end("invalidCode")
            }
        } catch (error) {
            console.error(error);
            res.status(500).end(error.message);
        }
    } else if (req.method === 'POST') {
        try {
            await createAnswer(req.body);
            res.status(200).send({done: true});
        } catch (error) {
            console.error(error);
            res.status(500).end(error.message);
        }
    }
}