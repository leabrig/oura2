import {createDomain, deleteDomain, updateDomain} from "../../../lib/domain";

export default async function domain(req, res) {
    if(req.method === 'POST'){
        try{
            await createDomain(req.body);
            res.status(200).send({done: true});
        } catch (error) {
            console.error(error);
            res.status(500).end(error.message);
        }
    } else if(req.method === 'PUT'){
        try{
            await updateDomain(req.body);
            res.status(200).send({done: true});
        } catch (error) {
            console.error(error);
            res.status(500).end(error.message);
        }
    } else if(req.method === 'DELETE'){
        try{
            await deleteDomain(req.query.id);
            res.status(200).send({done: true});
        } catch (error) {
            console.error(error);
            res.status(500).end(error.message);
        }
    }
}