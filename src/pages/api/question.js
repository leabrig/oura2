import {createQuestion, deleteQuestion, updateQuestion} from "../../../lib/question";

export default async function question(req, res) {
    if(req.method === 'POST') {
        try{
            await createQuestion(req.body);
            res.status(200).send({done: true});
        } catch (error) {
            console.error(error);
            res.status(500).end(error.message);
        }
    } else if(req.method === 'PUT') {
        try{
            await updateQuestion(req.body);
            res.status(200).send({done: true});
        } catch (error) {
            console.error(error);
            res.status(500).end(error.message);
        }
    } else if(req.method === 'DELETE') {
        try{
            await deleteQuestion(req.query.id);
            res.status(200).send({done: true});
        } catch (error) {
            console.error(error);
            res.status(500).end(error.message);
        }
    }
}