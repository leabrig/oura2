import {deleteUser} from "../../../lib/user";

export default async function userDelete(req, res) {
    if (req.method === 'DELETE') {
        try {
            await deleteUser(req.query.id);
            res.status(200).send({done: true});
        } catch (error) {
            console.error(error);
            res.status(500).end(error.message);
        }
    }
}