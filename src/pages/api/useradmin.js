import {updateAdmin} from "../../../lib/user";

export default async function useradmin(req, res) {
    if(req.method === 'PUT'){
        try {
            await updateAdmin(req.body);
            res.status(200).send({done: true});
        } catch (error) {
            res.status(500).end(error.message);
        }
    }
}