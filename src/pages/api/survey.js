import {createSurvey, updateArchived, deleteSurvey} from "../../../lib/survey";

export default async function survey(req, res) {
    if (req.method === 'POST') {
        try {
            const survey = await createSurvey(req.body);
            res.status(200).send({ survey });
        } catch (error) {
            console.error(error);
            res.status(500).end(error.message);
        }
    } else if (req.method === 'PUT') {
        try {
            await updateArchived(req.body);
            res.status(200).send({done:true});
        } catch (error) {
            console.error(error);
            res.status(500).end(error.message);
        }
    } else if (req.method === 'DELETE') {
        try{
            await deleteSurvey(req.query.id);
            res.status(200).send({done: true});
        } catch (error) {
            console.error(error);
            res.status(500).end(error.message);
        }
    }
}