import {checkRecoveryCode, deleteRecoveryCode, generateRecoveryCode} from "../../../lib/pswd-recovery";

export default async function passwordRecovery(req, res) {
    if (req.method === 'POST') {
        try {
            const body = await generateRecoveryCode(req.body);

            if (!body) {
                res.status(204).json();
            } else {
                res.status(200).json(body);
            }
        } catch (error) {
            console.error(error);
            res.status(500).end('An unexpected error occurred: ', error);
        }
    } else if (req.method === 'PUT') {
        try {
            const body = await checkRecoveryCode(req.body);
            res.status(200).json(body);
        } catch (error) {
            console.error(error);
            res.status(500).end('An unexpected error occurred: ', error);
        }
    } else if (req.method === 'DELETE') {
        try {
            await deleteRecoveryCode(req.query.id);
            res.status(200).json({done: true});
        } catch (error) {
            console.error(error);
            res.status(500).end(error.message);
        }
    }

 }