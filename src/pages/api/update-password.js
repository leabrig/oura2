import {updatePassword} from "../../../lib/user";

export default async function updateUserPassword(req, res) {
    if(req.method === 'POST') {
        try {
            const { id, email, password } = req.body;
            await updatePassword({ id, email, password });
            res.status(200).send({ done: true });
        } catch (error) {
            console.error(error);
            res.status(500).end(error.message);
        }
    }
}