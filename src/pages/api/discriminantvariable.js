import {deleteDVariable, updateArchive} from "../../../lib/discriminantvariable";

export default async function discriminantvariable(req, res) {
    if (req.method === 'DELETE') {
        try{
            await deleteDVariable(req.query.id);
            res.status(200).send({done: true});
        } catch (error) {
            console.error(error);
            res.status(500).end(error.message);
        }
    } else if (req.method === 'PUT') {
        try {
            await updateArchive(req.body);
            res.status(200).send({done:true});
        } catch (error) {
            console.error(error);
            res.status(500).end(error.message);
        }
    }
}