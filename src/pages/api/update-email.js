import {updateEmail} from "../../../lib/user";
import {getLoginSession, setLoginSession} from "../../../lib/auth";

export default async function updateUserEmail(req, res) {
    if(req.method === 'POST') {
        try {
            // Check if user is authenticated
            const session = await getLoginSession(req);

            if (!session) {
                res.status(401).send("Unauthorized");
                return;
            }

            const { oldEmail, newEmail } = req.body;
            await updateEmail({ oldEmail, newEmail });

            // Update session with new email
            session.email = newEmail;
            await setLoginSession(res, session);

            res.status(200).send({ done: true });
        } catch (error) {
            console.error(error);
            res.status(500).end(error.message);
        }
    }
}