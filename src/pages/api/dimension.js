import {createDimension, deleteDimension, updateDimension} from "../../../lib/dimension";

export default async function dimension(req, res) {
    if(req.method === 'POST'){
        try{
            await createDimension(req.body);
            res.status(200).send({done: true});
        } catch (error) {
            console.error(error);
            res.status(500).end(error.message);
        }
    } else if(req.method === 'PUT'){
        try{
            await updateDimension(req.body);
            res.status(200).send({done: true});
        } catch (error) {
            console.error(error);
            res.status(500).end(error.message);
        }
    } else if(req.method === 'DELETE'){
        try{
            await deleteDimension(req.query.id);
            res.status(200).send({done: true});
        } catch (error) {
            console.error(error);
            res.status(500).end(error.message);
        }
    }

}