import {sendMail} from "../../../lib/send-mail";

export default async function mail(req, res) {
    if(req.method === 'POST') {
        try{
            await sendMail(req.body);
            res.status(200).send({done: true});
        } catch (error) {
            console.error(error);
            res.status(500).end(error.message);
        }
    }
}