import Layout from '../components/Layout';
import styles from '../styles/home.module.css';
import Link from "next/link";
import Image from "next/image";
import productionImg from "../../public/images/production.png";
import feedbackImg from "../../public/images/feedback.png";
import logo from "../../public/images/oura2-logo.jpeg";
import {getLanguageFile} from "../../lib/getLanguageFile";
import Translation from "../components/Translation";

export async function getServerSideProps({locale}) {
    const dictionary = getLanguageFile(locale);
    return {props: {dictionary: dictionary, locale:locale}}
}

export default function Home({dictionary, locale}) {

    return (
        <Layout dictionary={dictionary} locale={locale}>
            <div className={styles.mainContainer}>
                <div className={styles.leftSide}>
                    {/*<Image src={logo} alt="logo" width={300}/>*/}
                    <h1>OURA²</h1>
                    <h2>
                        <Translation dictionary={dictionary} trKey="homepage_header_title"/>
                    </h2>
                    <p>
                        <Translation dictionary={dictionary} trKey="homepage_header_summary"/>
                    </p>
                </div>
                <div className={styles.rightSide}>
                    <div className={styles.firstImage}>
                        <Image src={feedbackImg} alt={dictionary.homepage_firstImage_alt}
                               style={{maxWidth: '100%', height: 'auto',}}/>
                    </div>
                </div>
            </div>
            <div className={styles.buttonContainer}>
                <Link href="/answer" className={styles.surveyButton}>
                    <Translation dictionary={dictionary} trKey="homepage_button_title"/>
                </Link>
            </div>
            <div className={styles.mainContainer}>
                <div className={styles.leftSide}>
                    <div className={styles.secondImage}>
                        <Image src={productionImg} alt={dictionary.homepage_secondImage_alt}
                               style={{maxWidth: '100%', height: 'auto',}}/>
                    </div>
                </div>
                <div className={styles.rightSide}>
                    <h2>
                        <Translation dictionary={dictionary} trKey="homepage_secondHeader_title"/>
                    </h2>
                    <p>
                        <Translation dictionary={dictionary} trKey="homepage_secondHeader_summary"/>
                    </p>
                </div>
            </div>
        </Layout>
    )
}
