import {useUser} from '../../lib/hooks'
import Layout from '../components/Layout'
import Head from "next/head";
import styles from "../styles/profile.module.css";
import Link from "next/link";
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faArrowRightFromBracket, faCircleExclamation} from '@fortawesome/free-solid-svg-icons';
import {useState} from "react";
import FlashMessage from "../components/FlashMessage";
import Loading from "../components/loading";
import BlueButton from "../components/BlueButton";
import {useRouter} from "next/router";
import DeleteButton from "../components/DeleteButton";
import {getLanguageFile} from "../../lib/getLanguageFile";
import Translation from "../components/Translation";

export async function getServerSideProps({locale}) {
    const dictionary = getLanguageFile(locale);

    return {props: {dictionary: dictionary, locale: locale}};
}

export default function Profile({dictionary, locale}) {
    const {user, isLoading} = useUser({redirectTo: '/login'});
    const [errorMsg, setErrorMsg] = useState('');
    const [successMsg, setSuccessMsg] = useState('');
    const router = useRouter();
    //RegExp to check if it's a valid email form and prevent matching multiple @ signs
    const re = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;

    //If user not returned yet from useUser -> Loading page, else error page!
    if(isLoading){
        return (<Loading dictionary={dictionary} locale={locale}/>);
    } else if (!isLoading && !user) {
        return (<Loading dictionary={dictionary} locale={locale}/>);
    }

    let userMail = '';
    if (user && user.email) {
        userMail = user.email;
    }

    let userLocale = '';
    if (user.language === 'FRENCH'){
        userLocale = 'fr';
    } else if (user.language === 'GERMAN'){
        userLocale = 'de';
    } else if (user.language === 'ENGLISH'){
        userLocale = 'en';
    } else if (user.language === 'ARABIC'){
        userLocale = 'ar';
    }

    async function handleEmailChange(e) {
        e.preventDefault();

        // Get the new email value from the state variable
        const body = {
            oldEmail: userMail,
            newEmail: e.currentTarget.user_email.value,
        }

        if (errorMsg) await setErrorMsg('');

        if (!re.test(body.newEmail)) {
            setErrorMsg(dictionary.profile_errorMsg_emailInvalid);
            return;
        }

        try {
            // Send the new email value to the server
            const response = await fetch('/api/update-email', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(body),
            });

            if (response.status === 200) {
                window.location.reload();
            } else {
                throw new Error(await response.text())
            }
        } catch (error) {
            console.error('An unexpected error occurred:', error);
            if(error.message.includes("Unique constraint failed on the fields: (`email`)")) {
                setErrorMsg(dictionary.profile_errorMsg_emailUniqueConstraint);
            } else {
                setErrorMsg(error.message);
            }
        }
    }

    async function handlePasswordChange(e) {
        e.preventDefault();

        setErrorMsg('');

        const body = {
            email: userMail,
            password: e.currentTarget.password.value,
        }

        if (body.password !== e.currentTarget.rpassword.value) {
            await setErrorMsg('');
            setErrorMsg(dictionary.profile_errorMsg_passwordsDontMatch)
            return;
        }

        try {
            const res = await fetch('/api/update-password', {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify(body),
            })
            if (res.status === 200) {
                window.location.reload();
            } else {
                throw new Error(await res.text())
            }
        } catch (error) {
            console.error('An unexpected error occurred:', error)
            setErrorMsg(error.message);
        }
    }

    // TODO: when e-mail up -> send a confirmation e-mail that the account has been successfully deleted.
    async function handleDelete(e){
        e.preventDefault();
        const confirmDelete = confirm(dictionary.profile_confirm_deleteAccount);
        if (!confirmDelete) return;

       if (errorMsg) setErrorMsg('');

       try {
           const res = await fetch(`/api/user-delete?id=${user.id}`, {
               method: 'DELETE',
               headers: {'Content-Type': 'application/json'},
           })
           if (res.ok) {
               await router.push('/api/logout?isAccountDeleted=true');
           } else {
               throw new Error(await res.text());
           }
       } catch (error) {
           console.error('An unexpected error occured:', error);
           setErrorMsg(error.message);
        }
    }

    async function handleLanguageSelect(e) {
        e.preventDefault();
        setErrorMsg('');
        setSuccessMsg('');
        const languageChosen = e.target.value;
        const body = {
            email: userMail,
            language: languageChosen,
        }

        try {
            const res = await fetch(`/api/update-lang`, {
                method: 'PUT',
                headers: {'Content-Type': 'application/json'},
                body: JSON.stringify(body),
            })
            if (!res.ok) {
                throw new Error(await res.text());
            }
        } catch (error) {
            console.error('An unexpected error occurred:', error);
            setErrorMsg(error.message);
        }

        await router.push('/profile', '/profile', {locale: languageChosen});
        setSuccessMsg(dictionary.profile_success_changedLang);
    }

    return (
        <Layout dictionary={dictionary} locale={locale}>
            <Head>
                <title>{dictionary.profile_header_title}</title>
            </Head>
            <div className={styles.smallerPageContainer}>
                <h1>
                    <Translation dictionary={dictionary} trKey="profile_header1_myAccount"/></h1>
                <div className={styles.disconnectButtonContainer}>
                    <Link href="/api/logout" className={styles.disconnectButton}>
                        <FontAwesomeIcon icon={faArrowRightFromBracket} className={styles.buttonIcon}/>
                        <span title={dictionary.profile_button_disconnect}>
                            <Translation dictionary={dictionary} trKey="profile_button_disconnect"/>
                        </span>
                    </Link>
                </div>
                <div className={styles.elementContainer}>
                    <h2>
                        <Translation dictionary={dictionary} trKey="profile_header2_email"/></h2>
                    <div className={styles.displayEmail}>
                        {userMail}
                    </div>
                    <p>
                        <Translation dictionary={dictionary} trKey="profile_summary_email"/>
                    </p>
                    <div>
                        <form onSubmit={e => handleEmailChange(e)}>
                            <label htmlFor="mail"></label>
                            <input type="email" id="mail" name="user_email" defaultValue={userMail}
                                   className={styles.inputField} required/>
                            <div className={styles.confirmButtonContainer}>
                                <BlueButton type="submit" title={dictionary.profile_button_changeEmail}
                                            label={dictionary.profile_button_changeEmail} isLink={false}/>
                            </div>
                        </form>
                    </div>
                </div>
                <div className={styles.elementContainer}>
                    <h2>
                        <Translation dictionary={dictionary} trKey="profile_header2_password"/>
                    </h2>
                    <p>
                        <Translation dictionary={dictionary} trKey="profile_summary_password"/>
                    </p>
                    <div>
                        <form onSubmit={e => handlePasswordChange(e)}>
                            <label htmlFor="password" className={styles.label}>
                                <Translation dictionary={dictionary} trKey="profile_label_password"/>
                            </label>
                            <input type="password" pattern=".{8,}" id="password" name="user_password" className={styles.inputField}
                                   required/>

                            <label htmlFor="rpassword" className={styles.secondLabel}>
                                <Translation dictionary={dictionary} trKey="profile_label_repeatPassword"/>
                            </label>
                            <input type="password" id="rpassword" pattern=".{8,}" name="rpassword" className={styles.inputField}
                                   required/>
                            <div className={styles.confirmButtonContainer}>
                                <BlueButton type="submit" title={dictionary.profile_button_changePassword} label={dictionary.profile_button_changePassword} isLink={false}/>
                            </div>
                        </form>
                    </div>
                </div>
                <div className={styles.elementContainer}>
                    <h2>
                        <Translation dictionary={dictionary} trKey="profile_header2_language"/>
                    </h2>
                    <p>
                        <Translation dictionary={dictionary} trKey="profile_description_activeLanguage"/>
                    </p>
                    <p>
                        <Translation dictionary={dictionary} trKey="profile_summary_language"/>
                    </p>
                    <div className={styles.savedLanguageContainer}>
                        <p>
                            <Translation dictionary={dictionary} trKey="profile_description_savedLanguage"/>
                        </p>
                        <select className={styles.languageSelection} defaultValue={userLocale} id="language-select"
                                name="language" onChange={(e) => handleLanguageSelect(e)}>
                            <option value="fr">Français</option>
                            <option value="de">Deutsch</option>
                            <option value="en">English</option>
                            {/*<option value="ar">العربية</option>*/}
                        </select>
                    </div>
                </div>
                <div>
                    <h2>
                        <Translation dictionary={dictionary} trKey="profile_header2_deleteAccount"/>
                    </h2>
                    <p>
                        <Translation dictionary={dictionary} trKey="profile_summary_delete"/>
                    </p>
                    <p className={styles.dangerZone}>
                        <FontAwesomeIcon icon={faCircleExclamation} className={styles.dangerIcon}/>
                        <Translation dictionary={dictionary} trKey="profile_warning_delete"/>
                    </p>
                    <div className={styles.confirmButtonContainer}>
                        <DeleteButton onClick={e => handleDelete(e)}
                                      title={dictionary.profile_buttonTitle_delete}
                                      label={dictionary.profile_buttonLabel_delete}/>
                    </div>
                </div>
                {errorMsg && <FlashMessage msg={errorMsg} type='error'/>}
                {successMsg && <FlashMessage msg={successMsg} type='success'/> }
            </div>
        </Layout>
    )
}