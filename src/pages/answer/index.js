import styles from "../../styles/answer.module.css";
import AnswerHeader from "../../components/AnswerHeader";
import Head from "next/head";
import {useRouter} from "next/router";
import FlashMessage from "../../components/FlashMessage";
import {useRef, useState} from "react";
import LayoutAnswerPage from "../../components/LayoutAnswerPage";
import BlueButton from "../../components/BlueButton";
import {getLanguageFile} from "../../../lib/getLanguageFile";
import Translation from "../../components/Translation";

export async function getServerSideProps({locale}) {
    const dictionary = getLanguageFile(locale);
    return {props: {dictionary: dictionary, locale:locale}}
}

export default function Answer({dictionary, locale}) {
    const [errorMsg, setErrorMsg] = useState('')
    const codeInputRef = useRef(null);
    const router = useRouter();

    async function handleSubmit(e) {
        e.preventDefault();

        if (errorMsg) setErrorMsg('');

        const body = {
            code: e.currentTarget.code.value,
        }

        try {
            const res = await fetch('/api/answer', {
                method: 'PUT',
                headers: {'Content-Type': 'application/json'},
                body: JSON.stringify(body),
            })
            if (res.status === 200) {
                await router.push(`/answer/${body.code}`);
            } else {
                throw new Error(await res.text());
            }
        } catch (error) {
            console.error('An unexpected error occurred:', error);
            if(error.message === 'invalidCode') {
                setErrorMsg(dictionary.answer_errorMsg_invalidCode);
            } else {
                setErrorMsg(error.message);
            }
            codeInputRef.current.value = '';
        }
    }

    return (
        <LayoutAnswerPage>
                <main>
                    <Head>
                        <title>{dictionary.answerCode_header_title}</title>
                    </Head>
                    <AnswerHeader dictionary={dictionary} locale={locale}/>
                    <div className={styles.simpleWhiteContainer}>
                        <h2>
                            <Translation dictionary={dictionary} trKey="answerCode_header2_label"/>
                        </h2>
                        <div>
                            <p>
                                <Translation dictionary={dictionary} trKey="answerCode_info_text"/>
                            </p>
                        </div>
                        <div>
                            {errorMsg && <FlashMessage msg={errorMsg} type='error'/>}
                            <form onSubmit={handleSubmit}>
                                <input
                                    type="text"
                                    className={styles.codeInput}
                                    title={dictionary.answerCode_input_title}
                                    name="code"
                                    ref={codeInputRef}
                                />
                                <div className={styles.confirmButtonContainer}>
                                    <BlueButton isLink={false} type="submit" title={dictionary.answerCode_confirmButton_title} label={dictionary.answerCode_confirmButton_label}/>
                                </div>
                            </form>
                        </div>
                    </div>
                </main>
        </LayoutAnswerPage>
    )
}
