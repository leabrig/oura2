import AnswerHeader from "../../../components/AnswerHeader";
import styles from "../../../styles/answer.module.css";
import Head from "next/head";
import prisma from "../../../../lib/prisma";
import LayoutAnswerPage from "../../../components/LayoutAnswerPage";
import {getLanguageFile} from "../../../../lib/getLanguageFile";
import Translation from "../../../components/Translation";

export async function getServerSideProps(context) {
    const notSerializedSurvey = await prisma.survey.findUnique({
        where: {
            uniqueStringForm: context.query.id,
        }
    });

    const survey = {
        ...notSerializedSurvey,
        createdAt: notSerializedSurvey.createdAt.toISOString(),
    };
    const locale = context.locale;
    const dictionary = getLanguageFile(locale);

    return {
        props: {dictionary: dictionary, survey:survey, locale:locale},
    };
}

export default function formResponse({dictionary, survey, locale}) {

    return (
        <LayoutAnswerPage>
            <main>
                <Head>
                    <title>{dictionary.formResponse_header_title}</title>
                </Head>
                <div>
                    <AnswerHeader dictionary={dictionary} locale={locale}/>
                    <div className={styles.simpleWhiteContainer}>
                        <Translation dictionary={dictionary} trKey="formResponse_div_message"/>
                        <span className={styles.formResponseLabel}>
                            {survey.label}
                        </span>
                        !
                    </div>
                </div>
            </main>
        </LayoutAnswerPage>
    )
}
