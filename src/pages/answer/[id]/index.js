import styles from "../../../styles/answer.module.css";
import AnswerHeader from "../../../components/AnswerHeader";
import Head from "next/head";
import {faBarcode, faCalendarDays} from '@fortawesome/free-solid-svg-icons';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {useRouter} from "next/router";
import {useCallback, useEffect, useState} from "react";
import prisma from "../../../../lib/prisma";
import FlashMessage from "../../../components/FlashMessage";
import LayoutAnswerPage from "../../../components/LayoutAnswerPage";
import BlueButton from "../../../components/BlueButton";
import {faThumbsDown, faThumbsUp} from "@fortawesome/free-regular-svg-icons";
import {getLanguageFile} from "../../../../lib/getLanguageFile";
import Translation from "../../../components/Translation";
import {getLanguageLabel} from "../../../../lib/getLanguageLabel";

export async function getServerSideProps(context) {
    const survey = await prisma.survey.findUnique({
        where: {
            uniqueStringForm: context.query.id,
        },
        include: {
            discriminantvariables: {
                include: {
                    discriminantitems: true,
                },
            },
            questions: true,
        },
    });

    const serializedSurvey = {
        ...survey,
        createdAt: survey.createdAt.toISOString(),
    };
    const locale = context.locale;
    const dictionary = getLanguageFile(locale);

    return { props: {dictionary: dictionary, survey: serializedSurvey, locale: locale} };
}

export default function AnswerSurvey({dictionary, survey, locale}) {
    let createdAt = new Date(survey.createdAt);
    let date = createdAt.toLocaleDateString('en-GB');
    let questionCount = 0;
    const surveyKey = `answeredSurvey_${survey.id}`;
    const router = useRouter();
    const [errorMsg, setErrorMsg] = useState('');
    const [shuffledQuestions, setShuffledQuestions] = useState([]);
    const [unansweredQuestionIds, setUnansweredQuestionIds] = useState([]);
    const [surveyAlreadyAnswered, setSurveyAlreadyAnswered] = useState(false);
    const langLabel = getLanguageLabel(locale);

    // Shuffle the questions
    useEffect(() => {
        const shuffled = [...survey.questions].sort(() => Math.random() - 0.5);
        setShuffledQuestions(shuffled);
    }, []);

    // Set survey already answered if it's the case
    useEffect(() => {
        if(typeof sessionStorage !== 'undefined') {
            setSurveyAlreadyAnswered(sessionStorage.getItem(surveyKey));
        }
    }, [])

    async function handleSubmit(e) {
        e.preventDefault();

        const form = new FormData(e.target);

        const questionsAnswers = survey.questions.map((question) => {
            const choice = form.get(`question-${question.id}`);

            return {
                questionId: question.id,
                questionChoice: parseInt(choice),
            }
        })

        const variablesAnswers = survey.discriminantvariables.map((variable) => {
            const choice = form.get(`variable-${variable.id}`);

            return {
                discriminantvariableId: variable.id,
                itemChoice: choice,
            }
        })

        const unansweredIds = [];
        survey.questions.forEach((question) => {
            const questionChoice = form.get(`question-${question.id}`);
            if (!questionChoice) {
                unansweredIds.push(question.id);
            }
        });

        survey.discriminantvariables.forEach((variable) => {
            const itemChoice = form.get(`variable-${variable.id}`);
            if (!itemChoice) {
                unansweredIds.push(variable.id);
            }
        });

        // If not all questions have been answered alert + set the unanswered box red.
        if (unansweredIds.length > 0) {
            await setErrorMsg('');
            setErrorMsg(dictionary.answer_errorMsg_notAllAnswered);
            setUnansweredQuestionIds(unansweredIds);
            return;
        }

        const surveyAnswer = {
            surveyId: survey.id,
            questionsAnswers: questionsAnswers,
            variablesAnswers: variablesAnswers,
        }

        try {
            const res = await fetch('/api/answer', {
                method: 'POST',
                headers: {'Content-Type': 'application/json'},
                body: JSON.stringify(surveyAnswer),
            })
            if (res.status === 200) {
                sessionStorage.setItem(surveyKey, 'answered');
                await router.push(`/answer/${survey.uniqueStringForm}/formResponse`)
            } else {
                throw new Error(await res.text());
            }
        } catch (error) {
            console.error('An unexpected error occurred:', error);
            setErrorMsg(error.message);
        }
    }

    // Watch and return media width (can this be an external function ?)
    const useMediaQuery = (width) => {
        const [targetReached, setTargetReached] = useState(false);

        const updateTarget = useCallback((e) => {
            if (e.matches) {
                setTargetReached(true);
            } else {
                setTargetReached(false);
            }
        }, []);

        useEffect(() => {
            const media = window.matchMedia(`(max-width: ${width}px)`);
            media.addEventListener("change", updateTarget);

            if (media.matches) {
                setTargetReached(true);
            }

            return () => media.removeEventListener("change", updateTarget);
        }, []);

        return targetReached;
    };

    const isBreakPoint = useMediaQuery(599);


    const title = dictionary.answer_header_title+survey.label;

    return (
        <LayoutAnswerPage>
            <main>
                <Head>
                    <title>
                        {title}
                    </title>
                </Head>
                <AnswerHeader dictionary={dictionary} locale={locale}/>
                <div className={styles.topColorOnBanner}/>
                <div className={styles.titleBannerContainer}>
                        <div className={styles.surveyTitle}>
                            {survey.label}
                        </div>
                        <div className={styles.surveyInfosContainer}>
                            <div className={styles.surveyInfos}>
                                <FontAwesomeIcon icon={faBarcode} className={styles.icon}/>
                                {survey.uniqueStringForm}
                            </div>
                            <div className={styles.surveyInfos}>
                                <FontAwesomeIcon icon={faCalendarDays} className={styles.icon}/>
                                {date}
                            </div>
                        </div>
                    {isBreakPoint ? (
                            <div className={styles.scaleInfoContainer}>
                                <FontAwesomeIcon icon={faThumbsDown} className={styles.scaleInfoIcon}/>
                                <span>
                                    <Translation dictionary={dictionary} trKey="answer_headerInfo_disagree"/>
                                </span>
                                <FontAwesomeIcon icon={faThumbsUp} className={styles.scaleInfoIcon}/>
                                <span>
                                    <Translation dictionary={dictionary} trKey="answer_headerInfo_agree"/>
                                </span>
                            </div>
                    ) : ('')
                    }
                </div>
                {surveyAlreadyAnswered ? (
                    <div className={styles.simpleWhiteContainer}>
                        <div className={styles.alreadyAnsweredText}>
                            <Translation dictionary={dictionary} trKey="answer_successMessage_alreadyAnswered"/>
                        </div>
                    </div>
                ) : (
                    <form onSubmit={handleSubmit}>
                        {shuffledQuestions.map((question, index) => {
                            const isUnanswered = unansweredQuestionIds.includes(question.id);
                            questionCount = index + 1;

                            return (
                                <div className={`${styles.simpleWhiteContainer} ${isUnanswered ? styles.unansweredQuestion : ''}`} key={questionCount}>
                                    <span className={styles.questionLabel}>
                                        {index + 1}. {question[langLabel]}
                                    </span>
                                    <div className={styles.scaleContainer}>
                                        {isBreakPoint ? (
                                            <FontAwesomeIcon icon={faThumbsDown} className={styles.scaleIcon}/>
                                        ) : (
                                            <div className={styles.scaleLabel}>
                                                <Translation dictionary={dictionary} trKey="answer_infoScale_disagree"/>
                                            </div>
                                        )}
                                        {[...Array(6)].map((_, itemIndex) => {
                                            return (
                                                <div
                                                    className={styles.radioButtonAndLabelContainer}
                                                    key={`question-${question.id}-${itemIndex + 1}`}
                                                    onClick={() => {
                                                        const radioInput = document.getElementById(`question-${question.id}-${itemIndex + 1}`);
                                                        if (radioInput) {
                                                            radioInput.click();
                                                        }
                                                    }}
                                                >
                                                    <input
                                                        id={`question-${question.id}-${itemIndex + 1}`}
                                                        key={`question-${question.id}-${itemIndex + 1}`}
                                                        name={`question-${question.id}`}
                                                        type="radio"
                                                        value={itemIndex + 1}
                                                        className={styles.radioButton}
                                                    />
                                                    <label className={styles.radioButtonLabel}>
                                                        {itemIndex + 1}
                                                    </label>
                                                </div>
                                            );
                                        })}
                                        {isBreakPoint ? (
                                            <FontAwesomeIcon icon={faThumbsUp} className={styles.scaleIcon}/>
                                        ) : (
                                            <div className={styles.scaleLabel}>
                                                <Translation dictionary={dictionary} trKey="answer_infoScale_agree"/>
                                            </div>
                                        )}
                                    </div>
                                </div>
                            );
                        })}
                        {survey.discriminantvariables.map((variable, index) => {
                            const isUnanswered = unansweredQuestionIds.includes(variable.id);
                            questionCount = questionCount + index;
                            return (
                                <div className={`${styles.simpleWhiteContainer} ${isUnanswered ? styles.unansweredQuestion : ''}`} key={questionCount}>
                                    <span className={styles.questionLabel}>
                                        {questionCount + 1}. {variable.label}
                                    </span>
                                    <div>
                                        <div className={styles.scaleContainer}>
                                            {variable.discriminantitems.map((item) => {
                                                return (
                                                    <div
                                                        className={styles.radioButtonAndLabelContainerForDV}
                                                        key={`variable-${variable.id}-${item.id}`}
                                                        onClick={() => {
                                                            const radioInput = document.getElementById(`variable-${variable.id}-${item.id}`);
                                                            if (radioInput) {
                                                                radioInput.click();
                                                            }
                                                        }}>
                                                        <input
                                                            id={`variable-${variable.id}-${item.id}`}
                                                            key={`variable-${variable.id}-${item.id}`}
                                                            name={`variable-${variable.id}`}
                                                            type="radio"
                                                            value={item.id}
                                                            className={styles.radioButton}
                                                        />
                                                        <label className={styles.radioButtonLabel}>
                                                            {item.label}
                                                        </label>
                                                    </div>
                                                );
                                            })}
                                        </div>
                                    </div>
                                </div>
                            );
                        })}
                        {errorMsg && <FlashMessage msg={errorMsg} type='error'/>}
                        <div className={styles.confirmButtonContainer}>
                            <BlueButton isLink={false} type="submit" title={dictionary.answer_submitButton_title} label={dictionary.answer_submitButton_label}/>
                        </div>
                    </form>
                )}
            </main>
        </LayoutAnswerPage>
    )
}