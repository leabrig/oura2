import pageNotFoundImg from "../../public/images/page-not-found.png";
import Image from "next/image";
import styles from '../styles/404.module.css';
import BlueButton from "../components/BlueButton";

export default function Custom404() {
    return <div className={styles.container}>
        <div className={styles.image}>
            <Image src={pageNotFoundImg} style={{maxWidth: '100%', height: 'auto',}} alt="Error 404."/>
        </div>
        <div className={styles.pageNotFoundText}>This page could not be found</div>
        <div className={styles.buttonContainer}>
            <BlueButton title="Back to homepage" label="Back to homepage" isLink={true} href={`/`}/>
        </div>
    </div>
}