import { Html, Head, Main, NextScript } from 'next/document';

export default function Document() {
    return (
        <Html lang='en'>
            {/*Something is wrong here, it doesn't work the favicon is not showing up*/}
            <Head>
                <link rel="shortcut icon" href="/layout/favicon/favicon.ico"/>
                <link rel="apple-touch-icon" sizes="180x180" href="/layout/favicon/apple-touch-icon.png"/>
                <link rel="icon" type="image/png" sizes="32x32" href="/layout/favicon/favicon-32x32.png"/>
                <link rel="icon" type="image/png" sizes="16x16" href="/layout/favicon/favicon-16x16.png"/>
                <link rel="manifest" href="/layout/favicon/site.webmanifest"/>
                <link rel="mask-icon" href="/layout/favicon/safari-pinned-tab.svg" color="#5bbad5"/>
                <meta name="msapplication-TileColor" content="#da532c"/>
                <meta name="theme-color" content="#ffffff"/>
            </Head>
            <body>
            <Main />
            <NextScript />
            </body>
        </Html>
    );
}