import { useState } from 'react';
import {useRouter} from 'next/router';
import { useUser } from '../../lib/hooks';
import Form from '../components/Form';
import styles from '../styles/signup.module.css';
import Head from "next/head";
import Navbar from "../components/Navbar";
import Loading from "../components/loading";
import {getLanguageFile} from "../../lib/getLanguageFile";
import Translation from "../components/Translation";

export async function getServerSideProps({locale}) {
  const dictionary = getLanguageFile(locale);
  return {props: {dictionary: dictionary, locale:locale}}
}
export default function Signup ({dictionary, locale}) {
  const {isLoading} = useUser({ redirectTo: '/', redirectIfFound: true });
  const [errorMsg, setErrorMsg] = useState('');
  const [forgotPassword, setForgotPassword] =useState(false);
  const router = useRouter();

  //RegExp to check if it's a valid email form and prevent matching multiple @ signs
  const re = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;

  //If loading: load page.
  if(isLoading){
    return (<Loading dictionary={dictionary}/>);
  }

  async function handleSubmit(e) {
    e.preventDefault()

    setErrorMsg('');

    const body = {
      email: e.currentTarget.email.value,
      password: e.currentTarget.password.value,
      language: locale,
    }

    if (body.password !== e.currentTarget.rpassword.value) {
      await setErrorMsg('');
      setErrorMsg(dictionary.signup_errorMsg_passwordsDontMatch);
      return;
    }

    if (!re.test(body.email)){
      setErrorMsg(dictionary.signup_errorMsg_emailInvalid);
      return;
    }

    // Fetch server response to login attempt
    try {
      setErrorMsg('');
      const res = await fetch('/api/signup', {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(body),
      })
      if (res.status === 200) {
        await router.push('/login');
      } else {
        throw new Error(await res.text());
      }
    } catch (error) {
      if(error.message.includes("Unique constraint failed on the fields: (`email`)")) {
        console.error('An unexpected error occurred.')
        setForgotPassword(true);
        setErrorMsg(dictionary.signup_errorMsg_emailUniqueConstraint)
      } else {
        console.error('An unexpected error occurred:', error)
        setErrorMsg(error.message);
      }
    }
  }

  return (
      <div className={styles.pageBackground}>
        <Head>
          <title>{dictionary.signup_header_title}</title>
        </Head>
        <Navbar dictionary={dictionary} locale={locale}/>
        <div className={styles.largeContainer}>
          <div className={styles.smallContainer}>
            <h2>
              <Translation dictionary={dictionary} trKey="signup_header2_signup"/>
            </h2>
            <Form dictionary={dictionary} isLogin={false} errorMessage={errorMsg} forgotPassword={forgotPassword} onSubmit={e => handleSubmit(e)}/>
          </div>
        </div>
      </div>
  )

}

