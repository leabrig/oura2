import Layout from "../../../components/Layout";
import prisma from "../../../../lib/prisma";
import styles from "../../../styles/mysurveys.module.css";
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faChevronUp, faChevronDown, faArchive} from '@fortawesome/free-solid-svg-icons';
import {useState} from "react";
import Head from "next/head";
import ReturnButton from "../../../components/ReturnButton";
import SurveyActionPanel from "../../../components/SurveyActionPanel";
import {getLoginSession} from "../../../../lib/auth";
import {getLanguageFile} from "../../../../lib/getLanguageFile";
import Translation from "../../../components/Translation";
import Loading from "../../../components/loading";
import {useUser} from "../../../../lib/hooks";

export async function getServerSideProps({req, locale}) {

    try {
        const session = await getLoginSession(req);
        if (!session) {
            return {
                redirect: {destination: '/login/', permanent: false,},
            };
        }
        const surveys = await prisma.survey.findMany({
            where: {
                authorId: session.id,
            },
            orderBy: [
                {
                    createdAt: 'desc',
                },
            ],
            include: {
                formanswers: {
                    include: {
                        answers: true,
                    }
                },
                questions: true,
                discriminantvariables: {
                    include: {
                        discriminantitems: true,
                    }
                },
            }
        });
        const serializedSurveys = surveys.map(survey => {
            const serializedFormAnswers = survey.formanswers.map((formanswer) => ({
                ...formanswer,
                createdAt: formanswer.createdAt.toISOString(),
            }))
            return {
                ...survey,
                createdAt: survey.createdAt.toISOString(),
                formanswers: serializedFormAnswers,
            }
        });

        const dictionary = getLanguageFile(locale);

        return {
            props: {surveys: serializedSurveys, dictionary: dictionary, locale:locale}
        };
    } catch (error) {
        console.error(error);
    }

}

export default function MySurveys({surveys, dictionary, locale}) {
    const [searchQuery, setSearchQuery] = useState('');
    const [showArchives, setShowArchives] = useState(false);
    const {user, isLoading} = useUser({redirectTo: '/login'});

    // Filter archived and not archived surveys
    let notArchivedSurveys = [];
    let archivedSurveys = [];
    surveys.forEach((survey) => {
            if (!survey.isArchived) {
                notArchivedSurveys.push(survey);
            } else {
                archivedSurveys.push(survey);
            }
    });

    if(isLoading){
        return (<Loading dictionary={dictionary} locale={locale}/>);
    } else if (!isLoading && !user) {
        return (<Loading dictionary={dictionary} locale={locale}/>);
    }

    //Filters the array (here surveys) given based on search
    const searchFilter = (array) => {
        return array.filter(
            (survey) => survey.label.toLowerCase().includes(searchQuery.toLowerCase())
        )
    }
    const userSurveysFiltered = searchFilter(notArchivedSurveys);

    //On searchbar change filter
    const handleSearchBarChange = (e) => {
        setSearchQuery(e.target.value);
    }

    const handleShowArchives = (e, show) => {
        e.preventDefault();

        setShowArchives(!show);
    }

    return (
        <Layout dictionary={dictionary} locale={locale}>
            <Head>
                <title>{dictionary.mysurveys_header_title}</title>
            </Head>
            <h1>
                <Translation dictionary={dictionary} trKey="mysurveys_header1_title"/>
            </h1>
            <div>
                <div className={styles.surveyContainer}>
                    <input onChange={handleSearchBarChange} name="dimension" id="dimension-select" type="search"
                           placeholder={dictionary.mysurveys_searchbar_placeholder} className={styles.search}/>
                </div>
                <SurveyActionPanel locale={locale} dictionary={dictionary} surveys={userSurveysFiltered} isArchived={false}/>
                <div>
                    <button className={styles.archiveButton} onClick={(e) => handleShowArchives(e, showArchives)}>
                        <div>
                            <FontAwesomeIcon className={styles.archiveButtonIcon} icon={faArchive}/>
                            <Translation dictionary={dictionary} trKey="mysurveys_button_archive"/>
                        </div>
                        <div className={styles.openCloseIcon}>
                            {showArchives ? <FontAwesomeIcon icon={faChevronUp}/> : <FontAwesomeIcon icon={faChevronDown}/>}
                        </div>
                    </button>
                </div>
                {showArchives && <SurveyActionPanel dictionary={dictionary} surveys={archivedSurveys} isArchived={true}/>}
            </div>
            <ReturnButton dictionary={dictionary} href="/dashboard" title={dictionary.mysurveys_button_return}/>
        </Layout>
    )
}