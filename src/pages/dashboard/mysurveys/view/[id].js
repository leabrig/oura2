import Layout from "../../../../components/Layout";
import Link from "next/link";
import styles from '../../../../styles/viewsurvey.module.css';
import Head from "next/head";
import {useQRCode} from "next-qrcode";
import prisma from "../../../../../lib/prisma";
import ReturnButton from "../../../../components/ReturnButton";
import {useEffect, useState} from "react";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {
    faArchive,
    faBarcode, faBoxesPacking,
    faChartPie,
    faCircleInfo,
    faClone,
    faDownload,
    faTrashCan
} from "@fortawesome/free-solid-svg-icons";
import {averageQuestions, sumVariables} from "../../../../../lib/viewsurvey-math";
import BlueButton from "../../../../components/BlueButton";
import {downloadCSV} from "../../../../../lib/download-csv";
import {useRouter} from "next/router";
import {getLoginSession} from "../../../../../lib/auth";
import {handleSurveyArchival} from "../../../../../lib/handle-archival";
import {getLanguageFile} from "../../../../../lib/getLanguageFile";
import Translation from "../../../../components/Translation";
import {getLanguageLabel} from "../../../../../lib/getLanguageLabel";
import Loading from "../../../../components/loading";
import {useUser} from "../../../../../lib/hooks";

export async function getServerSideProps(context) {
    const {req} = context;
    try {
        const session = await getLoginSession(req);
        if (!session) {
            return {
                redirect: {destination: '/login/', permanent: false,},
            };
        }
    } catch (error) {
        console.error(error);
    }

    const survey = await prisma.survey.findUnique({
        where: {
            id: context.query.id,
        },
        include: {
            discriminantvariables: {
                include: {
                    discriminantitems: true,
                },
            },
            questions: true,
            formanswers: {
                include: {
                    answers: true,
                },
            },
        },
    });

    const serializedFormAnswers = survey.formanswers.map((formanswer) => ({
        ...formanswer,
        createdAt: formanswer.createdAt.toISOString(),
    }))

    const serializedSurvey = {
        ...survey,
        createdAt: survey.createdAt.toISOString(),
        formanswers: serializedFormAnswers,
    };
    const locale = context.locale;
    const dictionary = getLanguageFile(locale);

    return { props: {survey: serializedSurvey, dictionary: dictionary, locale:locale }};
}

export default function View({survey, dictionary, locale}) {
    const {Image} = useQRCode();
    const [errorMsg, setErrorMsg] = useState('');
    const [isInfoVisible, setIsInfoVisible] = useState(false);
    const router = useRouter();
    const isArchived = survey.isArchived;
    const langLabel = getLanguageLabel(locale);
    const {user, isLoading} = useUser({redirectTo: '/login'});

    //To avoid hydration error when client doesn't match server side.
    const [isWindowDefined, setWindowDefined] = useState(false);

    // useEffect only runs on client side, which ensures that we have window var.
    useEffect(() => {
        if (typeof window !== 'undefined') {
            setWindowDefined(true);
        }
    }, []);

    if(isLoading){
        return (<Loading dictionary={dictionary} locale={locale}/>);
    } else if (!isLoading && !user) {
        return (<Loading dictionary={dictionary} locale={locale}/>);
    }

    let createdAt = new Date(survey.createdAt);
    let date = createdAt.toLocaleDateString(locale);

    let count = 0;
    survey.formanswers.forEach(() => {
        count++;
    });

    let isDiscriminantVariables = true; //Are there any DV ?
    if (survey.discriminantvariables.length === 0) {
        isDiscriminantVariables = false;
    }

    //Calculations for showing the averages.
    let questionAverages = [];
    let variableSums = [];

    if (survey.formanswers !== null) {
        questionAverages = averageQuestions(survey);
        variableSums = sumVariables(survey);
    }

    async function handleDelete(e, surveyId) {
        e.preventDefault();
        const confirmDelete = confirm(dictionary.viewSurvey_confirm_delete);
        if (!confirmDelete) return;

        if (errorMsg) setErrorMsg('');

        try {
            const res = await fetch(`/api/survey?id=${surveyId}`, {
                method: 'DELETE',
                headers: {'Content-Type': 'application/json'},
            })
            if (res.status === 200) {
                await router.push("/dashboard/mysurveys/");
            } else {
                throw new Error(await res.text());
            }
        } catch (error) {
            console.error('An unexpected error occurred:', error);
            setErrorMsg(error.message);
        }
    }

    async function handleCSVDownload(e) {
        e.preventDefault();
        downloadCSV(survey, locale);
    }

    const handleInfoClick = (e) => {
        e.preventDefault();
       setIsInfoVisible(oldValue => !oldValue);
    }

    async function handleArchive (e, surveyId) {
        e.preventDefault();

        setErrorMsg('');

        try {
            await handleSurveyArchival(surveyId, !isArchived);
        } catch (error) {
            console.error('An unexpected error occurred: ', error);
            setErrorMsg(error.message);
        }
    }

    return (
        <Layout dictionary={dictionary} locale={locale}>
            <Head>
                <title>{`OURA² - ${survey.label}`}</title>
            </Head>
            <div className={styles.titleBannerContainer}>
                <div className={styles.surveyInfoContainer}>
                    <h2 className={styles.surveyTitle}>
                        {survey.label}
                    </h2>
                    {isArchived &&
                        <div className={styles.archivedInfo}>
                            <Translation dictionary={dictionary} trKey="viewSurvey_info_archived"/>
                        </div>
                    }
                    <div className={styles.dateAndCountContainer}>
                        <div className={styles.countDisplay}>
                            {count <= 1 ?
                                <Translation dictionary={dictionary} trKey="viewSurvey_countDisplay_single" params={{answersCount: count}}/>
                                :
                                <Translation dictionary={dictionary} trKey="viewSurvey_countDisplay_multiple" params={{answersCount: count}}/>
                            }
                        </div>
                        <div className={styles.dateDisplay}>
                            <Translation dictionary={dictionary} trKey="viewSurvey_dateDisplay_createdAt" params={{date: date}}/>
                        </div>
                    </div>
                    {isWindowDefined && (
                        <Link href={`/answer/${survey.uniqueStringForm}`} className={styles.surveyLink}>
                            {window.location.origin}/answer/{survey.uniqueStringForm}
                        </Link>
                    )}
                    <div className={styles.linksContainer}>
                        {isArchived ?
                            <div className={styles.linkIconContainer}>
                                <div className={styles.analysePlaceholder} title={dictionary.viewSurvey_button_analysePlaceholder}>
                                    <FontAwesomeIcon icon={faChartPie}/>
                                </div>
                            </div>
                            :
                            <div className={styles.linkIconContainer}>
                                <BlueButton isLink={true} href={`/dashboard/statistics?selectedSurveyId=${survey.id}`}
                                            title={dictionary.viewSurvey_button_analyse}>
                                    <FontAwesomeIcon icon={faChartPie}/>
                                </BlueButton>
                            </div>
                        }

                        <div className={styles.linkIconContainer}>
                            <BlueButton isLink={true} href={`/dashboard/createsurvey?selectedSurveyId=${survey.id}`}
                                        title={dictionary.viewSurvey_button_duplicate}>
                                <FontAwesomeIcon icon={faClone}/>
                            </BlueButton>
                        </div>
                        <div className={styles.linkIconContainer}>
                            <BlueButton isLink={false} onClick={(e) => handleCSVDownload(e)}
                                        title={dictionary.viewSurvey_button_download}>
                                <FontAwesomeIcon icon={faDownload}/>
                            </BlueButton>
                        </div>
                        <div className={styles.linkIconContainer}>
                            <BlueButton isLink={false} onClick={(e) => handleArchive(e, survey.id)}
                                        title={isArchived ? dictionary.viewSurvey_button_unarchive : dictionary.viewSurvey_button_archive}>
                                {isArchived ? <FontAwesomeIcon icon={faBoxesPacking}/> :
                                    <FontAwesomeIcon icon={faArchive}/>}
                            </BlueButton>
                        </div>
                        <div className={styles.linkIconContainer}>
                            <BlueButton isLink={false} onClick={(e) => handleDelete(e, survey.id)}
                                        title={dictionary.viewSurvey_button_delete}>
                                <FontAwesomeIcon icon={faTrashCan}/>
                            </BlueButton>
                        </div>
                    </div>
                </div>
                <div className={styles.barCodeContainer}>
                    <div>
                        {isWindowDefined && (
                            <Image
                                text={`${window.location.origin}/answer/${survey.uniqueStringForm}`}
                                options={{
                                    level: 'medium',
                                    quality: 0.3,
                                    margin: 2,
                                    scale: 4,
                                    width: 120,
                                    color: {
                                        dark: '#000000',
                                        light: '#f0f5f5',
                                    },
                                }}
                            />
                        )}
                    </div>
                    <div className={styles.codeText}>
                        <FontAwesomeIcon icon={faBarcode} className={styles.barCodeIcon}/>
                        {survey.uniqueStringForm}
                    </div>
                </div>
            </div>

            {/*Display of averages of the answers for each question.*/}
            <div className={styles.smallTitleInfoContainer}>
                <h3>
                    <Translation dictionary={dictionary} trKey="viewSurvey_header3_questions"/>
                </h3>
                <button type="button" className={styles.infoIcon} onClick={e => handleInfoClick(e)}>
                    <FontAwesomeIcon icon={faCircleInfo}/>
                    <div
                        className={isInfoVisible ? `${styles.scaleContainer} ${styles.scaleContainerIsVisible}` : `${styles.scaleContainer}`}>
                        <div className={styles.scaleInnerContainer}>
                            <FontAwesomeIcon icon={faCircleInfo} className={styles.iconInInfo}/>
                            <div>
                                <div>
                                    <Translation dictionary={dictionary} trKey="viewSurvey_scaleInfo_disagree"/>
                                </div>
                                <div>
                                    <Translation dictionary={dictionary} trKey="viewSurvey_scaleInfo_agree"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </button>
                <h3>
                    <Translation dictionary={dictionary} trKey="viewSurvey_header3_average"/>
                </h3>
            </div>
            <div className={styles.questionsContainer}>
                {survey.questions.map((question, index) => {
                    const isFirstQuestionElement = index === 0;
                    const questionContainer = isFirstQuestionElement ? styles.firstQuestionContainer : styles.questionContainer;
                    return (
                        <div className={questionContainer} key={index}>
                            <span>
                                {index + 1}. {question[langLabel]}
                            </span>
                            <div className={styles.answerAverageContainer}>
                                <span>
                                    {questionAverages.map(average => {
                                        if (average.questionId === question.id && !isNaN(average.average)) {
                                            return (
                                                <span key={average.questionId}>
                                                    {average.average}
                                                </span>
                                            )
                                        } else if (average.questionId === question.id && isNaN(average.average)) {
                                            return (
                                                0
                                            )
                                        }
                                    })
                                    }
                                    </span>
                            </div>
                        </div>
                    );
                })}
            </div>

            {/*Display of the sum of answers for each discriminant variables.*/}
            {isDiscriminantVariables ? (
                <div>
                    <div className={styles.smallTitleInfoContainer}>
                        <h3>
                            <Translation dictionary={dictionary} trKey="viewSurvey_header3_variables"/>
                        </h3>
                        <h3 className={styles.secondTitle}>
                            <Translation dictionary={dictionary} trKey="viewSurvey_header3_total"/>
                        </h3>
                    </div>
                    <div className={styles.questionsContainer}>
                        {survey.discriminantvariables.map((variable, index) => {
                            const isFirstQuestionElement = index === 0;
                            const questionContainer = isFirstQuestionElement ? styles.firstQuestionContainer : styles.questionContainer;

                            return (
                                <div className={questionContainer} key={index}>
                                    <span>
                                        {index + 1}. {variable.label}
                                    </span>
                                    {variable.discriminantitems.map((item) => {
                                        let totalSum = 0;

                                        variableSums.forEach((sum) => {
                                            if (sum.discriminantvariableId === variable.id) {
                                                sum.choiceCounts.forEach((choiceCount) => {
                                                    if (choiceCount.choice === item.id) {
                                                        totalSum += choiceCount.count;
                                                    }
                                                });
                                            }
                                        });

                                        return (
                                            <div className={styles.variableItemsContainer} key={item.id}>
                                                <span>
                                                    {item.label}
                                                </span>
                                                <div className={styles.answerAverageContainer}>
                                                    <span>
                                                        {totalSum}
                                                    </span>
                                                </div>
                                            </div>
                                        );
                                    })}
                                </div>
                            );
                        })}
                    </div>
                </div>
            ) : (<div/>)
            }
            <ReturnButton dictionary={dictionary} href="/dashboard/mysurveys" title={dictionary.viewSurvey_button_return}/>
        </Layout>
    )
}