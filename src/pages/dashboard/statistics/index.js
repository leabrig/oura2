import Layout from "../../../components/Layout";
import {Bar, Line} from "react-chartjs-2";
import styles from "../../../styles/statistics.module.css";
import prisma from "../../../../lib/prisma";
import {useEffect, useState} from "react";
import {
    Chart as ChartJS,
    CategoryScale,
    LinearScale,
    Tooltip,
    PointElement,
    BarElement,
    LineElement,
    Title,
    Legend,
} from "chart.js";
import ChartDataLabels from 'chartjs-plugin-datalabels';
import Head from "next/head";
import {useRouter} from "next/router";
import ReturnButton from "../../../components/ReturnButton";
import FlashMessage from "../../../components/FlashMessage";
import BlueButton from "../../../components/BlueButton";
import {
    areArraysEqual,
    calcDimensionsAverages, getCommonDimensions, getDimensionsForFiltering,
} from "../../../../lib/statistics-math";
import {getLoginSession} from "../../../../lib/auth";
import {getLanguageFile} from "../../../../lib/getLanguageFile";
import Translation from "../../../components/Translation";
import {getLanguageLabel} from "../../../../lib/getLanguageLabel";
import {useUser} from "../../../../lib/hooks";
import Loading from "../../../components/loading";

ChartJS.register(Legend, CategoryScale, LinearScale, PointElement, Tooltip, BarElement, LineElement, ChartDataLabels, Title,);

export async function getServerSideProps({req, locale}) {

    try {
        const session = await getLoginSession(req);
        if(!session) {
            return {
                redirect: {destination: '/login/', permanent: false,},
            };
        }
        const surveys = await prisma.survey.findMany({
            where: {
                authorId: session.id,
            },
            orderBy: [
                {
                    createdAt: 'desc',
                },
            ],
            include: {
                formanswers: {
                    include: {
                        answers: true,
                    }
                },
                questions: {
                    include: {
                        dimension: true,
                    },
                },
                discriminantvariables: {
                    include: {
                        discriminantitems: true,
                    }
                },
            }
        });
        const serializedSurveys = surveys.map(survey => {
            const serializedFormAnswers = survey.formanswers.map((formanswer) => ({
                ...formanswer,
                createdAt: formanswer.createdAt.toISOString(),
            }))
            return {
                ...survey,
                createdAt: survey.createdAt.toISOString(),
                formanswers: serializedFormAnswers,
            }
        });

        const dictionary = getLanguageFile(locale);

        return {
            props: {surveys: serializedSurveys, dictionary: dictionary, locale:locale}
        };
    } catch (error) {
        console.error(error);
    }

}

export default function Statistics({surveys, dictionary, locale}) {
    const {user, isLoading} = useUser({redirectTo: '/login'});
    const router = useRouter();
    const [isFirstLoad, setIsFirstLoad] = useState(true);
    const [searchQuery, setSearchQuery] = useState('');
    const langLabel = getLanguageLabel(locale);

    const [errorMsg, setErrorMsg] = useState('');
    const [infoMsg, setInfoMsg] = useState('');
    const [successMsg, setSuccessMsg] = useState('');

    const [changesMade, setChangesMade] = useState(true);
    const [surveysFiltered, setSurveysFiltered] = useState(false);

    const [selectedSurveysIds, setSelectedSurveysIds] = useState([]);
    const [prevSelectedSurveysIds, setPrevSelectedSurveysIds] = useState([]);

    const [selectedVariablesIds, setSelectedVariablesIds] = useState([]);
    const [prevSelectedVariablesIds, setPrevSelectedVariablesIds] = useState([]);

    const [selectedDimensionsIds, setSelectedDimensionsIds] = useState([]);
    const [prevSelectedDimensionsIds, setPrevSelectedDimensionsIds] = useState([]);

    const [dimensions, setDimensions] = useState([]); //Stores the whole object
    const [discriminantVariables, setDiscriminantVariables] = useState([]); //Stores the whole object

    const [renderBarChart, setRenderBarChart] = useState(true);
    const [chartLabels, setChartLabels] = useState([]);
    const [datasets, setDatasets] = useState([{data: 0}]);
    const [chartOptions, setChartOptions] = useState({
        scales: {
            y: {
                min: 1,
                max: 6,
            },
            x: {
                ticks: {
                    font: {
                        size: 14,
                        weight: 'bold',
                    }
                },
            },
        },
        plugins: {
            title: {
                display: true,
                text: ``,
                font: {
                    size: 18,
                },
            },
            datalabels: {
                color: '#b8b8b8',
                font: {
                    weight: 'bold',
                    size: 15,
                },
                clamp: true,
                anchor: 'center',
                align: 'center',
            },
            legend: {
                display: false,
                labels: {
                    font: {
                        size: 17,
                    },
                },
            },
        },
    });
    const chartColors = ['#00315b', '#004580', '#0060b3', '#007ce6', '#1a95ff', '#4dadff',
        '#4e0075', '#6d2492', '#8a46ad', '#a868c8', '#c58ae4', '#e3acff',
        '#b00000', '#c01f1f', '#d03c3c', '#e05a5a', '#ef7777', '#ff9595',
        '#730024', '#901c41', '#ac375c', '#c85277', '#e36d93', '#ff88ae',
        '#206600', '#3c861a', '#56a432', '#70c24b', '#8be163', '#a5ff7c',
        '#019144', '#20a85f', '#3ebe79', '#5cd393', '#7ae9ac', '#98ffc6',
    ];

    // Check if there have been changes to adapt changesMade constant, to adapt info/error/success message
    useEffect(() => {
        if (!areArraysEqual(selectedSurveysIds, prevSelectedSurveysIds) ||
            !areArraysEqual(selectedDimensionsIds, prevSelectedDimensionsIds) ||
            !areArraysEqual(selectedVariablesIds, prevSelectedVariablesIds)
        ) {
            setChangesMade(true);
        }
        if(surveysFiltered) {
            setChangesMade(false);
        }
    }, [selectedSurveysIds, selectedDimensionsIds, selectedVariablesIds]);

    // Selects surveys of the user that are NOT archived
    let notArchivedSurveys = [];
    surveys.forEach((survey) => {
        if (!survey.isArchived) {
            notArchivedSurveys.push(survey);
        }
    });

    //On surveySelectionChange prepare survey data, labels etc.
    useEffect(() => {
        setInfoMsg('');

        if (notArchivedSurveys.length === 0 && !isFirstLoad) {
            setInfoMsg(dictionary.statistics_infoMsg_noSurvey);
            return;
        }

        setRenderBarChart(selectedSurveysIds.length <= 1); //Set bar chart according to number of selected surveys.
        setSelectedVariablesIds([]); //Reset / unselect selected DV.
        const chosenSurveys = selectedSurveysIds.length === 1 ? getChosenSurvey() : getChosenSurveys(); //Filter survey(s) that have been selected.

        if (selectedSurveysIds.length === 1) {
            //Extract dimensions of chosen survey
            const chosenDimensions = getDimensionsForFiltering(chosenSurveys);

            setDimensions(chosenDimensions);
            setDiscriminantVariables(Array.from(chosenSurveys.discriminantvariables));
            //Set selection and label for initial dimension.
            setSelectedDimensionsIds(chosenDimensions.map(dimension => dimension.id));
            setChartLabels(chosenDimensions.map(dimension => dimension[langLabel]));

            if (chosenSurveys.formanswers.length >= 1) {
                simpleDisplayUsingOnlyDimensions(chosenSurveys, chosenDimensions);
            } else {
                //No answers available for this survey; only show title and dimension label.
                setDatasets([
                        {
                            label: `${chosenSurveys.label}`,
                        }
                    ]
                );
                setChartOptionsDetailed(chosenSurveys.label, false, 'center');
                setInfoMsg(dictionary.statistics_infoMsg_noAnswers);
            }
        } else if (selectedSurveysIds.length > 1) {
            //Display the discriminant variables common to all surveys.
            const commonDiscriminantVariables = getCommonDiscrVariables(chosenSurveys);
            setDiscriminantVariables(commonDiscriminantVariables);

            //Display the dimensions available common to all surveys.
            const commonChosenDimensions = getCommonDimensions(chosenSurveys);
            setDimensions(commonChosenDimensions);
            let title = '';

            if (commonChosenDimensions[0] === undefined) {
                setSelectedDimensionsIds([]);
                setDatasets([]);
                setChartLabels([]);
                setInfoMsg(dictionary.statistics_infoMsg_noCommonDimension);
            } else {
                title = commonChosenDimensions[0][langLabel];
                doubleDisplayUsingOnlyDimensions(chosenSurveys, commonChosenDimensions, title);
            }
        }
        updatePrevValues();
    }, [selectedSurveysIds]);

    if (isFirstLoad) {
        const surveyId = router.query.selectedSurveyId;
        if (surveyId) {
            setSelectedSurveysIds([surveyId]);
        }
        setIsFirstLoad(false);
    }

    if(isLoading){
        return (<Loading dictionary={dictionary} locale={locale}/>);
    } else if (!isLoading && !user) {
        return (<Loading dictionary={dictionary} locale={locale}/>);
    }

    function updatePrevValues() {
        setPrevSelectedSurveysIds(selectedSurveysIds);
        setPrevSelectedDimensionsIds(selectedDimensionsIds);
        setPrevSelectedVariablesIds(selectedVariablesIds);
        setChangesMade(false);
    }

    //Filter the surveys based on search query
    const searchFilter = (array) => {
        return array.filter(
            (survey) => survey.label.toLowerCase().includes(searchQuery.toLowerCase())
        )
    }
    const ownFilteredSurveys = searchFilter(notArchivedSurveys);

    //On searchbar input, change filter
    const handleSearchBarChange = (e) => {
        setSearchQuery(e.target.value);
    }

    //ChartOptions for different usages: title, the title of the chart. showLegend: when more than 1 survey show datalabels and format it.
    function setChartOptionsDetailed(title, showLegend, dataLabelAnchor){
        setChartOptions((prevOptions) => {
            return {
                ...prevOptions,
                plugins: {
                    ...prevOptions.plugins,
                    title: {
                        ...prevOptions.plugins.title,
                        text: `${title}`,
                    },
                    legend: {
                        ...prevOptions.plugins.legend,
                        display: showLegend,
                    },
                    datalabels: {
                        ...prevOptions.plugins.datalabels,
                        color: dataLabelAnchor === 'center' ? '#b8b8b8' : '#3973ac', //One color for barChart and another for LineChart
                        anchor: dataLabelAnchor,
                        align: dataLabelAnchor === 'center' ? 'center' : 'top', // Align top for end and center for center
                    },
                },
            };
        });
    }

    //Calc: 1 survey and one or two variables selected. More than 1 survey and 2 variables selected Options
    function setLegendsTrueOptions(){
        setChartOptions((prevOptions) => {
            return {
                ...prevOptions,
                plugins: {
                    ...prevOptions.plugins,
                    legend: {
                        ...prevOptions.plugins.legend,
                        display: true,
                    }
                },
            };
        });
    }

    // On change, update the survey selection to keep track which surveys have been selected.
    async function handleSurveySelection(surveyId) {
        await setSurveysFiltered(true);
        await setSelectedSurveysIds((prevSelectedSurveys) => {
            if (prevSelectedSurveys.includes(surveyId)) {
                return prevSelectedSurveys.filter((id) => id !== surveyId);
            } else {
                return [...prevSelectedSurveys, surveyId];
            }
        });
    }

    //On select update the variable selection
    async function handleVariableSelection(variableId) {
        await setSurveysFiltered(false);
        await setInfoMsg(''); //Reset info message.
        setSelectedVariablesIds((prevSelectedVariables) => {
            if (prevSelectedVariables.includes(variableId)) {
                //Remove the already selected variable.
                return prevSelectedVariables.filter((id) => id !== variableId);
            } else if (prevSelectedVariables.length === 2) {
                //Already selected the two max variables, return same list.
                setInfoMsg(dictionary.statistics_infoMsg_selectVarRestriction);
                return prevSelectedVariables;
            } else {
                return [...prevSelectedVariables, variableId];
            }
        });
    }

    //On select update the variable selection
    async function handleDimensionSelection(dimensionId) {
        await setSurveysFiltered(false);
        if (renderBarChart) {
            setSelectedDimensionsIds((prevSelectedDimensions) => {
                if (prevSelectedDimensions.includes(dimensionId)) {
                    return prevSelectedDimensions.filter((id) => id !== dimensionId);
                } else {
                    return [...prevSelectedDimensions, dimensionId];
                }
            });
        } else {
            setSelectedDimensionsIds(dimensionId);
        }
    }

    // ---------- GETTERS -----------

    // Get the dimensions for the calculation
    function getDimensionsForCalc (survey) {
        const chosenDimensions = [];
        const usedDimensionsIds = [];
        survey.questions.forEach((question) => {
            const dimensionId = question.dimension.id;
            if (!usedDimensionsIds.includes(dimensionId) && selectedDimensionsIds.includes(dimensionId)) {
                usedDimensionsIds.push(dimensionId);
                chosenDimensions.push(question.dimension);
            }
        });
        return chosenDimensions;
    }

    function getCommonDiscrVariables(surveys) {
        if (!surveys || surveys.length === 0) {
            return [];
        }

        const variableFrequency = new Map();

        surveys.forEach((survey) => {
            survey.discriminantvariables.forEach((variable) => {
                const variableId = variable.id;
                if (variableFrequency.has(variableId)) {
                    variableFrequency.set(variableId, variableFrequency.get(variableId) + 1);
                } else {
                    variableFrequency.set(variableId, 1);
                }
            });
        });

        const commonVariables = [];

        for (const [variableId, frequency] of variableFrequency.entries()) {
            if (frequency === surveys.length) {
                surveys[0].discriminantvariables.forEach((variable) => {
                    if (variable.id === variableId) {
                        commonVariables.push(variable);
                    }
                });
            }
        }

        return commonVariables;
    }

    function getChosenSurvey() {
        return notArchivedSurveys.find((survey) => survey.id === selectedSurveysIds[0]);
    }

    function getChosenSurveys() {
        return notArchivedSurveys.filter((survey) => selectedSurveysIds.includes(survey.id)).sort((a, b) => new Date(a.createdAt) - new Date(b.createdAt));
    }

    function getChosenDiscrVariables(survey) {
        return survey.discriminantvariables.filter(variable => selectedVariablesIds.includes(variable.id))
    }

    // -------- DISPLAYS ----------

    function simpleDisplayUsingOnlyDimensions(survey, dimensions) {
        const dimensionAverages = calcDimensionsAverages(dimensions, survey, null, null, null, null);

        //Set the new states
        setChartLabels(dimensions.map(dimension => dimension[langLabel]));
        setDatasets([
            {
                label: `${survey.label}`,
                data: dimensionAverages,
                backgroundColor: chartColors[0],
            },
        ]);
        setChartOptionsDetailed(survey.label, false, 'center');
    }

    function doubleDisplayUsingOnlyDimensions(surveys, dimensions, title) {
        setSelectedDimensionsIds(dimensions[0].id);
        const firstDimension = [dimensions[0]];
        const surveysDimensionAverages = [];

        // We first push a null value so that we have space on left and right of points
        surveysDimensionAverages.push(null);
        surveys.forEach((survey) => {
            if (survey.formanswers.length >= 1) {
                const currSurveyDimensionAverage = calcDimensionsAverages(firstDimension, survey, null, null, null, null);
                surveysDimensionAverages.push(currSurveyDimensionAverage);
            } else {
                surveysDimensionAverages.push(null);
            }
        });

        // At the end we push again for space on the right.
        surveysDimensionAverages.push(null);

        setChartOptionsDetailed(title, false, 'end');

        const labels = surveys.map(survey => survey.label);
        setChartLabels([null, ...labels, null]);
        setDatasets([
            {
                label: `${title}`,
                data: surveysDimensionAverages,
                backgroundColor: `${chartColors[0]}`,
                borderColor: `${chartColors[0]}`,
                pointRadius: 5,
                pointHoverRadius: 6,
            }
        ]);
    }

    // ------------ Handle calculation button ------------------

    //On Calc click, calculate the averages of the chosenSurvey(s) by dimension and by DV.
    async function handleSurveyCalc() {
        setErrorMsg('');
        setSuccessMsg('');
        await setSurveysFiltered(false);

        // Error: if try to calc but not surveys selected
        if (selectedSurveysIds.length <= 0) {
            setErrorMsg(dictionary.statistics_errorMsg_chooseSurvey);
            return;
        }

        // Success: no changes, calc has already been made
        if (!changesMade) {
            setSuccessMsg(dictionary.statistics_successMsg_alreadyCalc);
            return;
        }

        // Set bar chart according to number of selected surveys
        setRenderBarChart(selectedSurveysIds.length <= 1);

        // Filter survey(s) that have been selected
        const chosenSurveys = selectedSurveysIds.length === 1 ? getChosenSurvey() : getChosenSurveys();

        // SimpleDisplay of only one survey
        if (selectedSurveysIds.length === 1) {
            const chosenDimensions = getDimensionsForCalc(chosenSurveys);
            const chosenVariables = getChosenDiscrVariables(chosenSurveys);

            //If no answer, no calc.
            if (chosenSurveys.formanswers.length >= 1) {

                //If no DV selected, only need the dimensionAverages
                if (chosenVariables.length <= 0) {
                    simpleDisplayUsingOnlyDimensions(chosenSurveys, chosenDimensions);
                } else if (chosenVariables.length === 1) {

                    //When discriminant variable selected calculate the average for it.
                    const datasets = [];
                    chosenVariables[0].discriminantitems.map((item, index) => {
                        const dimensionsAveragesByItem = calcDimensionsAverages(chosenDimensions, chosenSurveys, chosenVariables[0], item, null, null);
                        datasets.push(
                            {
                                label: item.label,
                                data: dimensionsAveragesByItem,
                                backgroundColor: chartColors[index],
                            },
                        );
                    });

                    await setLegendsTrueOptions();
                    setChartLabels(chosenDimensions.map(dimension => dimension[langLabel]));
                    setDatasets(datasets);
                } else if (chosenVariables.length === 2) {
                    const datasets = [];
                    const firstVar = chosenVariables[0];
                    const secVar = chosenVariables[1];

                    let colorCounter = 0;
                    firstVar.discriminantitems.map((itemFirstVar) => {
                        secVar.discriminantitems.map((itemSecVar) => {
                            const items = [];
                            items.push(itemFirstVar);
                            items.push(itemSecVar);
                            const dimensionsAveragesByItems = calcDimensionsAverages(chosenDimensions, chosenSurveys, null, null, chosenVariables, items);
                            datasets.push({
                                label: `${itemFirstVar.label} - ${itemSecVar.label}`,
                                data: dimensionsAveragesByItems,
                                backgroundColor: chartColors[colorCounter],
                            })
                            colorCounter++;
                        })
                    })

                    await setLegendsTrueOptions();
                    setChartLabels(chosenDimensions.map(dimension => dimension[langLabel]));
                    setDatasets(datasets);
                }
            }

        } else if (selectedSurveysIds.length > 1) {

            // Because the dimensions and variables must be shared by both surveys to be displayed we can just pass one to get the correct ones.
            const chosenDimensions = getDimensionsForCalc(chosenSurveys[0]);
            const chosenVariables = getChosenDiscrVariables(chosenSurveys[0]);

            if (chosenDimensions[0] !== undefined) {
                let title = chosenDimensions[0][langLabel];

                if (chosenVariables.length <= 0) {
                    doubleDisplayUsingOnlyDimensions(chosenSurveys, chosenDimensions, title);
                } else if (chosenVariables.length === 1) {

                    //More than one survey selected, one variable selected.
                    const surveyDatasets = [];
                    chosenVariables[0].discriminantitems.map((item, index) => {
                        const surveysDimensionsAveragesByItem = [];

                        // We push a first null average and a last one to achieve that the surveys are displayed in the middle of the screen.
                        surveysDimensionsAveragesByItem.push(null);
                        chosenSurveys.forEach((survey) => {
                            if (survey.formanswers.length >= 1) {
                                const currSurveyDimensionsAveragesByItem = calcDimensionsAverages(chosenDimensions, survey, chosenVariables[0], item, null, null);
                                surveysDimensionsAveragesByItem.push(currSurveyDimensionsAveragesByItem);
                            } else {
                                surveysDimensionsAveragesByItem.push(null);
                            }
                        })
                        surveysDimensionsAveragesByItem.push(null);

                        surveyDatasets.push(
                            {
                                label: item.label,
                                data: surveysDimensionsAveragesByItem,
                                backgroundColor: chartColors[index],
                                borderColor: chartColors[index],
                                pointRadius: 5,
                                pointHoverRadius: 6,
                            },
                        );
                    });

                    const labels = chosenSurveys.map(survey => survey.label);
                    setChartLabels([null, ...labels, null]);
                    setDatasets(surveyDatasets);
                    setChartOptionsDetailed(title, true, 'end');
                } else if (chosenVariables.length === 2) {
                    const surveyDatasets = [];
                    const firstVar = chosenVariables[0];
                    const secVar = chosenVariables[1];

                    let colorCounter = 0;
                    firstVar.discriminantitems.map((itemFirstVar) => {
                        secVar.discriminantitems.map((itemSecVar) => {
                            const items = [];
                            items.push(itemFirstVar);
                            items.push(itemSecVar);
                            const surveysDimensionsAveragesByItem = [];
                            surveysDimensionsAveragesByItem.push(null);
                            chosenSurveys.forEach((survey) => {
                                if (survey.formanswers.length >= 1) {
                                    const currSurveyDimensionsAveragesByItem = calcDimensionsAverages(chosenDimensions, survey, null, null, chosenVariables, items);
                                    surveysDimensionsAveragesByItem.push(currSurveyDimensionsAveragesByItem);
                                } else {
                                    surveysDimensionsAveragesByItem.push(null);
                                }
                            })
                            surveysDimensionsAveragesByItem.push(null);

                            surveyDatasets.push(
                                {
                                    label: `${itemFirstVar.label} - ${itemSecVar.label}`,
                                    data: surveysDimensionsAveragesByItem,
                                    backgroundColor: chartColors[colorCounter],
                                    borderColor: chartColors[colorCounter],
                                    pointRadius: 5,
                                    pointHoverRadius: 6,
                                },
                            );
                            colorCounter++;
                        })
                    })
                    const labels = chosenSurveys.map(survey => survey.label);
                    setChartLabels([null, ...labels, null]);
                    setLegendsTrueOptions();
                    setDatasets(surveyDatasets);
                }
            }
        }
        updatePrevValues();
    }

    return (
        <Layout dictionary={dictionary} locale={locale}>
            <Head>
                <title>{dictionary.statistics_header_title}</title>
            </Head>
            <h1>
                <Translation dictionary={dictionary} trKey="statistics_header1_title"/>
            </h1>
            <div className={styles.statsArea}>
                <div className={styles.leftArea}>
                    <div className={styles.filterArea}>
                        <div className={styles.areaTitle}>
                            <Translation dictionary={dictionary} trKey="statistics_areaTitle_search"/>
                        </div>
                        <div className={styles.searchBarWrapper}>
                            <input onChange={handleSearchBarChange} name="dimension" id="dimension-select" type="search"
                                   placeholder={dictionary.statistics_search_placeholder} className={styles.search}/>
                        </div>
                    </div>
                    <div className={styles.surveyListArea}>
                        <div className={styles.areaTitle}>
                            <Translation dictionary={dictionary} trKey="statistics_areaTitle_surveys"/>
                        </div>
                        {ownFilteredSurveys.map(survey => {
                            let count = survey.formanswers ? survey.formanswers.length : 0;
                            let createdAt = new Date(survey.createdAt);
                            let date = createdAt.toLocaleDateString(locale);
                            const isChecked = selectedSurveysIds.includes(survey.id);
                            return (
                                <div className={styles.surveyListContainer} key={survey.id}>
                                    <input
                                        type="checkbox"
                                        id={survey.id}
                                        key={survey.id}
                                        className={styles.checkBox}
                                        checked={isChecked}
                                        onChange={() => handleSurveySelection(survey.id)}
                                    />
                                    <label htmlFor={survey.id} className={styles.surveyLabel}>
                                        {survey.label}
                                        <div className={styles.dateAndCountContainer}>
                                            {date} -
                                            {count <= 1 ?
                                                <Translation dictionary={dictionary} trKey="statistics_countDisplay_single" params={{answersCount: count}}/>
                                                :
                                                <Translation dictionary={dictionary} trKey="statistics_countDisplay_multiple" params={{answersCount: count}}/>
                                            }
                                        </div>
                                    </label>
                                    <div className={`${styles.dimensionsInfo} tooltip`}>
                                        <div className={styles.dimensionsInfoTitle}>
                                            <Translation dictionary={dictionary} trKey="statistics_tooltip_label"/>
                                        </div>
                                        {getDimensionsForFiltering(survey).map(dimension => {
                                            return <div className={styles.dimensionsInfoDimensionLabel}
                                                        key={dimension.id}> - {dimension[langLabel]}</div>;
                                        })}
                                    </div>
                                </div>
                            )
                        })}
                    </div>

                </div>
                <div className={styles.rightArea}>
                    <div className={styles.plotArea}>
                        <div className={styles.areaTitle}>
                            <Translation dictionary={dictionary} trKey="statistics_areaTitle_graph"/>
                        </div>
                        {renderBarChart ? (
                            <Bar
                                options={chartOptions}
                                data={
                                    {
                                        labels: chartLabels,
                                        datasets: datasets,
                                    }
                                }
                            />
                        ) : (
                            <Line
                                options={chartOptions}
                                data={
                                    {
                                        labels: chartLabels,
                                        datasets: datasets,
                                    }
                                }
                            />
                        )}
                    </div>
                    <div className={styles.selectionArea}>
                        <div className={styles.vIArea}>
                            <div className={styles.areaTitle}>
                                <Translation dictionary={dictionary} trKey="statistics_areaTitle_variables"/>
                            </div>
                            {discriminantVariables.map((variable) => {
                                const isChecked = selectedVariablesIds.includes(variable.id);
                                return (
                                    <div className={styles.surveyListContainer} key={variable.id}>
                                        <input
                                            type="checkbox"
                                            id={variable.id}
                                            key={variable.id}
                                            className={styles.checkBox}
                                            onChange={() => handleVariableSelection(variable.id)}
                                            checked={isChecked}
                                        />
                                        <label htmlFor={variable.id} className={styles.surveyLabel}>
                                            {variable.label}
                                        </label>
                                    </div>
                                )
                            })}
                        </div>
                        <div className={styles.dimensionsArea}>
                            <div className={styles.areaTitle}>
                                <Translation dictionary={dictionary} trKey="statistics_areaTitle_dimensions"/>
                            </div>
                            {dimensions.map((dimension) => {
                                const isChecked = selectedDimensionsIds.includes(dimension.id);
                                const elementType = renderBarChart ? 'checkbox' : 'radio';
                                const elementStyle = renderBarChart ? styles.checkBox : styles.radio;
                                return (
                                    <div className={styles.surveyListContainer} key={dimension.id}>
                                        <input
                                            type={elementType}
                                            id={dimension.id}
                                            key={dimension.id}
                                            className={elementStyle}
                                            onChange={() => handleDimensionSelection(dimension.id)}
                                            checked={isChecked}
                                        />
                                        <label htmlFor={dimension.id} className={styles.surveyLabel}>
                                            {dimension[langLabel]}
                                        </label>
                                    </div>
                                )
                            })}
                        </div>
                    </div>
                    <BlueButton type="button" onClick={() => handleSurveyCalc()} title={dictionary.statistics_buttonTitle_refineStats} label={dictionary.statistics_buttonLabel_refineStats} isLink={false}>
                    </BlueButton>
                </div>
            </div>
            <ReturnButton dictionary={dictionary} href="/dashboard" title={dictionary.statistics_button_return}/>
            {errorMsg && <FlashMessage msg={errorMsg} type='error'/>}
            {infoMsg && <FlashMessage msg={infoMsg} type='info'/>}
            {successMsg && <FlashMessage msg={successMsg} type='success'/>}
        </Layout>
    )
}