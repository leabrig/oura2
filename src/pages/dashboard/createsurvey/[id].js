import Layout from "../../../components/Layout";
import styles from "../../../styles/createsurvey.module.css";
import {useQRCode} from "next-qrcode";
import Link from "next/link";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faCheck} from "@fortawesome/free-solid-svg-icons";
import {useEffect, useState} from "react";
import Loading from "../../../components/loading";
import {useUser} from "../../../../lib/hooks";
import prisma from "../../../../lib/prisma";
import BlueButton from "../../../components/BlueButton";
import Head from "next/head";
import {getLanguageFile} from "../../../../lib/getLanguageFile";

export async function getServerSideProps(context) {
    const survey = await prisma.survey.findUnique({
        where: {
            id: context.query.id,
        },
        include: {
            discriminantvariables: {
                include: {
                    discriminantitems: true,
                },
            },
            questions: true,
            formanswers: {
                include: {
                    answers: true,
                },
            },
        },
    });

    const serializedFormAnswers = survey.formanswers.map((formanswer) => ({
        ...formanswer,
        createdAt: formanswer.createdAt.toISOString(),
    }))

    const serializedSurvey = {
        ...survey,
        createdAt: survey.createdAt.toISOString(),
        formanswers: serializedFormAnswers,
    };
    const locale = context.locale;
    const dictionary = getLanguageFile(locale);

    return { props: {survey: serializedSurvey, dictionary: dictionary, locale:locale} };
}

export default function CreateView({survey, dictionary, locale}) {
    const {user, isLoading} = useUser({redirectTo: '/login'})
    const {Image} = useQRCode();

    //To avoid hydration error when client doesn't match server side.
    const [isWindowDefined, setWindowDefined] = useState(false);

    //useEffect only runs on client side, which ensures that we have window var.
    useEffect(() => {
        if (typeof window !== 'undefined') {
            setWindowDefined(true);
        }
    }, []);

    //If user not returned yet from useUser -> Loading page, else error page!
    if(isLoading){
        return (<Loading dictionary={dictionary} locale={locale}/>);
    } else if (!isLoading && !user) {
        return (<Loading dictionary={dictionary} locale={locale}/>);
    }

    return (
        <Layout dictionary={dictionary} locale={locale}>
            <div>
                <Head>
                    <title>{`OURA² - ${survey.label}`}</title>
                </Head>
                <h2 className={styles.qrViewHeader}>
                    {survey.label}
                </h2>
                <div className={styles.qrViewCode}>
                    {isWindowDefined && (
                        <Image
                            text={`${window.location.origin}/answer/${survey.uniqueStringForm}`}
                            options={{
                                level: 'medium',
                                quality: 0.9,
                                margin: 2,
                                scale: 4,
                                width: window.screen.width / 3,
                                color: {
                                    dark: '#000000',
                                    light: '#f0f5f5',
                                },
                            }}
                        />
                    )}
                    {isWindowDefined && (
                        <Link href={`/answer/${survey.uniqueStringForm}`} className={styles.qrCodeLink}>
                            {window.location.origin}/answer/{survey.uniqueStringForm}
                        </Link>
                    )}
                </div>
                <div className={styles.finishLinkGroup}>
                    <BlueButton isLink={true} href={`/dashboard/mysurveys`} title={dictionary.createsurveyFinal_returnButton_title} label={dictionary.createsurveyFinal_returnButton_label}>
                        <span className={styles.buttonAdvanceIcon}>
                            <FontAwesomeIcon icon={faCheck}/>
                        </span>
                    </BlueButton>
                </div>
            </div>
        </Layout>
    )
}