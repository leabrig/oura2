import Layout from "../../../components/Layout";
import Head from "next/head";
import styles from "../../../styles/createsurvey.module.css";
import prisma from "../../../../lib/prisma";
import {useEffect, useState} from "react";
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {
    faArrowRight,
    faArrowLeft,
    faChevronDown,
    faChevronUp,
    faPlus,
    faTrashCan,
    faXmark,
    faSquare, faArchive, faBoxesPacking,
} from '@fortawesome/free-solid-svg-icons';
import {useRouter} from "next/router";
import {useUser} from "../../../../lib/hooks";
import {v4 as uuid} from 'uuid';
import FlashMessage from "../../../components/FlashMessage";
import ReturnButton from "../../../components/ReturnButton";
import BlueButton from "../../../components/BlueButton";
import DeleteButton from "../../../components/DeleteButton";
import {getLoginSession} from "../../../../lib/auth";
import {handleVariableArchival} from "../../../../lib/handle-archival";
import {getLanguageFile} from "../../../../lib/getLanguageFile";
import Loading from "../../../components/loading";
import Translation from "../../../components/Translation";
import {getLanguageLabel} from "../../../../lib/getLanguageLabel";

export async function getServerSideProps({req, locale}) {
    try {
        const session = await getLoginSession(req);
        if(!session) {
            return {
                redirect: {destination: '/login/', permanent: false,},
            };
        }
        const surveys = await prisma.survey.findMany({
            where: {
                authorId: session.id,
            },
            include: {
                questions: {
                    include: {
                        dimension: true,
                    }
                },
                discriminantvariables: true,
            },
        });
        const serializedSurveys = surveys.map(survey => {
            return {
                ...survey,
                createdAt: survey.createdAt.toISOString(),
            }
        });
        const domains = await prisma.domain.findMany();
        const dimensions = await prisma.dimension.findMany();
        const questions = await prisma.question.findMany();
        const discriminantVariables = await prisma.discriminantVariable.findMany({
            where: {
                authorId: session.id,
            },
            include: {
                discriminantitems: true,
                _count: {
                    select: {surveys: true},
                }
            },
        });
        const dictionary = getLanguageFile(locale);
        return {
            props: {surveys: serializedSurveys, domains: domains, dimensions: dimensions, questions: questions, discriminantVariables: discriminantVariables, dictionary: dictionary, locale:locale},
        };
    } catch (error) {
        console.error(error);
    }
}

export default function CreateSurvey({surveys, domains, dimensions, questions, discriminantVariables, dictionary, locale}) {
    const {user, isLoading} = useUser({redirectTo: '/login'});
    const router = useRouter();
    const [isFirstLoad, setIsFirstLoad] = useState(true);

    const [openDomains, setOpenDomains] = useState({});
    const [openDimensions, setOpenDimensions] = useState({});
    const [checkedDimensions, setCheckedDimensions] = useState([]);
    const [page1IsValid, setPage1IsValid] = useState(false);
    const [discriminantValues, setDiscriminantValues] = useState([]);
    const [checkedVariables, setCheckedVariables] = useState([]);

    const [showArchives, setShowArchives] = useState(false);
    const [errorMsg, setErrorMsg] = useState('');
    const [archivedVariables, setArchivedVariables] = useState([]);
    const [notArchivedVariables, setNotArchivedVariables] = useState([]);
    const langLabel = getLanguageLabel(locale);

    useEffect(() => {
        if (isFirstLoad) {
            const surveyId = router.query.selectedSurveyId;
            if (surveyId) {
                surveys.forEach((survey) => {
                    if (surveyId === survey.id) {
                        const dimensionsIds = [];
                        const domainsIds = [];
                        survey.questions.forEach((question) => {
                            const dimensionId = question.dimension.id;
                            const domainId = question.dimension.domainId;
                            if (!dimensionsIds.includes(dimensionId)) {
                                dimensionsIds.push(dimensionId);
                                if (!domainsIds.includes(domainId)) {
                                    domainsIds.push(domainId);
                                }
                            }
                        });
                        const variableIds = survey.discriminantvariables.map(variable => variable.id);
                        setCheckedDimensions(dimensionsIds);
                        setCheckedVariables(variableIds);
                        setOpenDomains(domainsIds);
                        setOpenDimensions(dimensionsIds);
                        domainsIds.forEach((id) => openFirstLayer(id))
                        dimensionsIds.forEach((id) => openSecondLayer(id))
                    }
                });
            }
            discriminantVariables.map((variable) => {
                if(variable.isArchived) {
                    setArchivedVariables((prevArchivedVariables) => {
                        return [...prevArchivedVariables, variable]
                    })
                } else {
                    setNotArchivedVariables((prevNotArchivedVariables) => {
                        return [...prevNotArchivedVariables, variable]
                    })
                }
            } )
            setIsFirstLoad(false);
        }
    }, [router.query.selectedSurveyId, surveys])

    if(isLoading){
        return (<Loading dictionary={dictionary} locale={locale}/>);
    } else if (!isLoading && !user) {
        return (<Loading dictionary={dictionary} locale={locale}/>);
    }

    async function openFirstLayer(domainId) {
        setOpenDomains((prevState) => ({
            ...prevState,
            [domainId]: !prevState[domainId],
        }));
    }

    async function openSecondLayer(dimensionId) {
        setOpenDimensions(prevState => ({
            ...prevState,
            [dimensionId]: !prevState[dimensionId],
        }));
    }

    async function addDiscriminantValue() {
        setDiscriminantValues((prevValues) => [
            ...prevValues,
            {id: uuid(), values: Array(6).fill('')},
        ]);
    }

    async function handleDiscriminantItemChange(e, valueIndex, itemIndex) {
        const {name, value} = e.target;

        setDiscriminantValues((prevValues) => {
            const updatedValues = [...prevValues];
            const [index] = name.split('-');

            updatedValues[valueIndex].values[itemIndex] = {
                ...updatedValues[valueIndex].values[itemIndex],
                [index]: value,
            };
            return updatedValues;
        });
    }

    const handleDimensionSelection = (dimensionId) => {
        setCheckedDimensions((prevCheckedDimensions) => {
            if (prevCheckedDimensions.includes(dimensionId)) {
                return prevCheckedDimensions.filter((id) => id !== dimensionId);
            } else {
                return [...prevCheckedDimensions, dimensionId]
            }
        });
    }

    const handleVariableSelection = (variableId) => {
        setCheckedVariables((prevCheckedVariables) => {
            if(prevCheckedVariables.includes(variableId)) {
                return prevCheckedVariables.filter((id) => id !== variableId);
            } else {
                return [...prevCheckedVariables, variableId]
            }
        });
    }

    async function handleSubmit(e) {
        e.preventDefault();

        if (errorMsg) setErrorMsg('');

        const selectedDimensions = dimensions.filter(
            (dimension) => checkedDimensions.includes(dimension.id)
        );

        const selectedQuestions = selectedDimensions.reduce(
            (prev, curr) =>
                prev.concat(
                    questions.filter((question) => question.dimensionId === curr.id)
                ),
            []
        );

        const selectedDVariables = discriminantVariables.filter(
            (dVariable) => checkedVariables.includes(dVariable.id)
        );

        const newDiscriminantValues = discriminantValues.map((value) => {
            const discriminantItems = value.values
                .map((item, index) => ({
                    id: `discriminantItem-${value.id}-${index}`,
                    label: e.currentTarget.elements[`discriminantItem-${value.id}-${index}`].value,
                }))
                .filter((item) => item.label.trim() !== '');

            return {
                label: e.currentTarget.elements[`variableLabel-${value.id}`].value,
                items: discriminantItems,
            }
        });

        const MASK = 0x3d;
        const LETTERS = 'abcdefghijklmnopqrstuvwxyz';
        const NUMBERS = '1234567890';
        const charset = `${NUMBERS}${LETTERS}${LETTERS.toUpperCase()}_-`.split('');

        const bytes = new Uint8Array(11);
        crypto.getRandomValues(bytes);

        const uniqueString = bytes.reduce((acc, byte) => `${acc}${charset[byte & MASK]}`, '');

        const newSurvey = {
            authorId: user.id,
            label: e.currentTarget.elements.label.value,
            questions: selectedQuestions.map((question) => ({id: question.id})),
            discriminantValues: newDiscriminantValues,
            uniqueStringForm: uniqueString,
            dVariables: selectedDVariables.map((dVariable) => ({id: dVariable.id})),
        };

        try {
            const res = await fetch('/api/survey', {
                method: 'POST',
                headers: {'Content-Type': 'application/json'},
                body: JSON.stringify(newSurvey),
            })
            if (res.status === 200) {
                const {survey} = await res.json(); //Retrieve survey object from response
                await router.push(`/dashboard/createsurvey/${survey.id}`); //Push the newly create survey page.
            } else {
                throw new Error(await res.text());
            }
        } catch (error) {
            console.error('An unexpected error occurred:', error);
            setErrorMsg(error.message);
        }
    }

    async function handleDelete(e, dVariableId) {
        e.preventDefault();

        let linkedSurveys = 0;
        discriminantVariables.map((variable) => {
            if(variable.id === dVariableId) {
                linkedSurveys = variable._count.surveys;
            }
        });

        let confirmDelete;
        if(linkedSurveys >= 1) {
            confirmDelete = confirm(dictionary.createsurvey_confirm_deleteMultipleLinks.replace(`{linkedSurveys}`, linkedSurveys));
        } else {
            confirmDelete = confirm(dictionary.createsurvey_confirm_deleteNoLink);
        }
        if (!confirmDelete) return;

        if (errorMsg) setErrorMsg('');

        try {
            const res = await fetch(`/api/discriminantvariable?id=${dVariableId}`, {
                method: 'DELETE',
                headers: {'Content-Type': 'application/json'},
            })
            if (res.status === 200) {
                await router.push("/dashboard/createsurvey");
            } else {
                throw new Error(await res.text());
            }
        } catch (error) {
            console.error('An unexpected error occurred:', error);
            setErrorMsg(error.message);
        }
    }

    const handleShowArchives = (e, show) => {
        e.preventDefault();

        setShowArchives(!show);
    }

    async function handleArchive (e, variableId, isArchived) {
        e.preventDefault();

        const toArchiveVariable = discriminantVariables.find(variable => {
            return variable.id === variableId;
        })

        if(isArchived) {
            setArchivedVariables((prevArchivedVariables) => {
                return prevArchivedVariables.filter((variable  => {
                    return variable.id !== variableId;
                })
                )
            })
            setNotArchivedVariables((prevNotArchivedVariables) => {
                return [...prevNotArchivedVariables, toArchiveVariable]
            })
        } else if (!isArchived) {
            setNotArchivedVariables((prevNotArchivedVariables) => {
                return prevNotArchivedVariables.filter((variable) => {
                    return variable.id !== variableId;
                })
            })
            setArchivedVariables((prevArchivedVariables) => {
                return [...prevArchivedVariables, toArchiveVariable]
            })
        }

        setErrorMsg('');

        try {
            await handleVariableArchival(variableId, !isArchived);
        } catch (error) {
            console.error('An unexpected error occurred: ', error);
            setErrorMsg(error.message);
        }
    }

    async function show(shown) {
        await setErrorMsg('');

        if (shown === 'choixDimensions1') {
            setErrorMsg('');
            document.getElementsByClassName(styles.firstPageDimensionChoice)[0].style.display = 'block';
            document.getElementsByClassName(styles.secondPageVariablesChoice)[0].style.display = 'none';
        } else {
            if (!page1IsValid) {
                setErrorMsg(dictionary.createsurvey_error_chooseName);
                return false;
            }
            if (!checkedDimensions.length > 0) {
                setErrorMsg(dictionary.createsurvey_error_chooseDimension);
                return false;
            }
            setErrorMsg('');
            document.getElementsByClassName(styles.firstPageDimensionChoice)[0].style.display = 'none';
            document.getElementsByClassName(styles.secondPageVariablesChoice)[0].style.display = 'block';
        }
        return false;
    }

    return (
        <Layout dictionary={dictionary} locale={locale}>
            <Head>
                <title>{dictionary.createsurvey_header_title}</title>
            </Head>
            <h1>
                <Translation dictionary={dictionary} trKey="createsurvey_header1_title"/>
            </h1>

            <form onSubmit={handleSubmit}>

                <div className={styles.firstPageDimensionChoice}>
                    <h2>
                        <Translation dictionary={dictionary} trKey="createsurvey_header2_labelPage1"/>
                    </h2>
                    <div>
                        <p>
                            <Translation dictionary={dictionary} trKey="createsurvey_info_domains"/>
                        </p>
                        <p>
                            <span className={styles.colorCodeInfo}>
                                <FontAwesomeIcon icon={faSquare} className={styles.domainInfoIcon}/>
                                <Translation dictionary={dictionary} trKey="createsurvey_info_domainName"/>
                            </span>
                            <span className={styles.colorCodeInfo}>
                                <FontAwesomeIcon icon={faSquare} className={styles.dimensionInfoIcon}/>
                                <Translation dictionary={dictionary} trKey="createsurvey_info_dimensionName"/>
                            </span>
                        </p>
                        <p>
                            <Translation dictionary={dictionary} trKey="createsurvey_info_tickDimensions"/>
                        </p>
                    </div>
                    <div className={styles.choiceContainer}>
                            <span className={styles.surveyLabel}>
                                <Translation dictionary={dictionary} trKey="createsurvey_input_surveyName"/>
                            </span>
                            <input placeholder={dictionary.createsurvey_input_placeholder} className={styles.surveyInput} type="text" name="label" maxLength="55"
                                   onInput={(e) => setPage1IsValid(!!e.currentTarget.value)} required/>
                        <div>
                            {domains.map((domain) => {
                                const isDomainOpen = !!openDomains[domain.id];
                                let dimensionsToDomain = [];
                                dimensions.forEach((dimension) => {
                                    if (dimension.domainId === domain.id) {
                                        dimensionsToDomain.push(dimension);
                                    }
                                });
                                return (
                                    <div key={domain.id}>
                                        <button
                                            type="button"
                                            onClick={() => openFirstLayer(domain.id)}
                                            className={styles.firstLayerContainer}
                                        >
                                            <div className={styles.buttonLabelContainer}>
                                                {domain[langLabel]}
                                                {isDomainOpen ? (
                                                    <span className={styles.openCloseIcon}>
                                                <FontAwesomeIcon icon={faChevronUp}/>
                                            </span>
                                                ) : (
                                                    <span className={styles.openCloseIcon}>
                                                <FontAwesomeIcon icon={faChevronDown}/>
                                            </span>
                                                )}
                                            </div>
                                        </button>
                                        {isDomainOpen && dimensionsToDomain.map((dimension) => {
                                            const isDimensionOpen = !!openDimensions[dimension.id];
                                            let questionsToDimension = [];
                                            questions.forEach((questionMap) => {
                                                if (questionMap.dimensionId === dimension.id) {
                                                    questionsToDimension.push(questionMap);
                                                }
                                            });
                                            const isChecked = checkedDimensions.includes(dimension.id);

                                            return (
                                                <div key={dimension.id} id={dimension.id}>
                                                    <button
                                                        type="button"
                                                        className={styles.secondLayerContainer}
                                                        onClick={() => openSecondLayer(dimension.id)}>
                                                        <div className={styles.buttonLabelContainer}>
                                                            <input className={styles.checkBox}
                                                                   type={"checkbox"}
                                                                   key={dimension.id}
                                                                   name={"question"}
                                                                   checked={isChecked}
                                                                   onChange={() => handleDimensionSelection(dimension.id)}
                                                            />
                                                            {dimension[langLabel]}
                                                            {isDimensionOpen ? (
                                                                <span className={styles.openCloseIcon}>
                                                                    <FontAwesomeIcon icon={faChevronUp}/>
                                                                </span>
                                                            ) : (
                                                                <span className={styles.openCloseIcon}>
                                                                    <FontAwesomeIcon icon={faChevronDown}/>
                                                                </span>
                                                            )}
                                                        </div>
                                                    </button>
                                                    {isDimensionOpen && questionsToDimension.map((question) => (
                                                        <div className={styles.thirdLayerContainer} key={question.id}>
                                                            {question[langLabel]}
                                                        </div>
                                                    ))}
                                                </div>
                                            )
                                        })}
                                    </div>
                                );
                            })}
                        </div>
                        <div className={styles.confirmButtonContainer}>
                            <BlueButton type="button" onClick={() => show('questionsSupp2', 'choixDimensions1')} title={dictionary.createsurvey_nextButton_title} label={dictionary.createsurvey_nextButton_label} isLink={false}>
                            <span className={styles.buttonAdvanceIcon}>
                                <FontAwesomeIcon icon={faArrowRight}/>
                            </span>
                            </BlueButton>
                        </div>
                    </div>
                    <ReturnButton dictionary={dictionary} href="/dashboard" title={dictionary.createsurvey_returnDashboard_title}/>
                </div>

                <div className={styles.secondPageVariablesChoice}>
                    <h2>
                        <Translation dictionary={dictionary} trKey="createsurvey_header2_labelPage2"/>
                    </h2>
                    <div>
                        <p>
                            <Translation dictionary={dictionary} trKey="createsurvey_info_variables"/>
                        </p>
                    </div>
                    <div className={styles.choiceContainer}>
                        <p>
                            <Translation dictionary={dictionary} trKey="createsurvey_info_newVariables"/>
                        </p>
                        <BlueButton isLink={false} type="button" onClick={() => addDiscriminantValue()}>
                            <Translation dictionary={dictionary} trKey="createsurvey_button_addVariable"/>
                            <span className={styles.openCloseIcon}>
                                <FontAwesomeIcon icon={faPlus}/>
                            </span>
                        </BlueButton>
                        {discriminantValues.map((value, valueIndex) => (
                            <table className={styles.variablesDisplayContainer} key={"table-" + valueIndex}>
                                <tbody key={"body-" + valueIndex}>
                                <tr className={styles.newVariableLabelRow} key={valueIndex}>
                                    <td className={styles.newVariableLabel} key={"title-" + value.id}>
                                        <Translation dictionary={dictionary} trKey="createsurvey_table_variableName"/>
                                    </td>
                                    <td key={"input-" + value.id}>
                                        <div className={styles.variableDelButtonContainer}>
                                            <input className={styles.variableInput} type="text"
                                                   placeholder={dictionary.createsurvey_input_variablePlaceholder}
                                                   name={`variableLabel-${value.id}`}
                                                   maxLength="55" title={dictionary.createsurvey_input_variableTitle}/>
                                            <div className={styles.deleteButtonContainer}>
                                                <DeleteButton type="button"
                                                              onClick={() => setDiscriminantValues((prevValues) => prevValues.filter((v) => v.id !== value.id))}
                                                              title={dictionary.createsurvey_button_deleteInsert}>
                                                    <FontAwesomeIcon icon={faXmark}/>
                                                </DeleteButton>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                {[...Array(6)].map((_, itemIndex) => (
                                    <tr key={itemIndex} className={styles.itemRows}>
                                        <td className={styles.newItemLabel}
                                            key={"item-title-" + itemIndex}>
                                            <Translation dictionary={dictionary} trKey="createsurvey_table_itemName" params={{itemIndex: itemIndex+1}}/>
                                        </td>
                                        <td key={"item-value-" + itemIndex}>
                                            <input className={styles.valueInput} type="text"
                                                   name={`discriminantItem-${value.id}-${itemIndex}`}
                                                   maxLength="55"
                                                   title={dictionary.createsurvey_input_itemTitle}
                                                   placeholder={dictionary.createsurvey_input_itemPlaceholder}
                                                   onChange={(e) => handleDiscriminantItemChange(e, valueIndex, itemIndex)}
                                            />
                                        </td>
                                    </tr>
                                ))}
                                </tbody>
                            </table>
                        ))}
                        <div>
                            <p>
                                <Translation dictionary={dictionary} trKey="createsurvey_info_orChoose"/>
                            </p>
                        </div>
                        <div>
                            {notArchivedVariables.map(((dVariable, index) => {
                                const isChecked = checkedVariables.includes(dVariable.id);
                                return (
                                    <table className={styles.variablesDisplayContainer} key={"table-" + index}>
                                        <tbody>
                                        <tr>
                                            <td className={styles.existingVariableLabelRow}>
                                                <input className={styles.checkBox}
                                                       type={"checkbox"}
                                                       key={dVariable.id}
                                                       id={`checkbox-${dVariable.id}`}
                                                       name={"dVariable"}
                                                       checked={isChecked}
                                                       onChange={() => handleVariableSelection(dVariable.id)}
                                                />
                                                <label
                                                    htmlFor={`checkbox-${dVariable.id}`}
                                                    className={styles.variableLabel}
                                                >
                                                    {dVariable.label}
                                                </label>
                                                <div className={styles.variablePanelButtonsContainer}>
                                                    <button onClick={(e) => handleArchive(e, dVariable.id, false)}
                                                            title={dictionary.createsurvey_button_archiveVariable}
                                                            className={styles.archiveVariableIcon}>
                                                        <FontAwesomeIcon icon={faArchive}
                                                                         />
                                                    </button>
                                                    <DeleteButton type="button"
                                                                  onClick={(e) => handleDelete(e, dVariable.id)}
                                                                  title={dictionary.createsurvey_button_deleteVariable}>
                                                        <FontAwesomeIcon icon={faTrashCan}/>
                                                    </DeleteButton>
                                                </div>
                                            </td>
                                        </tr>
                                        {dVariable.discriminantitems.map(((item, index) => {
                                            return (
                                                <tr className={styles.itemRows} key={index}>
                                                    <td className={styles.itemElement}>
                                                        {item.label}
                                                    </td>
                                                </tr>
                                            );
                                        }))}
                                        </tbody>
                                    </table>
                                );
                            }))}
                        </div>
                        <div>
                            <button className={styles.archiveButton}
                                    onClick={(e) => handleShowArchives(e, showArchives)}>
                                <div>
                                    <FontAwesomeIcon className={styles.archiveButtonIcon} icon={faArchive}/>
                                    <Translation dictionary={dictionary} trKey="createsurvey_button_archives"/>
                                </div>
                                <div className={styles.openCloseIcon}>
                                    {showArchives ? <FontAwesomeIcon icon={faChevronUp}/> : <FontAwesomeIcon icon={faChevronDown}/>}
                                </div>
                            </button>
                            {showArchives &&
                            <div>
                                {archivedVariables.map(((dVariable, index) => {
                                    return (
                                        <table className={styles.variablesDisplayContainer} key={"table-" + index}>
                                            <tbody>
                                            <tr>
                                                <td className={`${styles.existingVariableLabelRow} ${styles.archivedVariableLabelRow}`}>
                                                    <label
                                                        htmlFor={`checkbox-${dVariable.id}`}
                                                        className={styles.variableLabel}
                                                    >
                                                        {dVariable.label}
                                                    </label>
                                                    <div className={styles.variablePanelButtonsContainer}>
                                                        <button
                                                            onClick={(e) => handleArchive(e, dVariable.id, true)}
                                                            title={dictionary.createsurvey_button_unarchiveVariable}
                                                            className={styles.archiveVariableIcon}
                                                        >
                                                            <FontAwesomeIcon icon={faBoxesPacking}/>
                                                        </button>
                                                        <DeleteButton type="button"
                                                                      onClick={(e) => handleDelete(e, dVariable.id)}
                                                                      title={dictionary.createsurvey_button_deleteVariable}>
                                                            <FontAwesomeIcon icon={faTrashCan}/>
                                                        </DeleteButton>
                                                    </div>
                                                </td>
                                            </tr>
                                            {dVariable.discriminantitems.map(((item, index) => {
                                                return (
                                                    <tr className={styles.itemRows} key={index}>
                                                        <td className={styles.itemElement}>
                                                            {item.label}
                                                        </td>
                                                    </tr>
                                                );
                                            }))}
                                            </tbody>
                                        </table>
                                    );
                                }))}
                            </div>
                            }
                        </div>
                        <div className={styles.buttonGroup}>
                            <div className={styles.returnButtonContainer}>
                                <button className={styles.returnButton} title={dictionary.createsurvey_button_returnToChoiceTitle}
                                        type="button"
                                        onClick={() => show('choixDimensions1', 'questionsSupp2')}>
                                <span className={styles.returnArrow}>
                                    <FontAwesomeIcon icon={faArrowLeft}/>
                                </span>
                                    <span className={styles.returnButtonLabel}>
                                        <Translation dictionary={dictionary} trKey="createsurvey_button_returnToChoiceLabel"/>
                                    </span>
                                </button>
                            </div>
                            <div className={styles.confirmButtonContainer}>
                                <BlueButton type="submit" title={dictionary.createsurvey_button_submitTitle} label={dictionary.createsurvey_button_submitLabel} isLink={false}>
                                <span className={styles.buttonAdvanceIcon}>
                                    <FontAwesomeIcon icon={faArrowRight}/>
                                </span>
                                </BlueButton>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            {errorMsg && <FlashMessage msg={errorMsg} type='error'/>}
        </Layout>
    );
}