import {useUser} from '../../../lib/hooks';
import Layout from '../../components/Layout';
import styles from '../../styles/dashboard.module.css';
import Head from "next/head";
import Link from "next/link";
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faList, faChartPie, faCirclePlus} from '@fortawesome/free-solid-svg-icons';
import Loading from "../../components/loading";
import {getLanguageFile} from "../../../lib/getLanguageFile";
import Translation from "../../components/Translation";
import FlashMessage from "../../components/FlashMessage";
import {useRouter} from "next/router";

export async function getServerSideProps({locale}) {
    const dictionary = getLanguageFile(locale);
    return {props: {dictionary: dictionary, locale:locale}}
}

export default function Dashboard({dictionary, locale}) {
    const {user, isLoading} = useUser({redirectTo: '/login'});
    const router = useRouter();

    //If user not returned yet from useUser -> Loading page, else error page!
    if(isLoading){
        return (<Loading dictionary={dictionary} locale={locale}/>);
    } else if (!isLoading && !user) {
        return (<Loading dictionary={dictionary} locale={locale}/>);
    }

    return (
        <Layout dictionary={dictionary} locale={locale}>
            <Head>
                <title>{dictionary.dashboard_header_title}</title>
            </Head>
            <div className={styles.smallerPageContainer}>
                <h1>
                    <Translation dictionary={dictionary} trKey="dashboard_header1_title"/>
                </h1>
                <div className={styles.menuContainer}>
                    <Link href="/dashboard/mysurveys" title={dictionary.dashboard_linkTitle_mySurveys} className={styles.lightButton}>
                        <Translation dictionary={dictionary} trKey="dashboard_linkLabel_mySurveys"/>
                        <div className={styles.icon}>
                            <FontAwesomeIcon icon={faList}/>
                        </div>
                    </Link>
                    <Link href="/dashboard/statistics" title={dictionary.dashboard_link_analyse} className={styles.lightButton}>
                        <Translation dictionary={dictionary} trKey="dashboard_link_analyse"/>
                        <div className={styles.icon}>
                            <FontAwesomeIcon icon={faChartPie}/>
                        </div>
                    </Link>
                    <Link href="/dashboard/createsurvey" title={dictionary.dashboard_link_create}
                          className={styles.darkButton}>
                        <Translation dictionary={dictionary} trKey="dashboard_link_create"/>
                        <div className={styles.icon}>
                            <FontAwesomeIcon icon={faCirclePlus}/>
                        </div>
                    </Link>
                </div>
            </div>
        </Layout>
    )
}