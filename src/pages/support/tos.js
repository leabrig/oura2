import { getFileData} from "../../../lib/files";
import Layout from "../../components/Layout";
import Head from "next/head";
import styles from '../../styles/support.module.css'
import {getLanguageFile} from "../../../lib/getLanguageFile";

export async function getStaticProps({locale}) {
    const fileData = await getFileData("termsofservice", locale);
    const dictionary = getLanguageFile(locale);
    return {
        props: {
            fileData: fileData,
            dictionary: dictionary,
            locale: locale
        },
    };
}

export default function Tos({ fileData, dictionary, locale }) {
    return (
        <Layout dictionary={dictionary} locale={locale}>
            <Head>
                <title>{fileData.title}</title>
            </Head>
            <article>
                <h1 className={styles.headingXL}>{fileData.title}</h1>
                <div dangerouslySetInnerHTML={{ __html: fileData.contentHtml }} />
            </article>
        </Layout>
    );
}