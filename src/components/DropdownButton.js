import React, { useState } from 'react'
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faEllipsisVertical} from "@fortawesome/free-solid-svg-icons";
import styles from ".//dropdownbutton.module.css";

export default function Dropdown({children, isLastSurvey, dictionary}) {
    const [isOpen, setIsOpen] = useState(false);

    // Open and close dropdown
    const toggle = () => {
        setIsOpen(oldValue => !oldValue);
    }

    let dropMenu = isLastSurvey ? `${styles.dropMenuLast}` : `${styles.dropMenu}`;

    return (
        <>
            <div className={styles.dropButtonContainer}>
                <button
                    className={isOpen ? `${styles.dropButton} ${styles.dropButtonActive}` : `${styles.dropButton}`}
                    onClick={toggle}
                    title={dictionary.DropdownButton_button_title}
                >
                   <FontAwesomeIcon icon={faEllipsisVertical}/>
                </button>
                <div className={isOpen ? `${dropMenu} ${styles.dropMenuIsOpen}` : `${dropMenu}`}>
                    {children}
                </div>
            </div>
            {
                isOpen
                    ?
                    <div
                        className={styles.clickBackground}
                        onClick={toggle}
                    ></div>
                    :
                    <></>
            }
        </>
    )
}
