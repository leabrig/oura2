import styles from './footer.module.css';
import Link from "next/link";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faCreativeCommons, faCreativeCommonsBy, faCreativeCommonsNc, faCreativeCommonsSa} from '@fortawesome/free-brands-svg-icons';
import Translation from "./Translation";

export default function Footer({dictionary}) {

    // TODO: Maybe add a contact information or page ?
    return (
        <footer className={styles.pageFooter}>
            <nav>
                <ul className={styles.linksContainer}>
                    <Link href="/support/about" title={dictionary.footer_aboutLink_title}>
                        <li>
                            <div className={styles.link}>
                                <Translation dictionary={dictionary} trKey="footer_aboutLink_title"/>
                            </div>
                        </li>
                    </Link>
                    <Link href="/support/tos" title={dictionary.footer_tosLink_title}>
                        <li>
                            <div className={styles.link}>
                                <Translation dictionary={dictionary} trKey="footer_tosLink_title"/>
                            </div>
                        </li>
                    </Link>
                </ul>
                <p className={styles.license}> OURA² © 2024 |
                    <span>
                        <FontAwesomeIcon icon={faCreativeCommons} className={styles.licenseIcon}/>
                        <FontAwesomeIcon icon={faCreativeCommonsBy} className={styles.licenseIcon}/>
                        <FontAwesomeIcon icon={faCreativeCommonsNc} className={styles.licenseIcon}/>
                        <FontAwesomeIcon icon={faCreativeCommonsSa} className={styles.licenseIcon}/>
                    </span>
                </p>
            </nav>
        </footer>
    )
}