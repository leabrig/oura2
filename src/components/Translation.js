import translate from "../../lib/translate";

export default function Translation ({dictionary, trKey, params}) {
    return <>{translate(dictionary, trKey, params)}</>
}