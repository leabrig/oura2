import styles from "./blueButton.module.css";
import Link from "next/link";

//is link !!!!
export default function BlueButton ({type, id, title, label, onClick, href, children, isLink}) {

    return (
        <div>
            {isLink ? (<Link id={id} href={href} title={title} className={styles.link}>
                    {label}
                    {children}
                </Link>
            ) : (
                <button id={id} type={type} title={title} className={styles.button} onClick={onClick}>
                    {label}
                    {children}
                </button>
            )}
        </div>
    )
}
