import Layout from "../components/Layout";

export default function Loading({dictionary, locale}) {
    return (
        <div>
            <style jsx>{`
              .loader {
                border: 8px solid #f3f3f3;
                border-top: 8px solid #3973ac;
                border-radius: 50%;
                width: 100px;
                height: 100px;
                animation: spin 2s linear infinite;
                margin: 35% auto;
              }

              @keyframes spin {
                0% {
                  transform: rotate(0deg);
                }
                100% {
                  transform: rotate(360deg);
                }
              }
            `}</style>
            <Layout dictionary={dictionary} locale={locale}>
                <div className="loader"></div>
            </Layout>
        </div>
    );
}