import Link from 'next/link';
import styles from './form.module.css';
import FlashMessage from "./FlashMessage";
import BlueButton from "./BlueButton";
import Translation from "./Translation";

export default function Form({isLogin, errorMessage, onSubmit, forgotPassword, dictionary}) {
    return (
        <form onSubmit={onSubmit}>
            <label>
                <span className={styles.label}>
                    <Translation dictionary={dictionary} trKey="form_label_email"/>
                </span>
                <input className={styles.input} type="email" name="email" required/>
            </label>
            <label>
                <span className={styles.secondLabel}>
                    <Translation dictionary={dictionary} trKey="form_label_password"/>
                </span>
                <input className={styles.input} type="password" name="password" required/>
            </label>
            {!isLogin && (
                <label>
                    <span className={styles.secondLabel}>
                        <Translation dictionary={dictionary} trKey="form_label_repeatPassword"/>
                    </span>
                    <input className={styles.input} type="password" pattern=".{8,}" title={dictionary.form_info_atLeast8} name="rpassword" required/>
                </label>
            )}

            <div>
                {isLogin ? (
                    <>
                        <div className={styles.confirmButtonContainer}>
                            <BlueButton type="submit" label={dictionary.form_button_login} title={dictionary.form_button_login}/>
                        </div>
                        <div className={styles.underLineElement}>
                            <div className={styles.infoText}>
                                <Translation dictionary={dictionary} trKey="form_info_noAccount"/>
                            </div>
                            <Link href="/signup">
                                <div className={styles.linkText}>
                                    <Translation dictionary={dictionary} trKey="form_link_createAccount"/>
                                </div>
                            </Link>
                        </div>
                        <div>
                            <div className={styles.infoText}>
                                <Translation dictionary={dictionary} trKey="form_info_forgotPassword"/>
                            </div>
                            <Link href="/reset-password/">
                                <div className={styles.linkText}>
                                    <Translation dictionary={dictionary} trKey="form_link_resetPassword"/>
                                </div>
                            </Link>
                        </div>
                    </>
                ) : (
                    <>
                        <div className={styles.infoText}>
                            <Translation dictionary={dictionary} trKey="form_info_atLeast8"/>
                        </div>
                        <div className={styles.confirmButtonContainer}>
                            <BlueButton type="submit" label={dictionary.form_button_signup} title={dictionary.form_button_signup}/>
                        </div>
                        <div className={styles.underLineElement}>
                            <div className={styles.infoText}>
                                <Translation dictionary={dictionary} trKey="form_info_alreadyAccount"/>
                            </div>
                            <Link href="/login">
                                <div className={styles.linkText}>
                                    <Translation dictionary={dictionary} trKey="form_link_login"/>
                                </div>
                            </Link>
                        </div>
                        {forgotPassword &&
                            <div>
                                <div className={styles.infoText}>
                                    <Translation dictionary={dictionary} trKey="form_info_forgotPassword"/>
                                </div>
                                <Link href="/reset-password/">
                                    <div className={styles.linkText}>
                                        <Translation dictionary={dictionary} trKey="form_link_resetPassword"/>
                                    </div>
                                </Link>
                            </div>
                        }
                    </>
                )}
            </div>
            {errorMessage && <FlashMessage msg={errorMessage} type='error'/>}
        </form>
    );
}
