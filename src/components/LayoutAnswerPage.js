import styles from "./layoutanswerpage.module.css";

export default function LayoutAnswerPage({children}) {
    return (
        <div className={styles.pageContainer}>
            <main>
                <div className={styles.mainContainer}>{children}</div>
            </main>
        </div>
    );
}