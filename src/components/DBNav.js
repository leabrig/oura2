import styles from './dbnav.module.css';
import Link from "next/link";
import {useRouter} from "next/router";
import Translation from "./Translation";

export default function DBNav({dictionary}) {
    const router = useRouter();

    return (
        <nav>
            <div>
                <Link href="/admindb/questions" title={dictionary.DBNav_link_questions}>
                    <div className={styles.navLink}>
                        <div className={router.pathname === "/admindb/questions" ? `${styles.active}` : ""}>
                            <span>
                                <Translation dictionary={dictionary} trKey="DBNav_link_questions"/></span>
                        </div>
                    </div>
                </Link>
                <Link href="/admindb/dimensions" title={dictionary.DBNav_link_dimensions}>
                    <div className={styles.navLink}>
                        <div className={router.pathname === "/admindb/dimensions" ? `${styles.active}` : ""}>
                            <span>
                                <Translation dictionary={dictionary} trKey="DBNav_link_dimensions"/>
                            </span>
                        </div>
                    </div>
                </Link>
                <Link href="/admindb/domains" title={dictionary.DBNav_link_domains}>
                    <div className={styles.navLink}>
                        <div className={router.pathname === "/admindb/domains" ? `${styles.active}` : ""}>
                            <span>
                                <Translation dictionary={dictionary} trKey="DBNav_link_domains"/>
                            </span>
                        </div>
                    </div>
                </Link>
                <Link href="/admindb/usermanager" title={dictionary.DBNav_link_users}>
                    <div className={styles.navLink}>
                        <div className={router.pathname === "/admindb/usermanager" ? `${styles.active}` : ""}>
                            <span>
                                <Translation dictionary={dictionary} trKey="DBNav_link_users"/>
                            </span>
                        </div>
                    </div>
                </Link>
            </div>
        </nav>
    );
}