import Head from 'next/head';
import Navbar from './Navbar';
import styles from './layout.module.css';
import Footer from "./Footer";

export default function Layout({children, dictionary, locale}) {
    return (
            <div className={styles.pageContainer}>
                <Head>
                    <title>OURA²</title>
                </Head>
                <Navbar dictionary={dictionary} locale={locale}/>
                <main className={styles.mainContainer}>
                        {children}
                </main>
                <Footer dictionary={dictionary}/>
            </div>
    );
}
