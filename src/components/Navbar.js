import Link from 'next/link';
import {useUser} from '../../lib/hooks';
import styles from './navbar.module.css';
import { FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faCircleQuestion, faGaugeHigh, faUser} from '@fortawesome/free-solid-svg-icons';
import Translation from "./Translation";
import {useRouter} from "next/router";
import Image from "next/image";
import logo from "../../public/images/navbar-logo.png";

export default function Navbar({dictionary, locale}) {
    const { user, isAdmin } = useUser()
    const router = useRouter();
    async function handleLanguageSelect(e) {
        e.preventDefault();

        const path = window.location.pathname;
        let url = window.location.pathname;
        if (path.includes('/en')) {
            url = path.replace('/en','');
        } else if (path.includes('/de')) {
            url = path.replace('/de', '');
        } else if (path.includes('/fr')) {
            url = path.replace('/fr','');
        } else if (path.includes('/ar')) {
            url = path.replace('/ar', '');
        }

        const languageChosen = e.target.value;
        await router.push(url, url, {locale: languageChosen});
    }

    return (
        <header>
            <nav>
                <ul className={styles.navbarContainer}>

                    <Link title={dictionary.navbar_logoLink_title} href="/" className={styles.ouraLink}>
                        <Image className={styles.logo} src={logo} alt="OURA²"/>
                    </Link>
                    {isAdmin ? (
                        <>
                            <Link title={dictionary.navbar_adminLink_title} href="/admindb/"
                                  className={styles.adminLink}>
                                <li>
                                    <Translation dictionary={dictionary} trKey="navbar_adminLink_label"/>
                                </li>
                            </Link>
                        </>
                    ) : (
                        <></>
                    )}
                    {user ? (
                        <>
                            <Link title={dictionary.navbar_dashboardLink_title} href="/dashboard"
                                  className={styles.firstLink}>
                                <li className={styles.icon}>
                                    <FontAwesomeIcon icon={faGaugeHigh}/>
                                </li>
                            </Link>
                            <Link title={dictionary.navbar_profileLink_title} href="/profile"
                                  className={styles.nextLink}>
                                <li className={styles.icon}>
                                    <FontAwesomeIcon icon={faUser}/>
                                </li>
                            </Link>
                        </>
                    ) : (
                        <>
                            <Link title={dictionary.navbar_homeLink_title} href="/" className={styles.firstLink}>
                                <li>
                                    <Translation dictionary={dictionary} trKey="navbar_homeLink_title"/>
                                </li>
                            </Link>
                            <Link title={dictionary.navbar_loginLink_title} href="/login" className={styles.nextLink}>
                                <li>
                                    <Translation dictionary={dictionary} trKey="navbar_loginLink_title"/>
                                </li>
                            </Link>
                        </>
                    )}
                    <Link title={dictionary.navbar_helpLink_title} href="/support/help" className={styles.nextLink}>
                        <li className={styles.icon}>
                            <FontAwesomeIcon icon={faCircleQuestion}/>
                        </li>
                    </Link>
                    <select title={dictionary.navbar_language_title} className={styles.languageSelection} defaultValue={locale} id="language-select"
                            name="language" onChange={(e) => handleLanguageSelect(e)}>
                        <option value="fr">FR</option>
                        <option value="de">DE</option>
                        <option value="en">EN</option>
                        {/*<option value="ar">العربية</option>*/}
                    </select>
                </ul>
            </nav>
        </header>
    );
}
