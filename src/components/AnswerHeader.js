import styles from ".//answerheader.module.css";
import Link from "next/link";
import {useRouter} from "next/router";
import Image from "next/image";
import logo from "../../public/images/navbar-logo.svg";

export default function AnswerHeader({dictionary, locale}) {
    const router = useRouter();
    async function handleLanguageSelect(e) {
        e.preventDefault();

        const path = window.location.pathname;
        let url = window.location.pathname;
        if (path.includes('/en')) {
            url = path.replace('/en','');
        } else if (path.includes('/de')) {
            url = path.replace('/de', '');
        } else if (path.includes('/fr')) {
            url = path.replace('/fr','');
        } else if (path.includes('/ar')) {
            url = path.replace('/ar', '');
        }

        const languageChosen = e.target.value;
        await router.push(url, url, {locale: languageChosen});
    }

    return (
        <div className={styles.headerContainer}>
            <Link title={dictionary.answerheader_link_title} href="/" className={styles.link}>
                <Image className={styles.logo} src={logo} alt="OURA²"/>
            </Link>
            <select title={dictionary.answerheader_language_title} className={styles.languageSelection} defaultValue={locale} id="language-select" name="language" onChange={(e) => handleLanguageSelect(e)}>
                <option value="fr">Français</option>
                <option value="de">Deutsch</option>
                <option value="en">English</option>
                {/*<option value="ar">العربية</option>*/}
            </select>
        </div>
    )
}
