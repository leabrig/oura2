import styles from ".//flashmessage.module.css";
import {useEffect, useState} from "react";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faCircleXmark} from "@fortawesome/free-solid-svg-icons";

export default function FlashMessage({msg, type}) {
    const [show, setShow] = useState(true);

    useEffect(() => {
        const timer = setTimeout(() => {
            setShow(false);
        }, 10000);

        return () => clearTimeout(timer);
    }, []);

    const handleClose = () => {
        setShow(false);
    }

    //The default style of the flash message is error
    let element = styles.error;
    let xMarkStyle = styles.xMarkError;
    if (type === 'error') {
        element = styles.error;
        xMarkStyle = styles.xMarkError;
    } else if (type === 'info') {
        element = styles.info;
        xMarkStyle = styles.xMarkInfo;
    } else if (type === 'success') {
        element = styles.success;
        xMarkStyle = styles.xMarkSuccess;
    }

    // Once the timeout is through the element disappears
    return show ? (
        <div className={styles.wrapperElement}>
            <div className={element}>
                <div className={styles.msg}>{msg}</div>
                <button onClick={handleClose} className={xMarkStyle}>
                    <FontAwesomeIcon icon={faCircleXmark}/>
                </button>
            </div>
        </div>
    ) : null;
}