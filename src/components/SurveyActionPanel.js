import styles from "../components/surveyactionpanel.module.css";
import Link from "next/link";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {
    faArchive, faBoxesPacking,
    faChartPie,
    faClone,
    faDownload,
    faSquarePollHorizontal,
    faTrashCan
} from "@fortawesome/free-solid-svg-icons";
import Dropdown from "./DropdownButton";
import {useState} from "react";
import FlashMessage from "./FlashMessage";
import {downloadCSV} from "../../lib/download-csv";
import {useRouter} from "next/router";
import {handleSurveyArchival} from "../../lib/handle-archival";
import Translation from "./Translation";

export default function SurveyActionPanel ({surveys, isArchived, dictionary, locale}) {
    const [errorMsg, setErrorMsg] = useState('');
    const [successMsg, setSuccessMsg] = useState('');
    const router = useRouter();

    async function handleDelete(e, surveyId) {
        e.preventDefault();
        const confirmDelete = confirm(dictionary.surveyActionPanel_confirm_delete);
        if (!confirmDelete) return;

        setErrorMsg('');
        setSuccessMsg('');

        try {
            const res = await fetch(`/api/survey?id=${surveyId}`, {
                method: 'DELETE',
                headers: {'Content-Type': 'application/json'},
            });
            if (res.status === 200) {
                window.location.reload();
            } else {
                throw new Error(await res.text());
            }
        } catch (error) {
            console.error('An unexpected error occurred:', error);
            setErrorMsg(error.message);
        }
    }

    async function handleCSVDownload(e, surveyId) {
        e.preventDefault();
        const survey = surveys.find((survey) => survey.id === surveyId);
        downloadCSV(survey, locale);
    }

    const handleDoubleClick = (e, surveyId) => {
        e.preventDefault();
        router.push(`/dashboard/mysurveys/view/${surveyId}`);
    }

    async function handleArchive (e, surveyId) {
        e.preventDefault();

        setErrorMsg('');

        try {
            await handleSurveyArchival(surveyId, !isArchived);
        } catch (error) {
            console.error('An unexpected error occurred: ', error);
            setErrorMsg(error.message);
        }
    }

    const surveyContainerStyle = isArchived ? `${styles.surveyContainerArchived}` : `${styles.surveyContainer}`;


    return (
        <div>
            {surveys.map( ((survey, index) => {
                const isLastSurvey = surveys.length === (index + 1);
                let answersCount = 0;
                survey.formanswers.forEach(() => {
                    answersCount++;
                })
                let createdAt = new Date(survey.createdAt);
                let date = createdAt.toLocaleDateString(locale);
                return (
                    <div className={surveyContainerStyle} key={index} onDoubleClick={(e) => handleDoubleClick(e, survey.id)}>
                        <div className={styles.surveyInfoContainer}>
                            <div className={styles.surveyTitle}>{survey.label}</div>
                            <div className={styles.dateAndCountContainer}>
                                <div className={styles.countDisplay}>
                                    {answersCount <= 1 ?
                                        <Translation dictionary={dictionary} trKey="surveyActionPanel_countDisplay_single" params={{answersCount: answersCount}}/>
                                        :
                                        <Translation dictionary={dictionary} trKey="surveyActionPanel_countDisplay_multiple" params={{answersCount: answersCount}}/>}
                                </div>
                                <div className={styles.dateDisplay}>
                                    <Translation dictionary={dictionary} trKey="surveyActionPanel_dateDisplay_createdAt" params={{date: date}}/>
                                </div>
                            </div>
                        </div>
                        <div className={styles.iconsContainer}>
                            <Link href={`/dashboard/mysurveys/view/${survey.id}`}
                                  title={dictionary.surveyActionPanel_linkView_title}
                                  className={styles.iconLink}>
                                <FontAwesomeIcon icon={faSquarePollHorizontal}/>
                            </Link>
                            <div>
                                <Dropdown dictionary={dictionary} isLastSurvey={isLastSurvey}>
                                    {isArchived ?
                                        <></>
                                        :
                                        <div className={styles.dropdownLinkContainer}>
                                            <Link href={`/dashboard/statistics?selectedSurveyId=${survey.id}`}
                                                  title={dictionary.surveyActionPanel_linkAnalyse_title}
                                                  className={styles.dropdownLink}>
                                                <FontAwesomeIcon icon={faChartPie} className={styles.dropdownIcon}/>
                                                <span>
                                                <Translation dictionary={dictionary}
                                                             trKey="surveyActionPanel_linkAnalyse_label"/>
                                            </span>
                                            </Link>
                                        </div>
                                    }
                                    <div className={styles.dropdownLinkContainer}>
                                        <Link href={`/dashboard/createsurvey?selectedSurveyId=${survey.id}`}
                                              title={dictionary.surveyActionPanel_linkDuplicate_title}
                                              className={styles.dropdownLink}>
                                            <FontAwesomeIcon icon={faClone} className={styles.dropdownIcon}/>
                                            <span>
                                                <Translation dictionary={dictionary}
                                                             trKey="surveyActionPanel_linkDuplicate_label"/>
                                            </span>
                                        </Link>
                                    </div>
                                    <div className={styles.dropdownSeparator}/>
                                    <div className={styles.dropdownLinkContainer}>
                                        <button onClick={(e) => handleCSVDownload(e, survey.id)}
                                                title={dictionary.surveyActionPanel_linkDownload_title}
                                                className={styles.dropdownLink}>
                                            <FontAwesomeIcon icon={faDownload} className={styles.dropdownIcon}/>
                                            <span>
                                                <Translation dictionary={dictionary} trKey="surveyActionPanel_linkDownload_label"/>
                                            </span>
                                        </button>
                                    </div>
                                    <div className={styles.dropdownLinkContainer}>
                                        <button onClick={(e) => handleArchive(e, survey.id)}
                                                title={isArchived ? dictionary.surveyActionPanel_linkDisarchive_title : dictionary.surveyActionPanel_linkArchive_title}
                                                className={styles.dropdownLink}>

                                            {isArchived ?
                                                <><FontAwesomeIcon icon={faBoxesPacking} className={styles.dropdownIcon}/>
                                                    <span>
                                                        <Translation dictionary={dictionary} trKey="surveyActionPanel_linkDisarchive_label"/>
                                                    </span></>
                                                :
                                                <><FontAwesomeIcon icon={faArchive} className={styles.dropdownIcon}/>
                                                    <span>
                                                        <Translation dictionary={dictionary} trKey="surveyActionPanel_linkArchive_label"/>
                                                    </span></>
                                            }
                                        </button>
                                    </div>
                                    <div className={styles.dropdownSeparator}/>
                                    <div className={styles.dropdownLinkContainer}>
                                        <button onClick={(e) => handleDelete(e, survey.id)}
                                                title={dictionary.surveyActionPanel_linkDelete_title}
                                                className={styles.dropdownLink}>
                                            <FontAwesomeIcon icon={faTrashCan} className={styles.dropdownIcon}/>
                                            <span>
                                                <Translation dictionary={dictionary} trKey="surveyActionPanel_linkDelete_label"/>
                                            </span>
                                        </button>
                                    </div>
                                </Dropdown>
                            </div>
                        </div>
                    </div>
                )
            }))
            }
            {errorMsg && <FlashMessage msg={errorMsg} type="error"/>}
            {successMsg && <FlashMessage msg={successMsg} type="success"/>}
        </div>
    )

}