import styles from "./deletebutton.module.css";

export default function DeleteButton ({type, onClick, label, title, children}) {
    return (
        <div>
            <button type={type} onClick={onClick} title={title} className={styles.deleteButton}>
                {label}
                {children}
            </button>
        </div>
    )
}