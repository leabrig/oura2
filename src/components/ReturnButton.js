import styles from ".//returnbutton.module.css";
import Link from "next/link";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faArrowLeft} from "@fortawesome/free-solid-svg-icons";
import Translation from "./Translation";

export default function ReturnButton({href, title, dictionary}) {
    return (
        <div className={styles.linkHolder}>
            <Link href={href} title={title} className={styles.returnLink}>
                    <span>
                        <FontAwesomeIcon icon={faArrowLeft}/>
                    </span>
                <div className={styles.returnLabel}>
                    <Translation dictionary={dictionary} trKey="returnButton_link_label"/>
                </div>
            </Link>
        </div>
    )
}