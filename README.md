# OURA2

OURA2 is a web-app that allows teachers to evaluate the learning experience of their students.
The goal is to improve teaching-learning methods by asking the students directly what their experiences with an activity or class was.

It is being developed under the GNU AFFERO GENERAL PUBLIC LICENSE.
You can therefore install it on your institutional or personal server for a similar usage.

# Quickstart

The [first section](#clone-project) covers the download of the project from gitlab.

The [second section](#install-dependencies) covers how to install node, npm and dependencies (including prisma that we use as an ORM), and postgres. You will need these to continue with installing OURA2.

The [third section](#connect-and-seed-database) covers how to connect prisma with your database and seed it.
The seeding adds all the preset questions, dimensions and domains.
It also creates a superuser with e-mail : "admin@oura2.ch" and password : "admin". Once you have your project running it is **imperative** to change this accounts password.
This account allows you to change questions, dimensions, domains and the roles of users from an admin page on the web-app.

The [last section](#build-and-run) covers how to build and run the project.

## Clone project

In the folder of your choice, clone the OURA2 project.

```shell
git clone https://gitlab.com/leabrig/oura2.git
```

## Install dependencies

Go into the ROOT directory of your project.

To install node we will use nvm (Node Version Manager).
The first step will thus be to install nvm.
Note that after installing nvm you will need to restart your terminal to apply changes.

Next you will need to install node and npm, the version will be automatically detected using .nvmrc.
This ensures you have the same version of node as used in the development of this application.
You may choose to install another version but this might cause issues.
If everything worked correctly you should now have node and its associated npm version installed.
If that is not the case you might need to retry installation of node.

Now you can install the required npm dependencies.

If you don't have a postgres database (or other) on your machine you will need to install one.
Here an example using the available package.

Well done ! Now to the next step.

```shell
# In the root directory of your project
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.40.0/install.sh | bash # Install nvm using curl
# Restart terminal and check if nvm is installed
nvm --version

nvm install # Install node


node -v # Check for node and npm
npm -v

npm install # Install npm dependencies

sudo apt install postgresql # Install postgresql
```

## Connect and seed database

First, you need to ensure that you know your postgres username and password.
If you are deploying on a server, I strongly advise you to use a secure password instead of the default.

Next you create a database with the name of your choice (we will call it oura2).
You will need to know how to connect to the database, we will use netstat for that.

```shell
sudo -i -u postgres # In terminal to connect to postgres
psql # In postgres

CREATE DATABASE oura2;

sudo netstat -nltp # Check the port at which your database is running (it is usually after the :)
```

Once you have all this information, you can set up your DATABASE_URL. This URL looks like this:
"postgresql://USERNAME:PASSWORD@localhost:PORT/DATABASE?schema=public"
We will take as an example these values and insert them:
- USERNAME=postgres
- PASSWORD=postgres
- PORT=5432
- DATABASE=oura2

This will result in the following link: "postgresql://postgres:postgres@localhost:5432/oura2?schema=public".
You will need to create one or two .env file to store your environment variables.
It will either be .env.development (for development on your personal computer) or .env.production.

In this file, you will add your DATABASE_URL="your-link".
Because we have a forgot password function, you also need to set these variables. 
- SMTP_SERVER_USERNAME=your-email
- SMTP_SERVER_PASSWORD=password
- SMTP_SERVER_HOST=server-host // Usually in a mail.server.com format

Now everything should be set to start the next step.
First generate your prisma client.
Then you can run the migrations to load the schema onto the database.
Finally, you can seed the database.

Note: We have specific scripts to ensure that prisma can load the environment variables from the env files. These scripts use the dotenv module that you will need to install if you want to use these commands.

```shell
npx prisma generate # Generate prisma client

npx prisma migrate:dev # For development environment
# OR
npx prisma migrate:prod # For production environment

npx prisma seed:dev  # For development environment
# OR
npx prisma seed:prod  # For production environment
```

## Build and run

Now that you have everything working between prisma and postgresql, you can build and start your project.

```shell
npm run build
npm run start

# For development running use this command:
npm run dev # This will allow for direct reproduction of changes
```

And you're good to go !