import crypto from 'crypto';
import prisma from "../lib/prisma";
import {findUserByEmail, findUserById} from "./user";

export async function generateRecoveryCode({email}) {
    const user = await findUserByEmail({email: email});
    if (!user) {
        return null;
    }

    // Create a code that will be used in link
    const MASK = 0x3d;
    const LETTERS = 'abcdefghijklmnopqrstuvwxyz';
    const NUMBERS = '1234567890';
    const charset = `${NUMBERS}${LETTERS}${LETTERS.toUpperCase()}_-`.split('');

    const bytes = new Uint8Array(20);
    crypto.randomFillSync(bytes);

    const code = bytes.reduce((acc, byte) => `${acc}${charset[byte & MASK]}`, '');

    // Create recovery hash and salt from code
    const recoverySalt = crypto.randomBytes(16).toString('hex');
    const recoveryHash = crypto
        .pbkdf2Sync(code, recoverySalt, 1000, 64, 'sha512')
        .toString('hex');
    const recoveryTimeStamp = new Date();

    await prisma.user.update({
        where: {
            id: user.id,
        },
        data: {
            recoveryHash: recoveryHash,
            recoverySalt: recoverySalt,
            recoveryTimeStamp: recoveryTimeStamp,
        }
    })

    return {userId: user.id, timeStamp: recoveryTimeStamp, code: code};
}

export async function checkRecoveryCode({userId, code}) {
    const user = await findUserById({id: userId});
    if (user.recoverySalt) {
        const givenCodeHash = crypto
            .pbkdf2Sync(code, user.recoverySalt, 1000, 64, 'sha512')
            .toString('hex');
        const codeValid = user.recoveryHash === givenCodeHash;
        return {codeValid: codeValid};
    } else {
        return false;
    }
}

export async function deleteRecoveryCode (id) {
    const user = await findUserById({id: id});
    return await prisma.user.update ({
        where: {
            id: user.id,
        },
        data: {
            recoveryHash: null,
            recoverySalt: null,
            recoveryTimeStamp: null,
        },
    });
}