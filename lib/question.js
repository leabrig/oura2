import { v4 as uuidv4 } from 'uuid';
import prisma from '../lib/prisma';

export async function createQuestion({ label, dimensionId, posneg, labelFr, labelDe, labelAr }) {
    const question = await prisma.question.create({
        data: {
            id: uuidv4(),
            label: label,
            labelFr: labelFr,
            labelDe: labelDe,
            labelAr: labelAr,
            dimensionId: dimensionId,
            posneg: posneg,
        }
    });
    return question;
}

export async function updateQuestion( {id, label, dimensionID, posneg, labelFr, labelDe, labelAr }){
    const question = await prisma.question.update({
        where: {
            id: id,
        },
        data: {
            label: label,
            labelFr: labelFr,
            labelDe: labelDe,
            labelAr: labelAr,
            dimensionId: dimensionID,
            posneg: posneg,
        },
    });
    return question;
}

export async function deleteQuestion(id) {
    const deleteQuestion = await prisma.question.delete({
        where: {
            id: id,
        },
    });
    return deleteQuestion;
}