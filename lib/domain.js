import { v4 as uuidv4 } from 'uuid';
import prisma from '../lib/prisma';

export async function createDomain({ label, labelFr, labelDe, labelAr }) {
    const domain = await prisma.domain.create({
        data: {
            id: uuidv4(),
            label: label,
            labelFr: labelFr,
            labelDe: labelDe,
            labelAr: labelAr,
        }
    });
    return domain;
}

export async function updateDomain( {id, label, labelFr, labelDe, labelAr }){
    const domain = await prisma.domain.update({
        where: {
            id: id,
        },
        data: {
            label: label,
            labelFr: labelFr,
            labelDe: labelDe,
            labelAr: labelAr,
        },
    })
}

export async function deleteDomain(id) {
    const deleteDomain = await prisma.domain.delete({
        where: {
            id: id,
        },
    });
    return deleteDomain;
}