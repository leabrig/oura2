import crypto from 'crypto';
import {v4 as uuidv4} from 'uuid';
import prisma from '../lib/prisma';

export async function createUser({email, password, language}) {
    // Here you should create the user and save the salt and hashed password (some dbs may have
    // authentication methods that will do it for you so you don't have to worry about it):
    const salt = crypto.randomBytes(16).toString('hex');
    const hash = crypto
        .pbkdf2Sync(password, salt, 1000, 64, 'sha512')
        .toString('hex');

    const user = await prisma.user.create({
        data: {
            id: uuidv4(),
            createdAt: new Date(),
            salt: salt,
            hash: hash,
            email: email,
        }
    });

    await prisma.discriminantVariable.create({
        data: {
            id: uuidv4(),
            label: "Âge/Alter/Age",
            authorId: user.id,
            discriminantitems: {
                create: [
                    {
                        id: uuidv4(),
                        label: "18-21",
                    },
                    {
                        id: uuidv4(),
                        label: "22-25",
                    },
                    {
                        id: uuidv4(),
                        label: "26-29",
                    },
                ]
            }
        }
    })

    await prisma.discriminantVariable.create({
        data: {
            id: uuidv4(),
            label: "Groupe/Gruppe/Group",
            authorId: user.id,
            discriminantitems: {
                create: [
                    {
                        id: uuidv4(),
                        label: "Bleu/Blau/Blue",
                    },
                    {
                        id: uuidv4(),
                        label: "Rouge/Rot/Red",
                    },
                    {
                        id: uuidv4(),
                        label: "Vert/Grün/Green",
                    },
                    {
                        id: uuidv4(),
                        label: "Jaune/Gelb/Yellow",
                    },
                ]
            }
        }
    })

    await updateLanguage({ email, language});

    return user;
}

//Update user email
export async function updateEmail({oldEmail, newEmail}){
    const updateUser = await prisma.user.update({
        where: {
            email: oldEmail,
        },
        data: {
            email: newEmail,
        },
    })

    return updateUser;
}

export async function updatePassword({id, email, password}){
    const salt = crypto.randomBytes(16).toString('hex')
    const hash = crypto
        .pbkdf2Sync(password, salt, 1000, 64, 'sha512')
        .toString('hex')

    return await prisma.user.update({
        where: {
            id: id,
            email: email,
        },
        data: {
            salt: salt,
            hash: hash,
        }
    })
}

export async function updateAdmin({id, admin}) {
    return await prisma.user.update({
        where: {
            id: id,
        },
        data: {
            admin: !admin,
        }
    })
}

// Look up user in DB
export async function findUserByEmail({email}) {
    try {
        return await prisma.user.findUnique({
            where: {
                email: email,
            },
        });
    } catch (error) {
        console.error(error);
        return null;
    }
}

export async function findUserById ({id}) {
    try {
        return await prisma.user.findUnique ({
            where: {
                id: id,
            },
        });
    } catch (error) {
        console.error(error);
        return null;
    }
}

// Compare the password of an already fetched user (using `findUser`) and compare the password for a potential match
export function validatePassword(user, inputPassword) {
    const inputHash = crypto
        .pbkdf2Sync(inputPassword, user.salt, 1000, 64, 'sha512')
        .toString('hex')
    return user.hash === inputHash;
}

// Delete user
export async function deleteUser (userId) {
     return await prisma.user.delete ({
        where: {
            id: userId,
        }
    });
}

export async function updateLanguage({email, language}) {
    let languageEnum;
    if (language === 'en') {
       languageEnum = 'ENGLISH';
    } else if (language === 'fr'){
        languageEnum = 'FRENCH';
    } else if (language === 'de'){
        languageEnum = 'GERMAN';
    } else if (language === 'ar') {
        languageEnum = 'ARABIC';
    }
    return await prisma.user.update({
        where: {
            email:email,
        },
        data: {
           language: languageEnum,
        }
    })
}