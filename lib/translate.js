
export default function translate(dictionary, trKey, params = {}) {
    try {
        if (dictionary[trKey]) {
            let translation = dictionary[trKey];
            Object.keys(params).forEach((param) => {
                translation = translation.replace(`{${param}}`, params[param])
            })
            return translation;
        } else {
            throw Error('Key not found');
        }``
    } catch (error) {
        throw Error(`Unable to get translation content for ${trKey}`);
    }
}