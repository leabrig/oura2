'use server';
import nodemailer from 'nodemailer';
const SMTP_SERVER_HOST = process.env.SMTP_SERVER_HOST;
const SMTP_SERVER_USERNAME = process.env.SMTP_SERVER_USERNAME;
const SMTP_SERVER_PASSWORD = process.env.SMTP_SERVER_PASSWORD;
const transporter = nodemailer.createTransport({
    service: 'infomaniak',
    host: SMTP_SERVER_HOST,
    port: 465,
    secure: false, //Keep in mind that maybe on the server this must be set to true, but not on localhost
    auth: {
        user: SMTP_SERVER_USERNAME,
        pass: SMTP_SERVER_PASSWORD,
    },
});
export async function sendMail({sendTo, subject, text, html,}) {
    try {
        const info = await transporter.sendMail({
            from: SMTP_SERVER_USERNAME,
            to: sendTo,
            subject: subject,
            text: text,
            html: html ? html : '',
        });
        console.log('Message Sent', info.messageId);
        return info;
    } catch (error) {
        console.error('Something Went Wrong', SMTP_SERVER_USERNAME, error);
    }
}