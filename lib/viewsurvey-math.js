// This function averages all the results from the questions from a survey and inserts them into an array with [{id: id, choiceCount}, {...}].
export function averageQuestions (survey) {
    let questionAverages = [];
    survey.questions.forEach((question) => {
        const answers = survey.formanswers.flatMap((formAnswer) =>
            formAnswer.answers.filter((answer) => answer.questionId === question.id)
        );

        const total = answers.reduce((sum, answer) => {
            return sum + answer.questionChoice;
        }, 0);
        const average = (total / answers.length).toFixed(1);

        questionAverages.push({questionId: question.id, average});
    });
    return (questionAverages);
}

// This function sums all the results from the discriminant variables from a survey and inserts them into an array with [{id: id, choiceCount}, {...}].
export function sumVariables (survey) {
    const variableAverages = [];
    survey.discriminantvariables.forEach((variable) => {
        const counts = [];

        survey.formanswers.forEach((formAnswer) => {
            formAnswer.answers.forEach((answer) => {
                if (answer.discriminantvariableId && answer.discriminantvariableId === variable.id) {
                    const choice = answer.itemChoice;
                    const countItem = counts.find((item) => item.choice === choice);
                    if (countItem) {
                        countItem.count += 1;
                    } else {
                        counts.push({ choice: choice, count: 1 });
                    }
                }
            });
        });

        const choiceCounts = variable.discriminantitems.map((item) => {
            const countItem = counts.find((count) => count.choice === item.id);
            return {
                choice: item.id,
                count: countItem ? countItem.count : 0,
            }
        });

        variableAverages.push({
            discriminantvariableId: variable.id,
            choiceCounts,
        });
    });
    return (variableAverages);
}