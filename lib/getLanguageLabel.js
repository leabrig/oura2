
export function getLanguageLabel(locale){
    if (locale === 'fr'){
        return 'labelFr';
    } else if (locale === 'de'){
        return 'labelDe';
    } else if (locale === 'en'){
        return 'label';
    } else if (locale === 'ar'){
        return 'labelAr';
    }
}