export async function handleSurveyArchival (surveyId, toArchive) {
    const body = {
        surveyId: surveyId,
        archive: toArchive,
    }

    try {
        const res = await fetch('/api/survey/', {
            method: 'PUT',
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify(body),
        });
        if (res.status === 200) {
            window.location.reload()
        } else {
            throw new Error(await res.text());
        }
    } catch (error) {
        console.error('An unexpected error occurred: ', error);
    }
}

export async function handleVariableArchival (variableId, toArchive) {
    const body = {
        variableId: variableId,
        archive: toArchive,
    }

    try {
        const res = await fetch('/api/discriminantvariable/', {
            method: 'PUT',
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify(body),
        });
        if (res.status === 200) {
            // We want a console log because we don't reload the page so we need to know if the archival or unarchival worked in the DB
            console.log("The (un)archival of the variable was successfully handled.")
        } else {
            throw new Error(await res.text());
        }
    } catch (error) {
        console.error('An unexpected error occurred: ', error);
    }
}
