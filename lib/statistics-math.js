// Compares two arrays and checks if they are equal
export function areArraysEqual(arr1, arr2) {
    if (arr1.length !== arr2.length) {
        return false;
    }
    for (let i = 0; i < arr1.length; i++) {
        if (arr1[i] !== arr2[i]) {
            return false;
        }
    }
    return true;
}


// Calculates the averages for each dimension given and returns an array with the values.
export function calcDimensionsAverages(dimensions, survey, variable, item, variables, items) {
    const dimensionAverages = [];
    dimensions.map((dimension) => {
        const dimensionQuestions = survey.questions.filter(
            (question) => question.dimensionId === dimension.id
        );
        const questionAverages = [];
        dimensionQuestions.map((question) => {
            let answers;
            if (!variables) {
                answers = getSingleVarAnswers(survey, question, variable, item);
            } else {
                answers = getTwoVarsAnswers(survey, question, variables, items);
            }
            const total = answers.reduce((sum, answer) => {
                if (question.posneg === false) {
                    return sum + (7 - answer.questionChoice);
                }
                return sum + answer.questionChoice;
            }, 0);
            questionAverages.push(parseFloat((total / answers.length).toFixed(1)));
        });
        const questionTotal = questionAverages.reduce((accumulator, currentValue) => accumulator + currentValue, 0);

        // INFO: I removed the parseFloat around the division to achieve that instead of NaN nothing is displayed, not an optimal solution if you have a better idea please tell me
        dimensionAverages.push((questionTotal / questionAverages.length).toFixed(1));
    });
    return dimensionAverages;
}

// ------------- GETTERS -----------------

// Find Answers according to one question and max one variable
export function getSingleVarAnswers (survey, question, variable, item) {
    if (!variable || !item) {
        return survey.formanswers.flatMap((formAnswer) =>
            formAnswer.answers.filter((answer) => answer.questionId === question.id));
    } else if (variable && item) {

        //Filter the returned answers based on if the person chose the particular item or not.
        const filteredAnswers = [];
        survey.formanswers.forEach((formAnswer) => {
            formAnswer.answers.forEach((answer) => {
                if (answer.discriminantvariableId === variable.id && answer.itemChoice === item.id) {
                    filteredAnswers.push(formAnswer);
                }
            });
        });
        return filteredAnswers.flatMap((formAnswer) =>
            formAnswer.answers.filter((answer) => answer.questionId === question.id));
    }
}

//Find answers of survey according to one question and two variables
export function getTwoVarsAnswers (survey, question, variables, items) {

    //Filter the returned answers based on if the person chose the particular item pair or not
    const filteredAnswers = [];
    survey.formanswers.forEach((formAnswer) => {
        let matchCount = 0;
        formAnswer.answers.forEach((answer) => {
            if (answer.discriminantvariableId === variables[0].id && answer.itemChoice === items[0].id) {
                matchCount++;
            } else if (answer.discriminantvariableId === variables[1].id && answer.itemChoice === items[1].id) {
                matchCount++;
            }
            if (matchCount === 2) {
                filteredAnswers.push(formAnswer);
            }
        });
    });
    return filteredAnswers.flatMap((formAnswer) =>
        formAnswer.answers.filter((answer) => answer.questionId === question.id));
}

// Get the dimensions for filtering in the dimensions display which are shown
export function getDimensionsForFiltering (survey) {
    const chosenDimensions = [];
    const usedDimensionsIds = [];
    survey.questions.forEach((question) => {
        const dimensionId = question.dimension.id;
        if (!usedDimensionsIds.includes(dimensionId)) {
            usedDimensionsIds.push(dimensionId);
            chosenDimensions.push(question.dimension);
        }
    });
    return chosenDimensions;
}

// Get the common dimensions of all surveys passed
export function getCommonDimensions(surveys) {
    if (!surveys || surveys.length === 0) {
        return [];
    }

    const dimensionFrequency = new Map();

    // To collect the dimensions for next comparison
    let firstSurveyDimensions = [];

    surveys.forEach((survey, index) => {
        const dimensions = getDimensionsForFiltering(survey);
        if (index === 0) {
            firstSurveyDimensions = dimensions;
        }
        dimensions.forEach((dimension) => {
            const dimensionId = dimension.id;
            if (dimensionFrequency.has(dimensionId)) {
                dimensionFrequency.set(dimensionId, dimensionFrequency.get(dimensionId) + 1);
            } else {
                dimensionFrequency.set(dimensionId, 1);
            }
        });
    });

    const commonDimensions = [];

    for (const [dimensionId, frequency] of dimensionFrequency.entries()) {
        if (frequency === surveys.length) {
            firstSurveyDimensions.forEach((dimension) => {
                if (dimension.id === dimensionId) {
                    commonDimensions.push(dimension);
                }
            });
        }
    }

    return commonDimensions;
}
