import prisma from '../lib/prisma';
import {parseAndValidateFlightRouterState} from "next/dist/server/app-render/parse-and-validate-flight-router-state";

export async function deleteDVariable(id) {
    const deleteDVariable = await prisma.discriminantVariable.delete({
        where: {
            id: id,
        },
    });
    return deleteDVariable;
}

export async function updateArchive ({variableId, archive}) {
    return await prisma.discriminantVariable.update({
        where: {
            id: variableId,
        },
        data: {
            isArchived: archive,
        }
    })
}