import { v4 as uuidv4 } from 'uuid';
import prisma from '../lib/prisma';

export async function createSurvey({ authorId, label, questions, discriminantValues, uniqueStringForm, dVariables }) {
    const survey = await prisma.survey.create({
        data: {
            id: uuidv4(),
            label: label,
            authorId: authorId,
            createdAt: new Date(),
            uniqueStringForm: uniqueStringForm,
            questions: {
                connect: questions.map(question => ({
                    id: question.id,
                }))
            },
            discriminantvariables: {
                connect: dVariables.map(dVariable => ({
                    id: dVariable.id,
                })),
            }
        },
    });

    for(const discriminantValue of discriminantValues){
        if(discriminantValue.label !== '') {
            const discriminantVariable = await prisma.discriminantVariable.create({
                data: {
                    id: uuidv4(),
                    label: discriminantValue.label,
                    authorId: authorId,
                    surveys: {
                        connect: {
                            id: survey.id,
                        },
                    },
                },
            });

            for(const item of discriminantValue.items){
                    await prisma.discriminantItem.create({
                    data: {
                        id: uuidv4(),
                        label: item.label,
                        discriminantvariable: {
                            connect: {
                                id: discriminantVariable.id,
                            },
                        },
                    },
                });
            }
        }
    }
    return survey;
}

export async function updateArchived ({surveyId, archive}) {
    return await prisma.survey.update ({
        where: {
            id: surveyId,
        },
        data: {
            isArchived: archive,
        }
    })
}

export async function deleteSurvey(id) {
    return (
        await prisma.survey.delete({
        where: {
            id: id,
        },
        })
    );
}