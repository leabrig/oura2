import Local from 'passport-local';
import {findUserByEmail, validatePassword} from './user';

export const localStrategy = new Local.Strategy({ usernameField: 'email', passwordField: 'password'}, function (
    username,
    password,
    done
) {
    findUserByEmail({ email: username })
        .then((user) => {
            if (user && validatePassword(user, password)) {
                done(null, user)
            } else {
                done(new Error("Email or Password invalid"));
            }
        })
        .catch((error) => {
            done(error)
        })
})
