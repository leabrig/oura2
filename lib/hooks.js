import {useEffect} from 'react';
import Router from 'next/router';
import useSWR from 'swr';

const fetcher = (url) =>
  fetch(url)
    .then((r) => r.json())
    .then((data) => {
      return { user: data?.user || null }
    })

//Checks if the user that is trying to access the page is an admin
export function useAdmin({ redirectTo, redirectIfFound } = {}) {
  const { data, error, isLoading } = useSWR('/api/user', fetcher);
  const user = data?.user;
  const finished = Boolean(data);
  const hasUser = Boolean(user);
  const isAdmin = Boolean(user?.admin);

  useEffect(() => {
    if (!redirectTo || !finished) return
    if (
        // If redirectTo is set, redirect if the user was not found.
        (redirectTo && !redirectIfFound && !hasUser) ||
        // If redirectIfFound is also set, redirect if the user was found
        (redirectIfFound && hasUser)
    ) {
      Router.push(redirectTo);
    }
    // If the user is not an admin, redirect him to dashboard
    if (hasUser && !isAdmin) {
      Router.push('/dashboard');
    }
  }, [redirectTo, redirectIfFound, finished, hasUser]);

  return error ? null : {user, isAdmin, isLoading};
}

export function useUser({ redirectTo, redirectIfFound } = {}) {
  const { data, error, isLoading } = useSWR('/api/user', fetcher);
  const user = data?.user;
  const finished = Boolean(data);
  const hasUser = Boolean(user);
  const isAdmin = Boolean(user?.admin);

  useEffect(() => {
    if (!redirectTo || !finished) return;
    if (
      // If redirectTo is set, redirect if the user was not found.
      (redirectTo && !redirectIfFound && !hasUser) ||
      // If redirectIfFound is also set, redirect if the user was found
      (redirectIfFound && hasUser)
    ) {
      Router.push(redirectTo);
    }
  }, [redirectTo, redirectIfFound, finished, hasUser]);


  return error ? null : { user, isAdmin, isLoading };
}
