export function downloadCSV(survey, locale) {
    let createdAt = new Date(survey.createdAt);
    let date = createdAt.toLocaleDateString(locale);

    let labelLang = '';
    if (locale === 'fr') {
        labelLang = 'labelFr';
    } else if (locale === 'de') {
        labelLang = 'labelDe';
    } else if (locale === 'en') {
        labelLang = 'label';
    } else if (locale === 'ar') {
        labelLang = 'labelAr';
    }

    // Get questions and discriminant variables labels and ids.
    const questionsLabels = survey.questions.map((question) => JSON.stringify(question[labelLang]));
    const questionsIDs = survey.questions.map((question) => question.id);
    const dVariablesLabels = survey.discriminantvariables.map((variable) => JSON.stringify(variable.label));
    const dVariablesIDs = survey.discriminantvariables.map((variable) => variable.id);

    // Define heading for each row of data.
    let csvFileData = `${questionsLabels.join(',')},${dVariablesLabels.join(',')}\n`;

    // Retrieve each answer and add to csvFileData
    survey.formanswers.forEach((formAnswer) => {
        let questions = questionsIDs.map(questionId =>
            formAnswer.answers
                .filter(answer => answer.questionId === questionId)
                .map(answer => JSON.stringify(answer.questionChoice))
                .join(', ')
        ).join(', ');
        questions += ', ';

        let dVariables = dVariablesIDs.map(variableId => {
            const dVariable = survey.discriminantvariables.find(variable => variable.id === variableId);
            if (!dVariable) return '';

            return formAnswer.answers
                .filter(answer => answer.discriminantvariableId === variableId)
                .map(answer => {
                    const item = dVariable.discriminantitems.find(item => item.id === answer.itemChoice);
                    return item ? JSON.stringify(item.label) : '';
                })
                .join(', ');
        }).join(', ');
        csvFileData += `${questions}${dVariables}\n`;
    });

    const csvDataURI = `data:text/csv;charset=UTF-8, ${encodeURIComponent(csvFileData)}`;

    const link = document.createElement('a');
    link.href = csvDataURI;
    link.download = `${survey.label}-${date}.csv`;
    link.style.display = 'none';
    document.body.appendChild(link);

    link.click();

    document.body.removeChild(link);
}