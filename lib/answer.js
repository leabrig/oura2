import prisma from '../lib/prisma';
import { v4 as uuidv4 } from 'uuid';

export async function checkCode(code) {
    const survey = await prisma.survey.findUnique({
        where: {
            uniqueStringForm: code,
        },
    });

    if(survey !== undefined) {
        return survey;
    } else {
        return null;
    }
}

export async function createAnswer({surveyId, questionsAnswers, variablesAnswers}) {
    const formAnswer = await prisma.formAnswer.create({
        data: {
            id: uuidv4(),
            surveyId: surveyId,
        },
    });

    for(const question of questionsAnswers) {
        await prisma.answer.create({
            data: {
                id: uuidv4(),
                questionId: question.questionId,
                formanswerId: formAnswer.id,
                questionChoice: question.questionChoice,
            },
        });
    }

    for(const variable of variablesAnswers) {
        await prisma.answer.create({
            data: {
                id: uuidv4(),
                discriminantvariableId: variable.discriminantvariableId,
                formanswerId: formAnswer.id,
                itemChoice: variable.itemChoice,
            },
        });
    }
    return formAnswer;
}