import path from 'path';
import fs from 'fs';

export const getLanguageFile = (locale) => {
    const filePath = path.join(process.cwd(), 'public/locales', locale + '.json');

    if (fs.existsSync(filePath)) {
        return JSON.parse(fs.readFileSync(filePath, 'utf-8'));
    } else {
        console.error(`Language file for locale "${locale}" not found.`);
        return null;
    }
};
