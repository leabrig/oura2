import { v4 as uuidv4 } from 'uuid';
import prisma from '../lib/prisma';

export async function createDimension({ label, labelFr, labelDe, labelAr, domainId }) {
    const dimension = await prisma.dimension.create({
        data: {
            id: uuidv4(),
            label: label,
            labelFr: labelFr,
            labelDe: labelDe,
            labelAr: labelAr,
            domainId: domainId,
        }
    });
    return dimension;
}

export async function updateDimension( {id, label, labelFr, labelDe, labelAr, domainId }){
    const dimension = await prisma.dimension.update({
        where: {
            id: id,
        },
        data: {
            label: label,
            labelFr: labelFr,
            labelDe: labelDe,
            labelAr: labelAr,
            domainId: domainId,
        },
    })
}

export async function deleteDimension(id) {
    const deleteDimension = await prisma.dimension.delete({
        where: {
            id: id,
        },
    });
    return deleteDimension;
}